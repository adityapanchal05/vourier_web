<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistics Company | Individual Parcel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" type="text/css" href="css/individual-document.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php

  include 'conn.php';

	function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];

    $order_id = $_SESSION['orderID'];

    $fromName = $_SESSION['fromName'];
    $fromMobile = $_SESSION['fromMobile'];
    $fromAddressLine1 = $_SESSION['fromAddressLine1'];
    $fromAddressLine2 = $_SESSION['fromAddressLine2'];
    $fromCode = $_SESSION['fromCode'];

    $toName = $_SESSION['toName'];
    $toMobile = $_SESSION['toMobile'];
    $toAddressLine1 = $_SESSION['toAddressLine1'];
    $toAddressLine2 = $_SESSION['toAddressLine2'];
    $toCode = $_SESSION['toCode'];

    $product_name = $_SESSION['product_details'];
    $weight = $_SESSION['weight'];
    $length = $_SESSION['length'];
    $breadth = $_SESSION['breadth'];
    $height = $_SESSION['height'];
    $isCOD = $_SESSION['COD'];

    $PRICE = $_SESSION['PRICE'];

    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";
    */

    $zone;

    if(strcmp($member_plan, 'INDIVIDUAL')==0) {
      $operator_plan = 'INDIVIDUAL';
      $money_to_add = 40;
    }
    else if(strcmp($member_plan, 'SILVER')==0) {
      $operator_plan = 'SILVER';
      $money_to_add = 8;
    }
    else if(strcmp($member_plan, 'GOLD')==0) {
      $operator_plan = 'GOLD';
      $money_to_add = 4;
    }
    else if(strcmp($member_plan, 'DIAMOND')==0) {
      $operator_plan = 'DIAMOND';
      $money_to_add = 2;
    }

   /* Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    if(!Opencon()) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {
      */
      $fromCode_query = "SELECT * FROM pincode_database WHERE pin_code = '".$fromCode."' ";
      $fromCode_result = Opencon()->query($fromCode_query);
      if($fromCode_result->num_rows > 0) {
        while ($row = $fromCode_result->fetch_assoc()) {
          $fromCode_city = $row['City'];
          $fromCode_state = $row['State'];
        }

        $toCode_query = "SELECT * FROM pincode_database WHERE pin_code = '".$toCode."' ";
        $toCode_result = Opencon()->query($toCode_query);
        if($toCode_result->num_rows > 0) {
          while ($row = $toCode_result->fetch_assoc()) {
            $toCode_city = $row['City'];
            $toCode_state = $row['State'];
          }

          $volumetric_weight = ($length * $breadth * $height)/5000;

          if($volumetric_weight > $weight) {
            $final_weight = $volumetric_weight.' kg (Volumetric Weight)';
          }
          else {
            $final_weight = $weight. ' kg (Weight)';
          }

        }
        else {
          echo '<script language="javascript">';
          echo 'alert("No Data for destination Pin Code.");';
          echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
        echo 'alert("No Data for source Pin Code.");';
        echo '</script>';
      }

   // }

    if(isset($_POST['payMoneyAndShip'])) {
       $orderID = $_POST['orderID'];
       $orderPrice = $_POST['price'];
       $operator = $_POST['operator'];
       $courier_id = $_POST['courier_id'];
       $moneyAdded = $_POST['moneyAdded'];

       $getShipmentID = "SELECT Shipment_ID from customer_orders WHERE Order_ID = '".$orderID."'";
       $result = Opencon() -> query($getShipmentID);
       if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $shipmentID = $row['Shipment_ID'];
        }
       }

       $getToken = "SELECT * FROM shiprocket_token WHERE serial_number = '0000000001'";
       $result = Opencon()->query($getToken);
       if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $token = $row['value'];
        }
      }

       if($wallet < $orderPrice) {
        echo '<script language="javascript">';
        echo 'alert("Not Enough Money. Please Recharge your Wallet.");';
        echo 'location.href="Recharge";';
        echo '</script>';
       }
       else {
        $wallet = $wallet - $orderPrice;
        $updateWallet = Opencon()->query("UPDATE customer_details SET Wallet = '".$wallet."' WHERE Email = '".$Email."'");
        if($updateWallet) {
          $statusToShip = Opencon()->query("UPDATE customer_orders SET Status = 'READY TO SHIP' WHERE Order_ID = '".$orderID."'");
          if($statusToShip) {
            $setOperator = Opencon()->query("UPDATE customer_orders SET operator = '".$operator."' WHERE Order_ID = '".$orderID."'");
            if($setOperator) {

              $url = 'https://apiv2.shiprocket.in/v1/external/courier/assign/awb';

        $ch = curl_init($url);
        $data = array(
            "shipment_id" => [
              (int)$shipmentID
            ],
            "courier_id"=> (int)$courier_id,
            "status"=> "assign"
        );
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $json_result = json_decode($result);
        $awb_code = $json_result->response->data->awb_code;

        $shiprocketPrice = $orderPrice - $moneyAdded;

        $setOurPrice = Opencon()->query("UPDATE customer_orders SET Price_Paid_By_Customer = '".$orderPrice."' WHERE Order_ID = '".$orderID."'");

        $setShiprocketPrice = Opencon()->query("UPDATE customer_orders SET Price_By_Shiprocket = '".$shiprocketPrice."' WHERE Order_ID = '".$orderID."'");

                $setAWBnumber = Opencon()->query("UPDATE customer_orders SET AWB_Number = '".$awb_code."' WHERE Order_ID = '".$orderID."'");
			$url = 'https://apiv2.shiprocket.in/v1/external/courier/generate/label';

                  $label = "SELECT * FROM customer_orders WHERE AWB_Number = '".$awb_code."'";
                  $result = Opencon() -> query($label);
                  if($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                    $shipment_id = $row['Shipment_ID'];
                    }
                  }
                  $data = array(
                  "shipment_id" => [
                    array(
                    "$shipment_id" => $shipment_id
                    )
                  ]
                  );
                  $ch = curl_init($url);
                  $dir = 'label/';
                  $payload = json_encode($data);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                  //curl_setopt_array($ch, $payload);
                  $file_name = basename($url);
                  $save_file_loc = $dir . $file_name;
                  $fp = fopen($save_file_loc, 'wb');

                  curl_setopt($ch, CURLOPT_FILE, $fp);
                  curl_setopt($ch, CURLOPT_HEADER, 0);
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  $result = curl_exec($ch);
                  curl_close($ch);
                  echo "<br>";
                  echo $result;
                  $json_result = json_decode($result, true);

                  $pdf = "https://kr-shipmultichannel.s3.ap-southeast-1.amazonaws.com/128492/labels/shipping-label-".$shipment_id."-".$awb_code.".pdf";
                  $Label = ('Label.pdf');
                  $filename = basename($Label);
                  file_put_contents($filename,file_get_contents($pdf));

                  fclose($fp);

                $output = null;
		$return = null;

		exec("/home/ubuntu/anaconda3/bin/python decode.py", $output, $return);
		$barcode = $output[0];
                  $setBarcode = Opencon()->query("UPDATE customer_orders SET barcode_number = '".$barcode."' WHERE AWB_Number = '".$awb_code."'");


                $output = null;
		$return = null;

		exec("/home/ubuntu/anaconda3/bin/python Routing_code.py", $output, $return);
		$code = $output[0];
                  $setRoutingCode = Opencon()->query("UPDATE customer_orders SET routing_code = '".$code."' WHERE AWB_Number = '".$awb_code."'");
	if($setAWBnumber) {
                  echo '<script language="javascript">';
                  echo 'alert("Order successful!");';
                  echo 'location.href="READY_TO_SHIP";';
                  echo '</script>';
                }
                else {
                  echo '<script language="javascript">';
                  echo 'alert("Some Error Occured!");';
                  echo '</script>';
                }
              }
              else {
                echo '<script language="javascript">';
                echo 'alert("Some Error Occured!");';
                echo '</script>';
              }
          }
          else {
            echo '<script language="javascript">';
            echo 'alert("Order Failed. Money will be refunded.");';
            echo '</script>';
          }
        }
        else {
          echo '<script language="javascript">';
          echo 'alert("Some Error Occured.");';
          echo '</script>';
        }
       }
    }

	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="Recharge" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="Plans" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="KYC">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%;">

    	<div class="individual-order-heading" style="width: 100%; min-width: 1000px; overflow-x: hidden;">
    		<p align="left" style="color: #656565; font-size: 24px;">Select your Courier Partner Based on your Shipping Needs</p>
    		<p align="left" style="color: #656565; font-size: 14px; margin-top: -16px;">(Standard Pick-Up time is upto 7:00 PM)</p>
    		<br><br>
    		<div style="width: 18%; float: left;">
    			<p align="left" style="color: #5d9cec; font-size: 14px; margin-top: -16px;">Pickup From:</p>
          <p align="left" style="color: #656565; font-size: 14px;"><b><?php echo $fromName; ?></b></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><b><?php echo $fromMobile; ?></b></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><?php echo $fromAddressLine1; ?></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><?php echo $fromAddressLine2; ?></p>
    			<p align="left" style="font-size: 14px; color: #656565; margin-top: -10px;"><b><?php echo $fromCode; ?></b></p>
            	<p align="left" style="font-size: 14px; color: #656565; margin-top: -10px;"><b><?php echo $fromCode_city.', '; echo $fromCode_state; ?></b></p>
    		</div>
    		<div style="width: 11%; display: inline-block; float: left; padding-top: 20px;">
    		<p style="font-size: 14px; color: #656565; margin-bottom: -15px;">COD : <?php echo $isCOD; ?></p>
            <img src="images/long_arrow.png" style="margin-top: 25px; width: 100%; height: 11px; float: left;">
          	</div>
          	<div style="width: 10%; float: left;">
          		<p>&nbsp</p>
          	</div>
          	<div style="width: 61%; float: left;">
    			<p align="left" style="color: #5d9cec; font-size: 14px; margin-top: -16px;">Deliver To:</p>
          <p align="left" style="color: #656565; font-size: 14px;"><b><?php echo $toName; ?></b></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><b><?php echo $toMobile; ?></b></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><?php echo $toAddressLine1; ?></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><?php echo $toAddressLine2; ?></p>
    			<p align="left" style="font-size: 14px; color: #656565; margin-top: -10px;"><b><?php echo $toCode; ?></b></p>
            	<p align="left" style="font-size: 14px; color: #656565; margin-top: -10px;"><b><?php echo $toCode_city.', '; echo $toCode_state; ?></b></p>
    		</div>
    		<div style="width: 19%; float: left;">
    			<p align="left" style="color: #5d9cec; font-size: 14px; margin-top: 20px;">Order Details:</p>
    			<p align="left" style="color: #656565; font-size: 14px;"><b>Product Name : </b><?php echo $product_name; ?></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><b>Weight (in kg) : </b><?php echo $weight; ?></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><b>Volumetric Weight : </b><?php echo $volumetric_weight; ?></p>
    		</div>
    		<div style="width: 50%; float: left;">
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: 49px;"><b>Applied Weight : </b><?php echo $final_weight; ?></p>
    			<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;"><b>Dimensions (in cms) : </b><?php echo $length; ?> X <?php echo $breadth; ?> X <?php echo $height; ?></p>
    		</div>
    		<div style="width: 100%; float: left; background-color: rgb(255, 248, 220); min-width: 1000px; margin-top: 20px;">
    			<div style="width: 14%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Courier Company</b></p>
    			</div>
    			<div style="width: 16%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Courier Description</b></p>
    			</div>
    			<div style="width: 10%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Mode</b></p>
    			</div>
    			<div style="width: 10%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Rating</b></p>
    			</div>
    			<div style="width: 14%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Pickup Available</b></p>
    			</div>
    			<div style="width: 14%; float: left; padding-left: 20px;">
    				<p align="left" style=" font-size: 14px; color: rgb(69, 69, 69); margin-top: 10px;"><b>Price</b></p>
    			</div>
        	</div>
    		<div style="min-width: 1000px; width: 100%; float: left; margin-top: 20px;">
    			<?php

    			 $getToken = "SELECT * FROM shiprocket_token WHERE serial_number = '0000000001'";
           $result = Opencon()->query($getToken);
           if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
              $token = $row['value'];
            }
          }

          $url = 'https://apiv2.shiprocket.in/v1/external/courier/serviceability/';

          if(strcmp($isCOD, "Yes")==0) {
            $codQuery = 1;
          }
          else {
            $codQuery = 0;
          }

          $length = (int)$length;
          $breadth = (int)$breadth;
          $height = (int)$height;

          $ch = curl_init($url);
          $data = array(
            "pickup_postcode" => (int)$fromCode,
            "delivery_postcode" => (int)$toCode,
            "length" => $length/100,
            "breadth" => $breadth/100,
            "height" => $height/100,
            "weight" => $weight,
            "cod" => (int)$codQuery,
            "declared_value" => (int)$PRICE,
          );
          $payload = json_encode($data);

          curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
          curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $result = curl_exec($ch);
          curl_close($ch);
          
          $json_result = json_decode($result);
          $available_courier_companies = $json_result->data->available_courier_companies;
          
          foreach ($available_courier_companies as $company) {
            $courier_id = $company->courier_company_id;
            $courier_name = $company->courier_name;
            $min_weight = $company->min_weight;
            $mode = $company->mode;
            $rating = $company->rating;
            $seconds_left_for_pickup = $company->seconds_left_for_pickup;
            $rate = $company->rate + $money_to_add;
            $etd = $company->etd;

            if($seconds_left_for_pickup == 0) {
              $pickupTime = "Tomorrow";
            }
            else {
              $pickupTime = "Today";
            }

            if($mode == 0) {
              $mode_image = "delivery_surface.svg";
              $mode_text = "Surface";
            }
            else {
              $mode_image = "delivery_air.svg";
              $mode_text = "Air";
            }

            if(strpos($courier_name, 'Fedex') !== false || strpos($courier_name, 'FEDEX-SURFACE') !== false) {
              $image_name = "Fedex.png";
            }
            elseif (strpos($courier_name, 'Bluedart') !== false) {
              $image_name = "Bluedart.png";
            }

            elseif (strpos($courier_name, 'Xpressbees') !== false) {
              $image_name = "XpressBees.png";
            }

            elseif (strpos($courier_name, 'Delhivery') !== false) {
              $image_name = "Delhivery.png";
            }

            elseif (strpos($courier_name, 'Dotzot') !== false) {
              $image_name = "DotZot.png";
            }

            if(strpos($courier_name, 'Fedex') !== false || strpos($courier_name, 'FEDEX-SURFACE') !== false || strpos($courier_name, 'Bluedart') !== false || strpos($courier_name, 'Xpressbees') !== false || strpos($courier_name, 'Delhivery') !== false || strpos($courier_name, 'Dotzot') !== false) {
              echo '<div style="width: 14%; float: left;">';
                echo '<img src="images/'.$image_name.'" style=" max-height: 24px; max-width: 93px; float: left; padding-left: 20px;">';
                echo '</div>';

                echo '<div style="width: 16%; float: left; padding-left: 25px;">';
                echo '<p align="left" style="font-size: 14px; color: #656565;"><b>'.$courier_name.'</b><br>Min Weight: <b>'.$min_weight.'kg</b><br>Expected Delivery: <b>'.$etd.'</b></p>';
                echo '</div>';

                if($mode == 1) {
                  echo '<div style="width: 10%; float: left; padding-left: 25px;">';
                  echo '<img src="images/'.$mode_image.'" style=" max-height: 24px; max-width: 93px; float: left;"><br><br>';
                  echo '<p align="left" style="font-size: 14px; color: #656565; margin-top: -12px; margin-left: 38px;">'.$mode_text.'</p>';
                  echo '</div>';
                }
                else {
                  echo '<div style="width: 10%; float: left; padding-left: 60px;">';
                  echo '<img src="images/'.$mode_image.'" style=" max-height: 24px; max-width: 93px; float: left;"><br><br>';
                  echo '<p align="left" style="font-size: 14px; color: #656565; margin-top: -16px; margin-left: -7px;">'.$mode_text.'</p>';
                  echo '</div>';
                }

                echo '<div style="width: 10%; float: left; padding-left: 23px;">';
                echo '<p align="left" style="font-size: 14px; color: #656565;">'.$rating.'/5.0</p>';
                echo '</div>';

                echo '<div style="width: 14%; float: left; padding-left: 50px;">';
                echo '<p align="left" style="font-size: 14px; color: #656565;">'.$pickupTime.'</p>';
                echo '</div>';

                echo '<div style="width: 10%; float: left; padding-left: 15px;">';
                echo '<p align="left" style="font-size: 14px; color: #656565;"><i class="fa fa-inr">&nbsp</i><b>'.$rate.'</b></p>';
                echo '</div>';
                
                echo '<div style="width: 14%; float: left; padding-left: 15px; margin-top: -7px;">';
                echo '<form method="post" enctype="multipart/form-data" style="float: left;"><button style="float: left;"  class="btn btn-info" id="speed_post_button" name="payMoneyAndShip">Choose</button>
                      <input type="hidden" name="orderID" value="'.$order_id.'" style="float:left"></p>
                      <input type="hidden" name="operator" value="'.$courier_name.'" style="float: left;"></p>
                      <input type="hidden" name="courier_id" value="'.$courier_id.'" style="float: left;"></p>
                      <input type="hidden" name="moneyAdded" value="'.$money_to_add.'" style="float: left;"></p>
                      <input style="float: left;" type="hidden" name="price" value="'.$rate.'"></p>
                      </form>';
                    echo '</div>';
                    
                    echo '<div style="width: 100%; float: left; padding-left: 15px;">';
                echo '<hr style="border-bottom: 1px solid #eeeeee;">';
                echo '</div>';


            }
          }

    			?>
    		</div>
    	</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>
