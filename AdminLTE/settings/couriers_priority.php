<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | General Details</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">

  	<link rel="stylesheet" type="text/css" href="css/toggle_button.css">

  	<link rel="stylesheet" type="text/css" href="css/drag.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">
    <?php include '../aside.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding: 0px;">
        
        	<div style="background-color: #fafbfc; border-bottom: 1px solid #b3b3b3; color: #212121; width: 100%; float: left; padding: 15px; padding-bottom: 0px;">
          		<p align="left" style="font-size: 23px;">Couriers Priority</p>
        	</div>

        	<div style="width: 50%; background-color: #ffffff; padding: 20px; display: inline-block; margin-top: 40px;">
        		<div id="BRD" style="width: 150px; height: 150px; float: left; border-radius: 50%; background-color: #f5f7fa;">
        			<img id="BRI" src="images/best-rated_inactive.png" style="margin-top: 42px; margin-left: 45px;">
        			<img id="BRA" src="images/best-rated_active.png" style="margin-top: 42px; margin-left: 42px;">
        			<input id="best_rated" type="radio" name="priority_type" style="float: right; height: 1.2em; width: 1.2em; margin-right: 10px; margin-top: -97px;">
        		</div>
        		<div id="CD" style="width: 150px; height: 150px; float: left; border-radius: 50%; background-color: #f5f7fa; margin-left: 65px;">
        			<img id="CI" src="images/cheapest_inactive.png" style="margin-top: 42px; margin-left: 46px;">
        			<img id="CA" src="images/cheapest_active.png" style="margin-top: 42px; margin-left: 43px;">
        			<input id="cheapest" type="radio" name="priority_type" style="float: right; height: 1.2em; width: 1.2em; margin-right: 10px; margin-top: -97px;">
        		</div>
        		<div id="FD" style="width: 150px; height: 150px; float: left; border-radius: 50%; background-color: #f5f7fa; margin-left: 65px;">
        			<img id="FI" src="images/fastest_inactive.png" style="margin-top: 42px; margin-left: 44px;">
        			<img id="FA" src="images/fastest_active.png" style="margin-top: 42px; margin-left: 41px;">
        			<input id="fastest" type="radio" name="priority_type" style="float: right; height: 1.2em; width: 1.2em; margin-right: 10px; margin-top: -97px;">
        		</div>
        		<div id="CUD" style="width: 150px; height: 150px; float: left; border-radius: 50%; background-color: #f5f7fa; margin-left: 65px;">
        			<img id="CUI" src="images/custom_inactive.png" style="margin-top: 42px; margin-left: 45px;">
        			<img id="CUA" src="images/custom_active.png" style="margin-top: 42px; margin-left: 42px;">
        			<input id="custom" type="radio" name="priority_type" style="float: right; height: 1.2em; width: 1.2em; margin-right: 10px; margin-top: -97px;">
        		</div>
        	</div>
        	<div style="width: 50%; display: inline-block; background-color: #ffffff; margin-top: -13px; margin-left: -2px;">
        		<div style="width: 24%; float: left;">
	        		<p style="color: #656565; font-size: 13px;"><b>BEST RATED</b></p>
	        	</div>
	        	<div style="width: 27%; float: left;">
	        		<p style="color: #656565; font-size: 13px;"><b>CHEAPEST</b></p>
	        	</div>
	        	<div style="width: 24%; float: left;">
	        		<p style="color: #656565; font-size: 13px;"><b>FASTEST</b></p>
	        	</div>
	        	<div style="width: 25%; float: left;">
	        		<p style="color: #656565; font-size: 13px; margin-left: 10px;"><b>CUSTOM</b></p>
	        	</div>

	        	<div id="drag" style="width: 100%; float: left; margin-top: 10px; background-color: #f5f7fa; display: none;">
	        		<p style="color: #656565; font-size: 15px; margin-top: 10px; margin-bottom: 0px;"><i class="fa fa-arrows"></i>&nbsp&nbsp&nbspDRAG TO SET YOUR PRIORITY</p>
	        	</div>

	        	<div id="slides" style="width: 100%; float: left; background-color: #f5f7fa; padding: 10px; list-style: none; display: none;">
	        		<div class="slide fedex" style="width: 180px; float: left; background-color: #ffffff; padding: 5px; cursor: move;  margin-left: 20px; margin-top: 10px; display: inline-table;">
	        			<img src="images/Fedex.png" style="max-height: 24px; max-width: 93px; margin-top: 10px;">
	        			<p style="color: #404040; margin-top: 10px;">FedEx</p>
	        		</div>
	        		<div class="slide dtdc" style="width: 180px; float: left; background-color: #ffffff; padding: 5px; cursor: move; margin-left: 20px; margin-top: 10px; display: inline-table;">
	        			<img src="images/dtdc.png" style="max-height: 24px; max-width: 93px; margin-top: 10px;">
	        			<p style="color: #404040; margin-top: 10px;">DTDC</p>
	        		</div>
	        		<div class="slide dhl" style="width: 180px; float: left; background-color: #ffffff; padding: 5px; cursor: move; margin-left: 20px; margin-top: 10px; display: inline-table;">
	        			<img src="images/dhl.png" style="max-height: 24px; max-width: 93px; margin-top: 10px;">
	        			<p style="color: #404040; margin-top: 10px;">DHL</p>
	        		</div>
	        		<div class="slide speed_post" style="width: 180px; float: left; background-color: #ffffff; padding: 5px; cursor: move; margin-left: 20px; margin-top: 10px; display: inline-table;">
	        			<img src="images/Speed_Post.png" style="max-height: 24px; max-width: 93px; margin-top: 10px;">
	        			<p style="color: #404040; margin-top: 10px;">Speed Post</p>
	        		</div>
	        	</div>


	        	<div style="width: 100%; float: left; margin-bottom: 20px; margin-top: 20px;">
	        		<button class="btn btn-info" style="border-radius: 3px; background-color: #285fdb; border: none; padding-left: 30px; padding-right: 30px;">Ok</button>
	        	</div>
        	</div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include '../footer.php';?>
</div>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- jQuery 3 -->

<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript">
	window.setInterval(function () {
		if(document.getElementById("best_rated").checked == true) {
			document.getElementById("BRA").style.display = 'block';
			document.getElementById("BRD").style.border = '3px solid #595959';
			document.getElementById("BRI").style.display = 'none';
		}
		else {
			document.getElementById("BRA").style.display = 'none';
			document.getElementById("BRD").style.border = 'none';
			document.getElementById("BRI").style.display = 'block';
		}

		if(document.getElementById("cheapest").checked == true) {
			document.getElementById("CA").style.display = 'block';
			document.getElementById("CD").style.border = '3px solid #595959';
			document.getElementById("CI").style.display = 'none';
		}
		else {
			document.getElementById("CA").style.display = 'none';
			document.getElementById("CD").style.border = 'none';
			document.getElementById("CI").style.display = 'block';
		}

		if(document.getElementById("fastest").checked == true) {
			document.getElementById("FA").style.display = 'block';
			document.getElementById("FD").style.border = '3px solid #595959';
			document.getElementById("FI").style.display = 'none';
		}
		else {
			document.getElementById("FA").style.display = 'none';
			document.getElementById("FD").style.border = 'none';
			document.getElementById("FI").style.display = 'block';
		}

		if(document.getElementById("custom").checked == true) {
			document.getElementById("CUA").style.display = 'block';
			document.getElementById("CUD").style.border = '3px solid #595959';
			document.getElementById("CUI").style.display = 'none';
			document.getElementById("drag").style.display = 'block';
			document.getElementById("slides").style.display = 'block';
		}
		else {
			document.getElementById("CUA").style.display = 'none';
			document.getElementById("CUD").style.border = 'none';
			document.getElementById("CUI").style.display = 'block';
			document.getElementById("drag").style.display = 'none';
			document.getElementById("slides").style.display = 'none';
		}
	},1);
</script>

<script>
  $( function() {
    $( "#slides" ).sortable();
    $( "#slides" ).disableSelection();
  } );
</script>

</body>
</html>