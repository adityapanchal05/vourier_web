<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | General Details</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">
    <?php include '../aside.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding: 0px;">
        
        <div style="background-color: #fafbfc; border-bottom: 1px solid #b3b3b3; color: #212121; width: 100%; float: left; padding: 15px; padding-bottom: 0px;">
          <p align="left" style="font-size: 23px;">Company Information</p>
        </div>

        <div style="background-color: white; padding: 20px; width: 25%; float: left; margin-left: 20px; margin-top: 40px;">

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='general-details'">
            <button style="background-color: transparent; border: none; color: #285fdb; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspGeneral Details</button><span style="float: right; color: #285fdb; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='address-details'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspAddress Details</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='address-pickup'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspPickup Address</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='accounting-bank-details'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Accounting & Bank Details</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='ship-link'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Ship With Link</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='remittance-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Remittance Settings</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='rto-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Change RTO Address</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='label-buyer-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Change Label & Buyer Settings</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='gstin-invoicing'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">GSTIN Invoicing</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

        </div>

        <div style="width: 71%; float: left; background-color: white; padding: 20px; margin-top: 40px; margin-left: 28px;">
          
          <div style="width: 100%; padding: 5px; padding-bottom: 0px; float: left;">
            <p align="left" style="color: #656565; font-size: 21px;"><b>Company Details</b></p>
          </div>

          <div style="width: 100%; padding: 5px; padding-left: 15px; float: left; border: 1px solid rgba(0, 0, 0, 0.12);">
            <div style="width: 100%; float: left; padding: 5px;">
              <p align="left" style="color: #656565; margin-bottom: 0px;"><b>Company ID</b><span style="color: red"><b>*</b></span></p>
            </div>
            <div style="width: 100%; float: left;">
              <input type="text" style="width: 50%; float: left; cursor: not-allowed; height: 30px; border: none; border-radius: 3px; padding-left: 5px; color: rgba(0, 0, 0, 0.5); background-color: #ddd; " value="17065" disabled>
            </div>

            <div style="width: 100%; float: left; padding: 5px; margin-top: 10px;">
              <p align="left" style="color: #656565; margin-bottom: 0px;"><b>Company Name</b><span style="color: red"><b>*</b></span></p>
            </div>
            <div style="width: 100%; float: left;">
              <input type="text" style="width: 50%; float: left; height: 30px; border: 1px solid #dde6e9; border-radius: 3px; padding-left: 5px; " value="<?php echo $first_name.' '.$last_name ?>">
            </div>

            <div style="width: 100%; float: left; padding: 5px; margin-top: 10px;">
              <p align="left" style="color: #656565; margin-bottom: 0px;"><b>Website</b></p>
            </div>
            <div style="width: 100%; float: left;">
              <input type="text" style="width: 50%; float: left; height: 30px; border: 1px solid #dde6e9; border-radius: 3px; padding-left: 5px;" placeholder="http://www.xyz.com">
            </div>

            <div style="width: 100%; float: left; padding: 5px; margin-top: 10px;">
              <p align="left" style="color: #656565; margin-bottom: 0px;"><b>Email</b><span style="color: red"><b>*</b></span></p>
            </div>
            <div style="width: 100%; float: left;">
              <input type="text" style="width: 50%; float: left; cursor: not-allowed; height: 30px; border: none; border-radius: 3px; padding-left: 5px; color: rgba(0, 0, 0, 0.5); background-color: #ddd; " value="<?php echo $Email; ?>" disabled>
            </div>

            <div style="width: 100%; float: left; padding: 5px; margin-top: 10px;">
              <p align="left" style="color: #656565; margin-bottom: 0px;"><b>Website/Company Logo</b></p>
            </div>
            <div style="width: 100%; float: left;">
              <input type="file" style="width: 50%; float: left; height: 30px; border-radius: 3px; padding-left: 5px;">
            </div>

          </div>


          <div style="width: 100%; float: left; margin-top: 20px; margin-bottom: 10px;">
            <button class="btn btn-info" style="float: left; background-color: #285fdb; border-radius: 3px; border: none;"><i class="fa fa-floppy-o"></i>&nbsp&nbsp&nbspSave & Next</button>
          </div>

        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../footer.php';?>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>