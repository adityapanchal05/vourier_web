<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | General Details</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

    /*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="recharge.php" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="buy_plans.php" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a href="pages/widgets.html">
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="support.php"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding: 0px;">
        
        <div style="background-color: #fafbfc; border-bottom: 1px solid #b3b3b3; color: #212121; width: 100%; float: left; padding: 15px; padding-bottom: 0px;">
          <p align="left" style="font-size: 23px;">Company Information</p>
        </div>

        <div style="background-color: white; padding: 20px; width: 25%; float: left; margin-left: 20px; margin-top: 40px;">

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='general-details'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspGeneral Details</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='address-details'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspAddress Details</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='address-pickup'">
            <button style="background-color: transparent; border: none; color: #285fdb; float: left; outline: none;"><span style="color: #27c24c;"><i class="fa fa-check"></i></span>&nbsp&nbsp&nbspPickup Address</button><span style="float: right; color: #285fdb; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='accounting-bank-details'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Accounting & Bank Details</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='ship-link'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Ship With Link</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='remittance-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Remittance Settings</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='rto-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Change RTO Address</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='label-buyer-settings'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">Change Label & Buyer Settings</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

          <div style="width: 100%; float: left; padding-bottom: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.12); cursor: pointer; margin-bottom: 15px;" onclick="window.location.href='gstin-invoicing'">
            <button style="background-color: transparent; border: none; color: #656565; float: left; outline: none;">GSTIN Invoicing</button><span style="float: right; color: #656565; margin-top: 3px;"><i class="fa fa-caret-right"></i></span>
          </div>

        </div>

        <div style="width: 71%; float: left; background-color: white; padding: 20px; margin-top: 40px; margin-left: 28px;">
          
          <div style="width: 100%; padding: 5px; padding-bottom: 0px; float: right;">
          	<button class="btn btn-info" style="float: right; background-color: #285fdb; border: none; border-radius: 3px; outline: none; margin-left: 10px;"><i class="fa fa-plus"></i>&nbsp&nbsp&nbspAdd Pickup Address</button>
          	<button class="otherProcessingButtons" title="Export Pickup Locations" style="float: right; padding-top: 6px; padding-bottom: 6px;"><i class="fa fa-level-down"></i></button>
            <button class="otherProcessingButtons" title="Upload Bulk Pickup Locations" style="float: right; padding-top: 6px; padding-bottom: 6px;"><i class="fa fa-level-up"></i></button>
          </div>

          <div style="width: 100%; padding: 5px; padding-left: 15px; float: left; margin-top: 10px;">
            <div style="width: 100%; float: left; padding: 5px; box-shadow: 1px 1px 1px 1px #eee; padding-left: 10px;">
            	<div style="width: 10%; float: left;"><p align="left" style="font-size: 21px; margin-bottom: 0px;"><b>Alias</b></p></div>
            	<div style="width: 3%; float: left; color: #656565; margin-top: 6.5px;"><a href="#" style="float: left;"><i class="fa fa-edit"></i></a></div>
            	<div style="width: 6%; float: left; background-color: #27c24c; border-radius: 6px; margin-top: 6px;"><p style="margin-bottom: 0px; color: white;"><b>Active</b></p></div>
            	<div style="width: 18%; float: left; margin-top: 3.5px;"><p style="margin-bottom: 0px; color: #656565; font-size: 18px;">Location ID : <b>110011</b></p></div>
            	<div style="width: 10%; float: right; margin-top: 6.5px;"><a href="#" style="color: #c1c2c3; font-size: 18px;" title="Collapse"><i class="fa fa-plus"></i></a></div>
            </div>
          </div>

          <div style="width: 100%; padding: 5px; padding-left: 15px; float: left; margin-top: 10px;">
            <div style="width: 100%; float: left; padding: 5px; box-shadow: 1px 1px 1px 1px #eee; padding-left: 10px;">
            	<div style="width: 10%; float: left;"><p align="left" style="font-size: 21px; margin-bottom: 0px;"><b>Alias 2</b></p></div>
            	<div style="width: 3%; float: left; color: #656565; margin-top: 6.5px;"><a href="#" style="float: left;"><i class="fa fa-edit"></i></a></div>
            	<div style="width: 6%; float: left; background-color: #27c24c; border-radius: 6px; margin-top: 6px;"><p style="margin-bottom: 0px; color: white;"><b>Active</b></p></div>
            	<div style="width: 18%; float: left; margin-top: 3.5px;"><p style="margin-bottom: 0px; color: #656565; font-size: 18px;">Location ID : <b>110012</b></p></div>
            	<div style="width: 10%; float: right; margin-top: 6.5px;"><a href="#" style="color: #c1c2c3; font-size: 18px;" title="Collapse"><i class="fa fa-plus"></i></a></div>
            </div>
          </div>

          <div style="width: 100%; padding: 5px; padding-left: 15px; float: left; margin-top: 10px;">
            <div style="width: 100%; float: left; padding: 5px; box-shadow: 1px 1px 1px 1px #eee; padding-left: 10px;">
            	<div style="width: 10%; float: left;"><p align="left" style="font-size: 21px; margin-bottom: 0px;"><b>Alias 3</b></p></div>
            	<div style="width: 3%; float: left; color: #656565; margin-top: 6.5px;"><a href="#" style="float: left;"><i class="fa fa-edit"></i></a></div>
            	<div style="width: 6%; float: left; background-color: #27c24c; border-radius: 6px; margin-top: 6px;"><p style="margin-bottom: 0px; color: white;"><b>Active</b></p></div>
            	<div style="width: 18%; float: left; margin-top: 3.5px;"><p style="margin-bottom: 0px; color: #656565; font-size: 18px;">Location ID : <b>110013</b></p></div>
            	<div style="width: 10%; float: right; margin-top: 6.5px;"><a href="#" style="color: #c1c2c3; font-size: 18px;" title="Collapse"><i class="fa fa-plus"></i></a></div>
            </div>
          </div>

          <div style="width: 100%; padding: 5px; padding-left: 15px; float: left; margin-top: 10px;">
            <div style="width: 100%; float: left; padding: 5px; box-shadow: 1px 1px 1px 1px #eee; padding-left: 10px;">
            	<div style="width: 10%; float: left;"><p align="left" style="font-size: 21px; margin-bottom: 0px;"><b>Alias 4</b></p></div>
            	<div style="width: 3%; float: left; color: #656565; margin-top: 6.5px;"><a href="#" style="float: left;"><i class="fa fa-edit"></i></a></div>
            	<div style="width: 6%; float: left; background-color: #27c24c; border-radius: 6px; margin-top: 6px;"><p style="margin-bottom: 0px; color: white;"><b>Active</b></p></div>
            	<div style="width: 18%; float: left; margin-top: 3.5px;"><p style="margin-bottom: 0px; color: #656565; font-size: 18px;">Location ID : <b>110014</b></p></div>
            	<div style="width: 10%; float: right; margin-top: 6.5px;"><a href="#" style="color: #c1c2c3; font-size: 18px;" title="Collapse"><i class="fa fa-plus"></i></a></div>
            </div>
          </div>

        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>