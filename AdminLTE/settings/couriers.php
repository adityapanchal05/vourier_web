<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | General Details</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">

  	<link rel="stylesheet" type="text/css" href="css/toggle_button.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">
    <?php include '../aside.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding: 0px;">
        
        	<div style="background-color: #fafbfc; border-bottom: 1px solid #b3b3b3; color: #212121; width: 100%; float: left; padding: 15px; padding-bottom: 0px;">
          		<p align="left" style="font-size: 23px;">Couriers</p>
        	</div>

        	<div style="width: 100%; float: left; padding: 10px; background-color: #ffffff;">
        		<input type="text" style="float: left; width: 15%; border: 1px solid #dde6e9; border-radius: 3px; height: 26px; margin-left: 10px; padding-left: 10px;" placeholder="Search by Name">
        		<button style="float: left; box-shadow: 0 0px 1px rgba(0, 0, 0, 0.07); border: solid 1px #ccc; margin-left: -3px; padding-top: 2px; padding-bottom: 2px;"><i class="fa fa-search"></i></button>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa;">
        		<div style="width: 20%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>NAME</b><span><button style="float: right; color: #285fdb; border: none; background-color: transparent; outline: none;"><i class="fa fa-caret-down"></i></button></span></p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>ACCOUNT</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 2px;"><b>SERVICABILITY</b></p>
        		</div>

        		<div style="width: 18%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 2px;"><b>MASTER COMPANY</b></p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 2px;"><b>TYPE OF SERVICE</b></p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>STATUS</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa; border-top: none;">
        		<div style="width: 20%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<img src="images/fedex.png" style="max-width: 93px; max-height: 24px;">
        			<p style="color: #404040; margin-top: 5px; margin-bottom: 0px;">FedEx</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 18%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">FedEx</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Domestic</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<label class="switch" style="margin-bottom: 15px;">
                		<input type="checkbox" checked><span class="slider round"></span>
              		</label>
        		</div>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa; border-top: none;">
        		<div style="width: 20%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<img src="images/dtdc.png" style="max-width: 93px; max-height: 24px;">
        			<p style="color: #404040; margin-top: 5px; margin-bottom: 0px;">DTDC</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 18%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">DTDC</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Domestic</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<label class="switch" style="margin-bottom: 15px;">
                		<input type="checkbox" checked><span class="slider round"></span>
              		</label>
        		</div>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa; border-top: none;">
        		<div style="width: 20%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<img src="images/dhl.png" style="max-width: 93px; max-height: 24px;">
        			<p style="color: #404040; margin-top: 5px; margin-bottom: 0px;">DHL</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 18%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">DHL</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Domestic</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<label class="switch" style="margin-bottom: 15px;">
                		<input type="checkbox" checked><span class="slider round"></span>
              		</label>
        		</div>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa; border-top: none;">
        		<div style="width: 20%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<img src="images/Speed_Post.png" style="max-width: 93px; max-height: 24px;">
        			<p style="color: #404040; margin-top: 5px; margin-bottom: 0px;">Speed Post</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Logistics Company</p>
        		</div>

        		<div style="width: 18%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Speed Post</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 29px;">Domestic</p>
        		</div>

        		<div style="width: 16%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<label class="switch" style="margin-bottom: 15px;">
                		<input type="checkbox" checked><span class="slider round"></span>
              		</label>
        		</div>
        	</div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include '../footer.php';?>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>