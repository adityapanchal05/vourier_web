<?php
	require_once "config.php";
	if (isset($_SESSION['access_token']))
		$gClient->setAccessToken($_SESSION['access_token']);
	else if (isset($_GET['code'])) {
		$token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
		$_SESSION['access_token'] = $token;
	} else {
		header('Location: register.php');
		exit();
	}
	$oAuth = new Google_Service_Oauth2($gClient);
	$userData = $oAuth->userinfo_v2_me->get();

	$currentIDQuery = "SELECT * from user_ids";
	$result = $con->query($currentIDQuery);
	if($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$current_ID = $row['current'];
		}
	}

	$newID = $current_ID + 1;

 	$result = $con->query("INSERT into customer_details (Email, RegistrationType, User_ID ,FirstName, LastName, ImageLink, AccountType, Priority_Operator, Wallet, Member_Plan, Manifest_Number) values
 			('".$userData['email']."', 'GOOGLE', '$newID','".$userData['givenName']."','".$userData['familyName']."','".$userData['picture']."', 'INDIVIDUAL', 'FedEx', '0.0', 'INDIVIDUAL', '0')");

 	if($result) {
 		echo '<script language="javascript">';
 		echo 'alert("Registration Successful. Login to continue.");';
 		echo 'location.href="logout.php";';
 		echo '</script>';
		exit();
 	}
 	else {
 		echo '<script language="javascript">';
 		echo 'alert("Email Address already registered.");';
 		echo 'location.href="logout.php";';
 		echo '</script>';
 	}
?>