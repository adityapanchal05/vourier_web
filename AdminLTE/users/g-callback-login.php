<?php
	require_once "config-login.php";
	if (isset($_SESSION['access_token']))
		$gClient->setAccessToken($_SESSION['access_token']);
	else if (isset($_GET['code'])) {
		$token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
		$_SESSION['access_token'] = $token;
	} else {
		header('Location: register.php');
		exit();
	}
	$oAuth = new Google_Service_Oauth2($gClient);
	$userData = $oAuth->userinfo_v2_me->get();

	$google_email = $userData['email'];

	if(!$con) {
	die("Connection Failed :".mysqli_connect_error());
	}
	else {
		$email = $google_email;
		$isAMember = 1;

		if(!$con) {
			die("Connection Failed :".mysqli_connect_error());
		}
		else {
			$sql_query = "SELECT * from customer_details";
			$result = $con -> query($sql_query);

			if($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$db_email = $row['Email'];
					$first_name = $row['FirstName'];
					$last_name = $row['LastName'];
					$account_type = $row['AccountType'];
					$image_link = $row['ImageLink'];
					$gstin = $row['GSTIN'];
					$address = $row['ADDRESS'];
					$wallet = $row['Wallet'];
					$user_id = $row['User_ID'];
					$member_plan = $row['Member_Plan'];
					if(strcmp($db_email, $email) == 0) {
						$isAMember++;
						break;
					}
				}
				if($isAMember > 1) {
					$_SESSION['Email'] = $email;
					$_SESSION['FirstName'] = $first_name;
					$_SESSION['LastName'] = $last_name;
					$_SESSION['AccountType'] = $account_type;
					$_SESSION['ImageLink'] = $image_link;
					$_SESSION['GSTIN'] = $gstin;
					$_SESSION['ADDRESS'] = $address;
					$_SESSION['Wallet'] = $wallet;
					$_SESSION['User_ID'] = $user_id;
					$_SESSION['Member_Plan'] = $member_plan;
					echo '<script language="javascript">';
					echo 'location.href="../dashboard.php";';
					echo '</script>';
				}
				else {
					echo '<script language="javascript">';
					echo 'alert("Email is not registered!");';
					echo 'location.href="../index.php";';
					echo '</script>';
				}
			}
		}
	}
?>