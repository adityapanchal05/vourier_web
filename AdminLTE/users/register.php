<?php

  include '../conn.php';
  require_once "config.php";
	$loginURL = $gClient->createAuthUrl();

  if($_POST) {
      if(isset($_POST['register'])) {
        registerUser();
      }
  }

  function registerUser() {
   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";
*/
    $email = $_POST['Email'];
    $password = $_POST['password'];
    $First_Name = $_POST['First_Name'];
    $Last_Name = $_POST['Last_Name'];
    $gstin = $_POST['GSTIN'];
    $address = $_POST['ADDRESS'];

   // $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

   /* if(!$conn) {
    die("Connection Failed :".mysqli_connect_error());
    }
    else {*/

      $idForThisUser = "SELECT * from user_ids";
      $result = Opencon() -> query($idForThisUser);
      if($result->num_rows>0) {
      	while($row = $result->fetch_assoc()) {
      		$current_id = $row['current'];
      	}
      }

      $updatedID = $current_id + 1;
      $updateCurrentID = Opencon()->query("UPDATE user_ids SET current = '".$updatedID."'");
      	

      $result = Opencon()->query("INSERT INTO customer_details(Email, RegistrationType, Password, User_ID, FirstName, LastName, AccountType, GSTIN, ADDRESS, Priority_Operator, Wallet, Member_Plan, Manifest_Number) VALUES ('$email', 'SIMPLE', '$password', '$updatedID', '$First_Name', '$Last_Name', 'INDIVIDUAL', '$gstin', '$address', 'FedEx', '0.0', 'INDIVIDUAL', '0')");

        if($result) {
          echo '<script language="javascript">';
          echo 'alert("Registration Successful. Login to continue!");';
          echo 'location.href="../index.php";';
          echo '</script>';
        }
        else {
          echo '<script language="javascript">';
          echo 'alert("Email Address already registered.");';
          echo 'location.href="register.php";';
          echo '</script>';
        }
   // }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<title>Logistics Company | Log in</title>
  	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../index.php"><b>Logistics </b>Company</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">SIGN UP</p>

    <form method="post" enctype="multipart/form-data">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="Email" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" id="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Confirm Password" id="confirm_password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div>
        <input type="text" name="First_Name" style="width: 45%; float: left;" class="form-control" placeholder="First Name" required>
        <input type="text" name="Last_Name"  style="width: 45%; float: right;" class="form-control" placeholder="Last Name" required>
      </div><br><br><br>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="radio" name="acType" value="Individual" id="Individual" checked> Individual
            </label>
            &nbsp&nbsp&nbsp
            <label>
              <input type="radio" name="acType" value="Business" id="Business"> Business
            </label>
          </div><br>
        </div>
        <div id="show-me" style="display: none;">
          <input type="text" name="GSTIN" id="gst" style="width: 92%; margin: 0 auto;" class="form-control" placeholder="GSTIN"><br>
          <textarea class="form-control" rows="4" cols="2" id="addr" name="ADDRESS" placeholder="ADDRESS" style="width: 92%; margin: 0 auto;"></textarea>
        </div><br>
        <!-- /.col -->
          <button type="submit" name="register" class="btn btn-primary btn-block btn-flat" style="margin: 0 auto; width: 34%;">Sign Up</button>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <button class="btn btn-block btn-social btn-google btn-flat" onclick="window.location = '<?php echo $loginURL ?>';"><i class="fa fa-google-plus"></i> Sign up using Google</button>
    </div>
    <!-- /.social-auth-links -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyBaXmqOmmbn75HKFgZVtM7bROttl0dYzuY",
    authDomain: "shipcustomer-af1f7.firebaseapp.com",
    databaseURL: "https://shipcustomer-af1f7.firebaseio.com",
    projectId: "shipcustomer-af1f7",
    storageBucket: "shipcustomer-af1f7.appspot.com",
    messagingSenderId: "699864048209",
    appId: "1:699864048209:web:9cd6f0cb941cb94e610e70"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
<script type="text/javascript">
  function GoogleLogin() {
    //first of all create google provider object

    var provider=new firebase.auth.GoogleAuthProvider();
    //Login with popup window
    firebase.auth().signInWithPopup(provider).then(function () {
        //code executes after successful login

        window.location="index.php";
    }).catch(function (error) {
        var errorMessage=error.message;
        alert(errorMessage);
    });
}
</script>

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script>
$("input[name='acType']").click(function () {
    $('#show-me').css('display', ($(this).val() === 'Business') ? 'block':'none');
    if($("#gst").is(":visible")){
    $("#gst").prop("required", true);
    $("#addr").prop("required", true);
  } else{
    $("#gst").prop("required", false);
    $("#addr").prop("required", false);
  }
});
</script>


</body>
</html>
