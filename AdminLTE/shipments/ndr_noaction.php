<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | Activities</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="recharge.php" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="buy_plans.php" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a href="pages/widgets.html">
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="support.php"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding-left: 0px;">
    	<div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding-left: 0px;">
        
        <div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 2px;">

          <p align="left" style="color: black; font-size: 20px; padding: 17px; margin-bottom: -10px; margin-top: -9px; display: inline-block; float: left;">Shipments</p>
          
          <button class="order-button" onclick="window.location.href='tracking'" style="width: 12%; padding: 14px; padding-bottom: 2px;"><i class="fa fa-safari"></i>  Tracking   <span><p style="display: inline-block; background-color: #f05050; width: 15%; border-radius: 25px; color: white;">0</p></span></button>

          <button class="order-button" onclick="window.location.href='ndr-pending'" style="width: 12%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: none; padding: 14px; padding-bottom: 2px;"><i class="fa fa-rocket"></i>  NDR  <span><p style="display: inline-block; background-color: #ff902b; width: 15%; border-radius: 25px; color: white;">0</p></span></button>

          <button class="order-button" onclick="window.location.href='rto-initiated'" style="width: 12%; padding: 14px; padding-bottom: 2px;"><i class="fa fa-ambulance"></i>  RTO  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;">0</p></span></button>

        </div>

    	<div style="width: 100%; margin-top: 68px; min-width: 1500px;">
    	  <div style="width: 100%; float: left; background-color: #fafafa; padding-top: 5px; padding-bottom: 5px;">

             <div style="width: 27%; float: left; padding-left: 10px;">
              <button style="float: left; background-color: #fafafa; font-weight: 600; color: #333333; border: 1px solid #ccc; padding: 5px 21px; font-size: 14px; border-radius: 0 3px 3px 0;" onclick="window.location.href='ndr-pending'">Action Required</button>
              <button style="float: left; background-color: #fafafa; font-weight: 600; color: #333333; border: 1px solid #ccc; padding: 5px 21px; font-size: 14px; border-radius: 0 3px 3px 0;" onclick="window.location.href='ndr-closed'">Action Requested</button>
              <button style="float: left; background-color: #285fdb; font-weight: 600; color: #fff; border: 1px solid #1D99D9; padding: 5px 21px; font-size: 14px; border-radius: 0 3px 3px 0;" onclick="window.location.href='ndr-noaction'">Delivered/RTO</button>
             </div>

             <div style="width: 23%; float: left; padding-left: 10px;">
              <input type="text" name="searchPhoneNumber" style="border: 1px solid #CFD4D6; width: 92%; padding-left: 10px; height: 32px; border-radius: 3px; float: left;" placeholder="Search by Customer Phone Number">
              <button name="search_by_number" style="margin-left: -3px; padding-top: 3px; padding-bottom: 5px; float: left;"><i class="fa fa-search"></i></button>
             </div>

             <div style="width: 8%; float: left; margin-left: 10px;">
              <select style="float: left; height: 33px; line-height: 33px; border: 1px solid #CFD4D6; padding: 5px 10px; font-size: 14px; border-radius: 3px; width: 100%; color: #3a3f51;">
                <option>All Attempts</option>
                <option>Attempt 1</option>
                <option>Attempt 2</option>
                <option>Attempt 3</option>
              </select>
            </div>

            <div style="width: 10%; float: left; margin-left: 10px;">
              <select style="float: left; height: 33px; line-height: 33px; border: 1px solid #CFD4D6; padding: 5px 10px; font-size: 14px; border-radius: 3px; width: 100%; color: #3a3f51;">
                <option>All Shipment Status</option>
                <option>Delivered</option>
                <option>RTO</option>
              </select>
            </div>

            <div style="width: 14%; float: left; margin-left: 10px;">
              <select style="float: left; height: 33px; line-height: 33px; border: 1px solid #CFD4D6; padding: 5px 10px; font-size: 14px; border-radius: 3px; width: 100%; color: #3a3f51;">
                <option>All NDR Reasons</option>
                <option>Customer Not Contactable</option>
                <option>Address Incomplete/Incorrect</option>
                <option>COD amount not ready</option>
                <option>Customer requested future delivery</option>
                <option>Customer requested self pickup</option>
                <option>Customer refused delivery</option>
                <option>Auto Reattempt</option>
                <option>Door/Premises/Office Closed</option>
                <option>Others</option>
              </select>
            </div>

            <div style="width: 13%; float: right;">
            	<button class="btn btn-info" style="background-color: #285fdb;">Activate NDR Buyer Flow</button>
            </div>

          </div>	
    	</div>

    	<div style="width: 100%; float: left; min-width: 1500px; background-color: #eeeeee; margin-top: 10px;">
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>NDR RAISED</b></p><span style="float: right;"><a href="#" style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -32px; margin-right: 6px;"><i class="fa fa-calendar"></i></a></span>
    		</div>
    		<div style="float: left; width: 8.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>CHANNEL</b></p><span style="float: right;"><button style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -33px;"><i class="fa fa-filter"></i></button></span>
    		</div>
    		<div style="float: left; width: 9.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>ORDER ID</b></p><span style="float: right;"><button style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -33px;"><i class="fa fa-filter"></i></button></span>
    		</div>
        	<div style="float: left; width: 10.5%; border-right: 1px solid white; font-size: 15px;">
          		<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>PRODUCT DETAILS</b></p>
        	</div>
        	<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
          		<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>PAYMENT</b></p>
        	</div>
        	<div style="float: left; width: 10.5%; border-right: 1px solid white; font-size: 15px;">
          		<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>CUSTOMER DETAILS</b></p>
        	</div>
    		<div style="float: left; width: 10.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>SHIPPING DETAILS</b></p><span style="float: right;"><button style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -33px;"><i class="fa fa-filter"></i></button></span>
    		</div>
    		<div style="float: left; width: 10.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>CURRENT STATUS</b></p>
    		</div>
    		<div style="float: left; width: 11.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>NON DELIVERY INFO</b></p>
    		</div>
    		<div style="float: left; width: 7.5%; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>ACTION TAKEN</b></p>
    		</div>
    	</div>

    	<div style="width: 100%;">
    		<img src="images/no_data.png" style="margin-top: 25px;">
    		<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>
        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>