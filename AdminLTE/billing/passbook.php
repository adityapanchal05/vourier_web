<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | Activities</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
  include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="recharge.php" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="buy_plans.php" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a href="pages/widgets.html">
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>

        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="support.php"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding-left: 0px;">
    	<div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding-left: 0px;">
        
        <div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 2px;">

          <p align="left" style="color: black; font-size: 20px; padding: 17px; margin-bottom: -10px; margin-top: -9px; display: inline-block; float: left;">Billing</p>

          <button class="order-button" onclick="window.location.href='shipping-charges'" style="width: 13%; padding: 14px;"><i class="fa fa-gears"></i>  Shipping Charges  </button>
          
          <button class="order-button" onclick="window.location.href='weight-reconciliation'" style="width: 13%; padding: 14px;"><i class="fa fa-balance-scale"></i>  Weight Reconciliation  </button>
          
          <button class="order-button" onclick="window.location.href='cod-remittance'" style="width: 11%; padding: 14px;"><i class="fa fa-fighter-jet"></i>  Remittance Logs  </button>

          <button class="order-button" onclick="window.location.href='recharge-logs'" style="width: 9%; padding: 14px;"><i class="fa fa-bolt"></i>  Recharge Logs  </button>

          <button class="order-button" onclick="window.location.href='onhold-reconciliation'" style="width: 13%; padding: 14px;"><i class="fa fa-credit-card"></i>  On-Hold Reconciliation  </button>

          <button class="order-button" onclick="window.location.href='invoices'" style="width: 7%; padding: 14px;"><i class="fa fa-file-text"></i>  Invoices  </button>

          <button class="order-button" onclick="window.location.href='passbook'" style="width: 7%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: none; padding: 14px;"><i class="fa fa-book"></i>  Passbook  </button>
        </div>

    	<div style="width: 100%; margin-top: 68px; min-width: 1500px; padding-left: 20px;">
    	  <div style="width: 100%; float: left; background-color: #fafafa; padding-top: 5px; padding-bottom: 5px;">
            <div style="width: 25%; float: left; border-right: 1px solid rgba(0, 0, 0, 0.12); border: 1px solid rgba(0, 0, 0, 0.12); padding: 5px; border-radius: 3px; background-color: white;">
              <a href="#" style="color: #656565; text-decoration: none; float: left; cursor: auto;">DD MM, YYYY - DD MM, YYYY</a>
              <a href="#" style="color: #656565; border: none; float: right; background-color: transparent; outline: none;"><i class="fa fa-calendar"></i></a>
            </div>

            <div style="width: 23%; float: left; padding-left: 10px;">
              <input type="text" name="searchManifestId" style="border: 1px solid #CFD4D6; width: 92%; padding-left: 10px; height: 32px; border-radius: 3px; float: left;" placeholder="Search by Order ID">
              <button name="search_by_id" style="margin-left: -3px; padding-top: 3px; padding-bottom: 5px; float: left;"><i class="fa fa-search"></i></button>
             </div>

            <div style="width: 25%; float: left; margin-left: 10px;">
              <select style="float: left; height: 33px; line-height: 33px; border: 1px solid #CFD4D6; padding: 5px 10px; font-size: 14px; border-radius: 3px; width: 90%; color: #3a3f51;">
                <option>Category</option>
                <option>Freight Charge</option>
                <option>Freight Charge Reversed</option>
                <option>Excess Weight Charge</option>
                <option>RTO Freight Charge</option>
                <option>RTO Freight Reversed</option>
                <option>LC Credit</option>
                <option>Cancelled</option>
                <option>COD Charge</option>
                <option>COD Charge Reversed</option>
                <option>Lost Credit</option>
                <option>RTO Excess Weight Charge</option>
                <option>Damaged Credit</option>
                <option>RTO Excess Freight Reversed</option>
              </select>
            </div>

        </div>	

        <div style="width: 100%; margin-top: 68px; min-width: 1500px; padding-left: 10px;">
          <div style="width: 100%; float: left; margin-top: 5px;">
            <div style="width: 32.5%; float: left; background-color: #285fdb;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;">Current Usable Balance</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;"><i class="fa fa-inr"></i> 0</p>
            </div>
            <div style="width: 32.5%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;">Balance On Hold</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;"><i class="fa fa-inr"></i> 0</p>
            </div>
            <div style="width: 32.5%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;">Total Balance<span><button title="Sum of Usable + On-Hold Amount" style="background-color: transparent; border: none; outline: none;"><i class="fa fa-exclamation-circle"></i></button></span></p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px;"><i class="fa fa-inr"></i> 0</p>
            </div>
          </div> 
      </div>


    	</div>

    	<div style="width: 100%; float: left; min-width: 1500px; padding-left: 20px; background-color: #fafafa; margin-top: 10px; border: 3px solid #eeeeee;">
        <div style="float: left; width: 12.5%; border-right: 1px solid white; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px; border-right: 3px solid #eeeeee;"><b>DATE</b></p>
        </div>
        <div style="float: left; width: 13.5%; border-right: 1px solid white; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px; border-right: 3px solid #eeeeee;"><b>AWB CODE</b></p>
        </div>
        <div style="float: left; width: 11.5%; border-right: 1px solid white; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px; border-right: 3px solid #eeeeee;"><b>CATEGORY</b></p>
        </div>
        <div style="float: left; width: 11.5%; border-right: 1px solid white; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px; border-right: 3px solid #eeeeee;"><b>CREDIT (<i class="fa fa-inr"></i>)</b></p>
        </div>
        <div style="float: left; width: 11.5%; border-right: 1px solid white; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px; border-right: 3px solid #eeeeee;"><b>DEBIT (<i class="fa fa-inr"></i>)</b></p>
        </div>
        <div style="float: left; width: 12.5%; font-size: 15px;">
          <p align="left" style="padding: 10px; margin-bottom: 0px;"><b>DESCRIPTION</b></p>
        </div>
      </div>

      <div style="width: 100%;">
    		<img src="images/no_data.png" style="margin-top: 25px;">
    		<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>
       </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>