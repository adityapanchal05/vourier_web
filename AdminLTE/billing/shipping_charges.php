<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | Activities</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
   include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">

  <?php include '../aside.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding-left: 0px;">
    	<div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding-left: 0px;">
        
        <div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 2px;">

          <p align="left" style="color: black; font-size: 20px; padding: 17px; margin-bottom: -10px; margin-top: -9px; display: inline-block; float: left;">Billing</p>

          <button id="processing_orders" class="order-button" onclick="window.location.href='shipping-charges'" style="width: 13%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: none; padding: 14px;"><i class="fa fa-gears"></i>  Shipping Charges  </button>
          
          <button id="readytoship_orders" class="order-button" onclick="window.location.href='weight-reconciliation'" style="width: 13%; padding: 14px;"><i class="fa fa-balance-scale"></i>  Weight Reconciliation  </button>
          
          <button id="pickups_orders" class="order-button" onclick="window.location.href='cod-remittance'" style="width: 10%; padding: 14px;"><i class="fa fa-fighter-jet"></i>  Remittance Logs  </button>

          <button id="returns_orders" class="order-button" onclick="window.location.href='recharge-logs'" style="width: 9%; padding: 14px;"><i class="fa fa-bolt"></i>  Recharge Logs  </button>

          <button id="all_orders" class="order-button" onclick="window.location.href='onhold-reconciliation'" style="width: 13%; padding: 14px;"><i class="fa fa-credit-card"></i>  On-Hold Reconciliation  </button>

          <button id="all_orders" class="order-button" onclick="window.location.href='invoices'" style="width: 7%; padding: 14px;"><i class="fa fa-file-text"></i>  Invoices  </button>

          <button id="all_orders" class="order-button" onclick="window.location.href='passbook'" style="width: 7%; padding: 14px;"><i class="fa fa-book"></i>  Passbook  </button>
        </div>

    	<div style="width: 100%; margin-top: 68px; min-width: 1500px; padding-left: 20px;">

          <div style="width: 100%; float: left;">
            <div style="width: 19%; float: left; background-color: #285fdb;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px">Total Freight Charges</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px"><i class="fa fa-inr"></i> 0.00</p>
            </div>
            <div style="width: 19%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px">Billed Freight Charges</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px"><i class="fa fa-inr"></i> 0.00</p>
            </div>
            <div style="width: 19%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px">Unbilled Freight Charges</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px"><i class="fa fa-inr"></i> 0.00</p>
            </div>
            <div style="width: 19%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px">Total On-Hold Amount</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px"><i class="fa fa-inr"></i> 0.00</p>
            </div>
            <div style="width: 19%; float: left; background-color: #285fdb; margin-left: 10.5px;">
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px">Invoice Due Amount</p>
              <p style="width: 100%; color: white; font-size: 16px; margin-bottom: 5px"><i class="fa fa-inr"></i> 0.00</p>
            </div>
          </div>

    	  <div style="width: 100%; float: left; background-color: #fafafa; padding-top: 5px; padding-bottom: 5px;">
             <div style="width: 30%; float: left; margin-top: 10px;">
              <input type="text" name="searchManifestId" style="border: 1px solid #CFD4D6; width: 81%; padding-left: 10px; height: 30px; border-radius: 3px; float: left;" placeholder="Search by Order ID">
              <button name="search_by_id" style="margin-left: -3px; padding-top: 3px; padding-bottom: 3px; float: left;"><i class="fa fa-search"></i></button>
             </div>
          </div>	
    	</div>

    	<div style="width: 100%; float: left; min-width: 1500px; padding-left: 20px; background-color: #eeeeee; margin-top: 10px;">
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>ORDER ID</b></p><br><br>
    		</div>
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>AWB NUMBER</b></p><br><br>
    		</div>
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>COURIER</b></p><span style="float: right;"><button style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -33px;"><i class="fa fa-filter"></i></button></span><br><br>
    		</div>
    		<div style="float: left; width: 10.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>SHIPMENT STATUS</b></p><span style="float: right;"><button style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -33px;"><i class="fa fa-filter"></i></button></span><br><br>
    		</div>
    		<div style="float: left; width: 11.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>AWB ASSIGNED DATE</b></p><span style="float: right;"><a href="#" style="float: right; color: #285fdb; background-color: transparent; border: none; outline: none; margin-top: -32px; margin-right: 6px;"><i class="fa fa-calendar"></i></a></span><br><br>
    		</div>
    		<div style="float: left; width: 8.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>APPLIED WEIGHT CHARGES (<i class="fa fa-inr"></i>)</b></p><br>
    		</div>
    		<div style="float: left; width: 8.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>EXCESS WEIGHT CHARGES (<i class="fa fa-inr"></i>)</b></p><br>
    		</div>
    		<div style="float: left; width: 6.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>ON HOLD AMOUNT (<i class="fa fa-inr"></i>)</b></p><br>
    		</div>
    		<div style="float: left; width: 8.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>TOTAL FREIGHT CHARGES (<i class="fa fa-inr"></i>)</b></p><br>
    		</div>
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>ENTERED WEIGHT & DIMENSIONS</b></p>
    		</div>
    		<div style="float: left; width: 7.5%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>CHARGED WEIGHT & DIMENSIONS</b></p>
    		</div>
    		<div style="float: left; width: 8.5%; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px"><b>VIEW TRANSACTIONS DETAILS</b></p>
    		</div>
    	</div>

    	<div style="width: 100%;">
    		<img src="images/no_data.png" style="margin-top: 25px;">
    		<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>
        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include '../footer.php';?>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>
