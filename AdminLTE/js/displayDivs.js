function displayIndividualForm() {
	document.getElementById('individual_form').style.display = 'block';
	document.getElementById('business-form').style.display = 'none';
}
function displayBusinessForm() {
	document.getElementById('individual_form').style.display = 'none';
	document.getElementById('business-form').style.display = 'block';
}
function displayDomesticPlans() {
	document.getElementById('domestic_plans').style.display = 'block';
	document.getElementById('international_plans').style.display = 'none';
}
function displayInternationalPlans() {
	document.getElementById('domestic_plans').style.display = 'none';
	document.getElementById('international_plans').style.display = 'block';
}
function displayLitePlans() {
	document.getElementById('lite_plans').style.display = 'block';
	document.getElementById('basic_plans').style.display = 'none';
	document.getElementById('advanced_plans').style.display = 'none';
	document.getElementById('pro_plans').style.display = 'none';
}
function displayBasicPlans() {
	document.getElementById('lite_plans').style.display = 'none';
	document.getElementById('basic_plans').style.display = 'block';
	document.getElementById('advanced_plans').style.display = 'none';
	document.getElementById('pro_plans').style.display = 'none';
}
function displayAdvancedPlans() {
	document.getElementById('lite_plans').style.display = 'none';
	document.getElementById('basic_plans').style.display = 'none';
	document.getElementById('advanced_plans').style.display = 'block';
	document.getElementById('pro_plans').style.display = 'none';
}
function displayProPlans() {
	document.getElementById('lite_plans').style.display = 'none';
	document.getElementById('basic_plans').style.display = 'none';
	document.getElementById('advanced_plans').style.display = 'none';
	document.getElementById('pro_plans').style.display = 'block';
}
function verifyWithoutOtp() {
	document.getElementById('verify_with_otp').style.display = 'none';
	document.getElementById('verify_without_otp').style.display = 'block';
}
function verifyWithOtp() {
	document.getElementById('verify_without_otp').style.display = 'none';
	document.getElementById('verify_with_otp').style.display = 'block';
}
function sole_proprietor_verifyWithoutOtp() {
	document.getElementById('sole_proprietor_verify_with_otp').style.display = 'none';
	document.getElementById('sole_proprietor_verify_without_otp').style.display = 'block';
	return false;
}
function sole_proprietor_verifyWithOtp() {
	document.getElementById('sole_proprietor_verify_without_otp').style.display = 'none';
	document.getElementById('sole_proprietor_verify_with_otp').style.display = 'block';
	return false;
}
function displayOrderDiv() {
	document.getElementById('orders').style.display = 'block';
	document.getElementById('revenue').style.display = 'none';
}
function displayRevenueDiv() {
	document.getElementById('orders').style.display = 'none';
	document.getElementById('revenue').style.display = 'block';
}