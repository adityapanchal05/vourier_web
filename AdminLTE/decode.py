import sys
from pdf2image import convert_from_path
from PIL import Image
from pyzbar.pyzbar import decode

try:
	pages = convert_from_path('Label.pdf')
	pages[0].save("label.png")
except Exception as error:
	print(error)

data = decode(Image.open('label.png'))
barcode = data[0][0]
barcode = barcode.decode()
print(barcode)