<?php


include 'conn.php';
function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
if(isset($_GET['plan'])) {
	$plan_type = $_GET['plan'];
}

if(strcmp($plan_type, 'BASIC')==0) {
	$money_needed = 1000;
}
else if(strcmp($plan_type, 'ADVANCED')==0) {
	$money_needed = 2000;
}
else if(strcmp($plan_type, 'PRO')==0) {
	$money_needed = 3000;
}

if($wallet >= $money_needed) {

	$new_wallet = $wallet - $money_needed;

	date_default_timezone_set("Asia/Calcutta");
    $start_date = date('Y-m-d');
    $end_date = date('Y-m-d' ,strtotime($start_date.' + 30 days '));

    $result = Opencon()->query("UPDATE customer_details SET Wallet = '".$new_wallet."' WHERE Email = '".$Email."' ");
    if($result) {
    	$result = Opencon()->query("UPDATE customer_details SET Member_Plan = '".$plan_type."' WHERE Email = '".$Email."' ");
	    if($result) {
	    	$result = Opencon()->query("UPDATE customer_details SET Plan_Start = '".$start_date."' WHERE Email = '".$Email."' ");
	    	if($result) {
	    		$result = Opencon()->query("UPDATE customer_details SET Plan_End = '".$end_date."' WHERE Email = '".$Email."' ");
	    		echo '<script language="javascript">';
	    		echo 'alert("Plan Activated!");';
	    		echo 'location.href="dashboard.php";';
	    		echo '</script>';
	    	}
	    }
    }
}
else {
	echo '<script language="javascript">';
    echo 'alert("Not enough money. Please recharge.");';
    echo 'location.href="recharge.php";';
    echo '</script>';
}

?>