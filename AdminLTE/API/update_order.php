<?php


include '../conn.php';
/*$dbhost = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "logistics_v2";


Opencon() = mysqli_connect($dbhost, $db_user, $db_password, $db_name);
*/

//if(Opencon()) {
	if(isset($_POST['Order_ID'])) {

		$getToken = "SELECT * FROM shiprocket_token WHERE serial_number = '0000000001'";
		$result = Opencon()->query($getToken);
		if($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$token = $row['value'];
			}
		}

        $url = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc';

        date_default_timezone_set("Asia/Calcutta");
		$date_today = date("d/M/Y, h:i:s A");
        $date_created = date('Y-m-d');
        $time_stamp = date('Y-m-d h:i');

		$Order_ID = $_POST['Order_ID'];

		$fromName = $_POST['fromName'];
		$fromName = str_replace("'", "''", $fromName);

		$fromNumber = $_POST['fromNumber'];

		$fromAddress1 = $_POST['fromAddressComment'];
		$fromAddress1 = str_replace("'", "''", $fromAddress1);

		$fromAddress2 = $_POST['fromAddress'];
		$fromAddress2 = str_replace("'", "''", $fromAddress2);

		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];
		$fromPincode = $_POST['fromPincode'];

		$addressAlias = $_POST['addressAlias'];
		$addressAlias = str_replace("'", "''", $addressAlias);

		$toPincode = $_POST['toPincode'];

		$weight = $_POST['weight'];
		$length = $_POST['length'];
		$breadth = $_POST['breadth'];
		$height = $_POST['height'];
		$price = $_POST['price'];

		$HUB_ID = $_POST['HUB_ID'];

        $details = $_POST['DETAILS'];

        if(strcmp($price, "NOT AVAILABLE")==0) {
            $price = 100;
        }

		//get city_state
        $cityState = "SELECT * FROM pincode_database WHERE pin_code = '".$toPincode."'";
        $result = Opencon() -> query($cityState);
        if($result->num_rows > 0) {
        	while ($row = $result->fetch_assoc()) {
        		$city = $row['City'];
        		$state = $row['State'];
        	}
        }

		$ch = curl_init($url);
        $data = array(
                "order_id" => $Order_ID,
                "order_date" => $time_stamp,
                "pickup_location" => "Mini Cam",
                "channel_id" => "0",
                "comment" => "SAMPLE",
                "billing_customer_name" => $fromName,
                "billing_last_name" => "",
                "billing_address" => $fromAddress1,
                "billing_address_2" => $city,
                "billing_city" => $city,
                "billing_pincode" => $fromPincode,
                "billing_state" => $state,
                "billing_country" => "India",
                "billing_email" => "random.smaple@gmail.com",
                "billing_phone" => $fromNumber,
                "shipping_is_billing" => true,

                "order_items" => [
                  array(
                    "name" => "PACKAGE",
                    "sku" => "DOC",
                    "units" => 1,
                    "selling_price" => $price,
                    "discount" => "",
                    "tax" => "",
                    "hsn" => ""
                  )
                ],
                "payment_method" => "Prepaid",
                "shipping_charges" => 0,
                "giftwrap_charges" => 0,
                "transaction_charges" => 0,
                "total_discount" => 0,
                "sub_total" => $price,
                "length" => $length,
                "breadth" => $breadth,
                "height" => $height,
                "weight" => $weight
            );
            $payload = json_encode($data);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

            echo $result;
            curl_close($ch);
            
            $json_result = json_decode($result, true);

            $shipment_id = $json_result['shipment_id'];

            $result = Opencon()->query("UPDATE customer_orders SET Sender_Name = '".$fromName."', sender_mobile = '".$fromNumber."', sender_address_line_1 = '".$fromAddress1."', sender_address_line_2 = '".$fromAddress2."', sender_pincode = '".$fromPincode."', Latitude = '".$latitude."', Longitude = '".$longitude."', Weight = '".$weight."', length = '".$length."', breadth = '".$breadth."', height = '".$height."', Price = '".$price."', HUB_ID = '".$HUB_ID."', product_details = '".$details."' WHERE Order_ID = '".$Order_ID."' ");

            if($result) {
            	if(strcmp($addressAlias, 'NOT AVAILABLE')==0) {
            		$response->order_status = "200";
            		$response->new_order_id = $Order_ID;

            		$responseJSON = json_encode($response);
            		echo $responseJSON;
            	}
            }
            else {
            	echo(mysqli_error(Opencon()));
            }
        }
        else {
        	echo "WRONG";
        }
   // }
   // else {
    	//echo "404";
//}

?>
