<?php

include '../conn.php';

/*$dbhost = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "logistics_v2";

Opencon() = mysqli_connect($dbhost, $db_user, $db_password, $db_name);
*/
//if(Opencon()) {
	if(isset($_POST['userID'])) {

		$getToken = "SELECT * FROM shiprocket_token WHERE serial_number = '0000000001'";
		$result = Opencon()->query($getToken);
		if($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$token = $row['value'];
			}
		}

        $url = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc';

        date_default_timezone_set("Asia/Calcutta");
		$date_today = date("d/M/Y, h:i:s A");
        $date_orderID = date("ymd");
        $date_created = date('Y-m-d');
        $time_stamp = date('Y-m-d h:i');

        $counterValue = "SELECT * FROM daily_counter";
        $result = Opencon() -> query($counterValue);
        if($result->num_rows > 0) {
        	while ($row = $result->fetch_assoc()) {
        		$counter_value = $row['value'];
        	}
        }

		$Order_ID = 'SD'.$date_orderID.$counter_value;
		$new_counter_value = $counter_value + 1;
		$result = Opencon()->query("UPDATE daily_counter SET value = '".$new_counter_value."' WHERE value = '".$counter_value."' ");

		$Email = $_POST['userID'];

		$fromName = $_POST['fromName'];
		$fromName = str_replace("'", "''", $fromName);

		$fromNumber = $_POST['fromNumber'];

		$fromAddress1 = $_POST['fromAddressComment'];
		$fromAddress1 = str_replace("'", "''", $fromAddress1);

		$fromAddress2 = $_POST['fromAddress'];
		$fromAddress2 = str_replace("'", "''", $fromAddress2);

		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];
		$fromPincode = $_POST['fromPincode'];

		$addressAlias = $_POST['addressAlias'];
		$addressAlias = str_replace("'", "''", $addressAlias);

		$toName = $_POST['toName'];
		$toName = str_replace("'", "''", $toName);

		$toNumber = $_POST['toNumber'];

		$toAddress1 = $_POST['toAddress'];
		$toAddress1 = str_replace("'", "''", $toAddress1);

		$toPincode = $_POST['toPincode'];

		$weight = $_POST['weight'];
		$length = $_POST['length'];
		$breadth = $_POST['breadth'];
		$height = $_POST['height'];
		$price = $_POST['price'];

		$HUB_ID = $_POST['HUB_ID'];

        $details = $_POST['DETAILS'];

        if(strcmp($price, "NOT AVAILABLE")==0) {
            $price = 100;
        }

		//get city_state
        $cityState = "SELECT * FROM pincode_database WHERE pin_code = '".$toPincode."'";
        $result = Opencon() -> query($cityState);
        if($result->num_rows > 0) {
        	while ($row = $result->fetch_assoc()) {
        		$city = $row['City'];
        		$state = $row['State'];
        	}
        }

		$ch = curl_init($url);
        $data = array(
                "order_id" => $Order_ID,
                "order_date" => $time_stamp,
                "pickup_location" => "Mini Cam",
                "channel_id" => "0",
                "comment" => "SAMPLE",
                "billing_customer_name" => $toName,
                "billing_last_name" => "",
                "billing_address" => $toAddress1,
                "billing_address_2" => $city,
                "billing_city" => $city,
                "billing_pincode" => $toPincode,
                "billing_state" => $state,
                "billing_country" => "India",
                "billing_email" => "random.smaple@gmail.com",
                "billing_phone" => $toNumber,
                "shipping_is_billing" => true,
                "shipping_customer_name" => "",
                "shipping_last_name" => "",
                "shipping_address" => "",
                "shipping_address_2" => "",
                "shipping_city" => "",
                "shipping_pincode" => "",
                "shipping_country" => "",
                "shipping_state" => "",
                "shipping_email" => "",
                "shipping_phone" => "",
                "order_items" => [
                  array(
                    "name" => "PACKAGE",
                    "sku" => "DOC",
                    "units" => 1,
                    "selling_price" => $price,
                    "discount" => "",
                    "tax" => "",
                    "hsn" => ""
                  )
                ],
                "payment_method" => "Prepaid",
                "shipping_charges" => 0,
                "giftwrap_charges" => 0,
                "transaction_charges" => 0,
                "total_discount" => 0,
                "sub_total" => $price,
                "length" => $length,
                "breadth" => $breadth,
                "height" => $height,
                "weight" => $weight
            );
            $payload = json_encode($data);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

            echo $result;
            curl_close($ch);
            
            $json_result = json_decode($result, true);

            $shipment_id = $json_result['shipment_id'];

            $result = Opencon()->query("INSERT INTO customer_orders(Email, Order_ID, Shipment_ID, Order_Type, product_details, order_date, Channel, Status, Sender_Name, sender_mobile, sender_address_line_1, sender_address_line_2, sender_pincode, Customer_Name, customer_mobile, customer_address_line_1, customer_pincode, length, breadth, height, Weight, Latitude, Longitude, COD, Price, new_invoice_status, HUB_ID, date_created) VALUES ('$Email', '$Order_ID', '$shipment_id', 'INDIVIDUAL - DOCUMENT', '$details', '$date_today', 'CUSTOM', 'PROCESSING', '$fromName', '$fromNumber', '$fromAddress1', '$fromAddress2', '$fromPincode', '$toName', '$toNumber', '$toAddress1', '$toPincode', '$length', '$breadth', '$height', '$weight', '$latitude', '$longitude', 'No', '$price', 'NEW', '$HUB_ID', '$date_created')");

            if($result) {
            	if(strcmp($addressAlias, 'NOT AVAILABLE')==0) {
            		$response->order_status = "200";
            		$response->new_order_id = $Order_ID;

            		$responseJSON = json_encode($response);
            		echo $responseJSON;
            	}
            	else {
            		$result = Opencon()->query("INSERT INTO customer_address_mobile(Address_Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode, Address_Latitude, Address_Longitude) VALUES ('$Email', '$addressAlias', '$fromName', '$fromNumber', '$fromAddress1', '$fromAddress2', '$fromPincode', '$latitude', '$longitude')");
            		if ($result) {
            			$response->order_status = "200";
            			$response->new_order_id = $Order_ID;

            			$responseJSON = json_encode($response);
            			echo $responseJSON;
            		}
            		else {
            			echo "202";
            		}
            	}
            }
            else {
            	echo(mysqli_error(Opencon()));
            }
        }
        else {
        	echo "WRONG";
        }
    //}
    //else {
    	//echo "404";
//}

?>
