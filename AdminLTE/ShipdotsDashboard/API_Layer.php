<?php

include '../conn.php';

/*$dbhost = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "logistics_v2";

$conn = mysqli_connect($dbhost, $db_user, $db_password, $db_name);
*/
$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyMjY5MSwiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NjYzMDY5NzYsImV4cCI6MTU2NzE3MDk3NiwibmJmIjoxNTY2MzA2OTc2LCJqdGkiOiI5Tm12VTZCemFQZUwxVkZ1In0.90eG6uXzOco6nAt5mN7A03S5MVG0VgGwmTTKCxYac2s';

//get courier details
if(isset($_POST['fromName'])) {

	$url = 'https://apiv2.shiprocket.in/v1/external/courier/serviceability/';

	$fromAddress = $_POST['fromAddress'];
	$toPincode = $_POST['toPincode'];
	$fromPincode = $_POST['fromPincode'];
	$length = $_POST['length'];
	$breadth = $_POST['breadth'];
	$height = $_POST['height'];
	$weight = $_POST['weight'];
	$cod = 0;
	$price = $_POST['price'];

	if(strcmp($price, "NOT AVAILABLE")==0) {
		$price = 100;
	}

	$length = (int)$length;
	$breadth = (int)$breadth;
	$height = (int)$height;

	if($length <= 0) {
		$length = 0.5;
	}

	if($breadth <= 0) {
		$breadth = 0.5;
	}

	if($height <= 0) {
		$height = 0.5;
	}

	$ch = curl_init($url);
	$data = array(
		"pickup_postcode" => (int)$fromPincode,
		"delivery_postcode" => (int)$toPincode,
		"length" => $length/100,
		"breadth" => $breadth/100,
		"height" => $height/100,
		"weight" => $weight,
		"cod" => 0,
		"declared_value" => (int)$price,
	);
	$payload = json_encode($data);

	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	echo $result;
 }

 //add order on shiprocket
 if(isset($_POST['SHIPROCKET'])) {

 	$url = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc';
 	$EmailID = $_POST['EmailID'];
 	$OrderID = $_POST['OrderID'];

 	$single_order_details = "SELECT * FROM order_details WHERE Email = '".$Email."' AND Order_ID = '".$OrderID."' ";
 	$result = Opencon() -> query($single_order_details);

 	if($result->num_rows > 0) {
 		while ($row = $result->fetch_assoc()) {
 			$order_date = $row['timestamp'];
 			$billing_customer_name = $row['Customer_Name'];
 			$billing_address = $row['customer_address_line_1'];
 			$billing_address_2 = $row['customer_address_line_2'];
 		}
 	}

 	$ch = curl_init($url);
 	$data = array(
			  "order_id" => "1206888",
			  "order_date" => "2018-05-08 12:23",
			  "pickup_location" => "Mini Cam",
			  "channel_id" => "0",
			  "comment" => "Reseller: M/S Lal Enterprises",
			  "billing_customer_name" => "John",
			  "billing_last_name" => "Doe",
			  "billing_address" => "#12, street 1",
			  "billing_address_2" => "Near Post office, Nehru Place",
			  "billing_city" => "New Delhi",
			  "billing_pincode" => "110016",
			  "billing_state" => "Delhi",
			  "billing_country" => "India",
			  "billing_email" => "john@doe.com",
			  "billing_phone" => "9876543210",
			  "shipping_is_billing" => true,
			  "shipping_customer_name" => "Test",
			  "shipping_last_name" => "Test",
			  "shipping_address" => "Test",
			  "shipping_address_2" => "Test",
			  "shipping_city" => "Test",
			  "shipping_pincode" => "110002",
			  "shipping_country" => "India",
			  "shipping_state" => "Delhi",
			  "shipping_email" => "www.ghost25@gmail.com",
			  "shipping_phone" => "8272846915",
			  "order_items" => [
			    array(
			      "name" => "Test Product 1",
			      "sku" => "abc123",
			      "units" => 1,
			      "selling_price" => "200",
			      "discount" => "",
			      "tax" => "",
			      "hsn" => ""
			    )
			  ],
			  "payment_method" => "Prepaid/COD",
			  "shipping_charges" => 0,
			  "giftwrap_charges" => 0,
			  "transaction_charges" => 0,
			  "total_discount" => 0,
			  "sub_total" => 4000,
			  "length" => 18,
			  "breadth" => 18,
			  "height" => 40,
			  "weight" => 2.4
		);
		$payload = json_encode($data);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
 }

?>