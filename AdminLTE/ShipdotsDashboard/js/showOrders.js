function showProcessingOrders() {
	document.getElementById('processing_orders_div').style.display = 'block';
	document.getElementById('readytoship_orders_div').style.display = 'none';
	document.getElementById('pickup_orders_div').style.display = 'none';
	document.getElementById('return_orders_div').style.display = 'none';
	document.getElementById('all_orders_div').style.display = 'none';
}

function showReadyToShipOrders() {
	document.getElementById('processing_orders_div').style.display = 'none';
	document.getElementById('readytoship_orders_div').style.display = 'block';
	document.getElementById('pickup_orders_div').style.display = 'none';
	document.getElementById('return_orders_div').style.display = 'none';
	document.getElementById('all_orders_div').style.display = 'none';
}

function showPickupOrders() {
	document.getElementById('processing_orders_div').style.display = 'none';
	document.getElementById('readytoship_orders_div').style.display = 'none';
	document.getElementById('pickup_orders_div').style.display = 'block';
	document.getElementById('return_orders_div').style.display = 'none';
	document.getElementById('all_orders_div').style.display = 'none';
}

function showReturnOrders() {
	document.getElementById('processing_orders_div').style.display = 'none';
	document.getElementById('readytoship_orders_div').style.display = 'none';
	document.getElementById('pickup_orders_div').style.display = 'none';
	document.getElementById('return_orders_div').style.display = 'block';
	document.getElementById('all_orders_div').style.display = 'none';
}

function showAllOrders() {
	document.getElementById('processing_orders_div').style.display = 'none';
	document.getElementById('readytoship_orders_div').style.display = 'none';
	document.getElementById('pickup_orders_div').style.display = 'none';
	document.getElementById('return_orders_div').style.display = 'none';
	document.getElementById('all_orders_div').style.display = 'block';
}