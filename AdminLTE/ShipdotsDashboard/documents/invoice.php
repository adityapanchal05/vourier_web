<?php
include '../conn.php';
function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    restore_error_handler();

if(isset($_GET['order'])) {
	$orderID = $_GET['order'];
}

	/*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    */if(!$conn) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {
    	$invoiceQuery = "SELECT * from customer_orders WHERE Email = '".$Email."' AND Order_ID = '".$orderID."'";
    	$result = $conn -> query($invoiceQuery);
    	if($result->num_rows > 0) {
    		while ($row = $result->fetch_assoc()) {
    			$order_date = $row['order_date'];
    			$channel = $row['Channel'];

    			$product_details = $row['product_details'];

    			$customer_name = $row['Customer_Name'];
    			$customer_mobile = $row['customer_mobile'];
    			$customer_address_line_1 = $row['customer_address_line_1'];
    			$customer_address_line_2 = $row['customer_address_line_2'];
    			$customer_pincode = $row['customer_pincode'];

    			$sender_name = $row['Sender_Name'];
    			$sender_mobile = $row['sender_mobile'];
    			$sender_address_line_1 = $row['sender_address_line_1'];
    			$sender_address_line_2 = $row['sender_address_line_2'];
    			$sender_pincode = $row['sender_pincode'];

    			$operator = $row['operator'];
    			$cod = $row['COD'];
    			$price = $row['Price'];
    			$awb = $row['AWB_Number'];

    			$order_type = $row['Order_Type'];
    		}
    	}
    	if(strcmp($cod, 'Yes')==0) {
    		$cod = 'Postpaid';
    	}
    	else {
    		$cod = 'Prepaid';
    	}
    	$invoiceNumber = $orderID;

    	$product_details_array = explode("<br>", $product_details);
    	if(strcmp($order_type, 'BUSINESS - PARCEL')==0) {
    		$product_name = $product_details_array[0];
    		$product_sku = $product_details_array[1];
    		$product_hsn = $product_details_array[2];
    		$product_qty = $product_details_array[3];
    		$product_tax = $product_details_array[4];
    		$product_discount = $product_details_array[5];

    		$name = explode(":", $product_name);
    		$hsn = explode(":", $product_hsn);
    		$tax = explode(":", $product_tax);
    		$qty = explode(":", $product_qty);
    		$discount = explode(":", $product_discount);

    	}
    	else {
    		$product_name = $product_details_array[0];
    		$product_qty = $product_details_array[1];
    		$product_sku = '';
    		$product_hsn = '';
    		$product_tax = '';
    		$product_discount = '';
    		$hsn[1] = '';
    		$tax[1] = '';
    		$discount[1] = '';

    		$name = explode(":", $product_name);
    		$qty = explode(":", $product_qty);
    	}

    	$total = $price;
    }

require('../FPDF/fpdf.php');

$pdf = new FPDF('P', 'mm', 'A4');
$pdf->AddPage();

$pdf->SetFont('Arial', 'B', 14);
$pdf->SetTextColor(64, 64, 64);
$pdf->Cell(0,25, $first_name.' '.$last_name, 0, 0, 'C');

$pdf->Line(10, 30, 200, 30);

$pdf->SetFont('Arial', '', 24);
$pdf->Cell(-190, 55, 'TAX INVOICE', 0, 0, 'C');

$pdf->Line(10, 44, 200, 44);

//Left
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(0.1, 85, 'SHIPPING ADDRESS:', 0, 0, 'L');

$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0.1, 100, $customer_name, 0, 0, 'L');
$pdf->Cell(0.1, 110, $customer_address_line_1, 0, 0, 'L');
$pdf->Cell(0.1, 120, $customer_address_line_2, 0, 0, 'L');
$pdf->Cell(0.1, 130, $customer_pincode, 0, 0, 'L');
$pdf->Cell(80, 140, 'Ph: '.$customer_mobile, 0, 0, 'L');
$pdf->SetDrawColor(160, 160, 160);
$pdf->Line(70, 48, 70, 93);

//Center
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-18, 85, 'SOLD BY:', 0, 0, 'C');

$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0.1, 100, $sender_name, 0, 0, 'L');
$pdf->Cell(0.1, 110, $sender_address_line_1, 0, 0, 'L');
$pdf->Cell(0.1, 120, $sender_address_line_2, 0, 0, 'L');
$pdf->Cell(0.1, 130, $sender_pincode, 0, 0, 'L');
$pdf->Cell(0.1, 140, 'Ph: '.$sender_mobile, 0, 0, 'L');
$pdf->Cell(0.1, 150, 'GSTIN NO: '.$gstin, 0, 0, 'L');
$pdf->Cell(120, 160, 'Email: '.$Email, 0, 0, 'L');

$pdf->Line(140, 48, 140, 93);

//Right
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-18, 85, 'INVOICE DETAILS:', 0, 0, 'R');

$pdf->SetFont('Arial', '', 8);
$pdf->Cell(-34, 0, '', 0, 0, 'L');
$pdf->Cell(0.1, 100, 'INVOICE NO               : Retail'.$invoiceNumber, 0, 0, 'L');
$pdf->Cell(0.1, 110, 'ORDER ID                  : '.$orderID, 0, 0, 'L');
$pdf->Cell(0.1, 120, 'ORDER DATE            : '.$order_date, 0, 0, 'L');
$pdf->Cell(0.1, 130, 'CHANNEL                  : '.$channel, 0, 0, 'L');
$pdf->Cell(0.1, 140, 'SHIPPED BY              : '.$operator, 0, 0, 'L');
$pdf->Cell(0.1, 150, 'AWB NO                     : '.$awb, 0, 0, 'L');
$pdf->Cell(-130, 160, 'PAYMENT METHOD : '.$cod, 0, 0, 'L');

$pdf->Line(10, 102, 200, 102);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(0.1, 195, 'S.NO.          PRODUCT NAME          HSN          QTY          UNIT PRICE          UNIT DISCOUNT          TAXABLE VALUE          IGST          TOTAL', 0, 0, 'L');
$pdf->Line(10, 112, 200, 112);

$pdf->SetFont('Arial', '', 8);
$pdf->Cell(15, 215, '1');
$pdf->Cell(28, 215, $name[1]);
$pdf->Cell(18, 215, $hsn[1]);
$pdf->Cell(14, 215, $qty[1]);
$pdf->Cell(23, 215, 'Rs. '.$price);
$pdf->Cell(30, 215, $discount[1]);
$pdf->Cell(31, 215, $price);
$pdf->Cell(16, 215, $tax[1]);
$pdf->Cell(-160, 215, $total);

$pdf->Cell(80, 228, $product_sku);

$pdf->Line(100, 130, 200, 130);
$pdf->SetFont('Arial', 'B', 12);

$pdf->Cell(-95, 253, 'NET TOTAL(In Value)                         Rs. '.$price * $qty[1].'.00', 0 , 0);
$pdf->Line(100, 143, 200, 143);

$pdf->Image('../images/sign.jpg',13, 147, 50, 20);

$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(10, 266, 'Authorized Sign for '.$first_name.' '.$last_name);

$result = $conn->query("UPDATE customer_orders SET new_invoice_status = 'INVOICED' WHERE Order_ID = '".$orderID."'");

$pdf->Output('Retail'.$invoiceNumber.'.pdf', 'D');

?>