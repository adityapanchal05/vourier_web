<!DOCTYPE html>
<html>
<head>
	<title>Shipdots Dashboard</title>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="css/index.css">

	<link href="https://fonts.googleapis.com/css?family=Lacquer&display=swap" rel="stylesheet">

</head>
<body>

	<style>
		input:focus {
			border-bottom: 3px solid #27c24c;
		}
	</style>

	<?php
	include '../conn.php';

		if($_POST) {
			if(isset($_POST['login'])) {
				startLogin();
			}
		}

		/*$dbhost = "localhost";
		$dbuser = "root";
		$dbpass = "";
		$dbname = "logistics_v2";
        
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
		*/

		/*if(!$conn) {
		die("Connection Failed :".mysqli_connect_error());
		}
		else {*/
			$sql_query = "SELECT * from hub_operators";
			$result =OpenCon() -> query($sql_query);

			if($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$db_email = $row["OPERATOR_ID"];
				}
			}
		//}

		function startLogin() {

			$email = $_POST['OPERATOR_ID'];
			$password = $_POST['PASSWORD'];
			$isAMember = 1;

			/*$dbhost = "localhost";
			$dbuser = "root";
			$dbpass = "";
			$dbname = "logistics_v2";
			
			$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
			*/


			/*if(!$conn) {
				die("Connection Failed :".mysqli_connect_error());
			}
			else {*/
				$sql_query = "SELECT * from hub_operators";
				$result = OpenCon() -> query($sql_query);

				if($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
						$db_email = $row["OPERATOR_ID"];
						$name = $row['NAME'];
						$hub_id = $row['HUB_ID'];
						if(strcmp($db_email, $email) == 0) {
							$sql_password_query = "SELECT password from hub_operators WHERE operator_id = '".$email."'";
							$result = OpenCon()-> query($sql_password_query);
							while ($row = $result->fetch_assoc()) {
								$db_password = $row['password'];
								if(strcmp($db_password, $password) == 0) {
									$isAMember++;
								}
							}
						}
					}
					if($isAMember > 1) {
						session_start();
						$_SESSION['operator_id'] = $email;
						$_SESSION['Name'] = $name;
						$_SESSION['hub_id'] = $hub_id;
						$_SESSION['dbhost'] = $dbhost;
						$_SESSION['dbuser'] = $dbuser;
						$_SESSION['dbpass'] = $dbpass;
						$_SESSION['dbname'] = $dbname;
						echo '<script language="javascript">';
						echo 'location.href="dashboard.php";';
						echo '</script>';
					}
					else {
						echo '<script language="javascript">';
						echo 'alert("Invalid Details");';
						//echo 'location.href="index.php";';
						echo '</script>';
					}
				}
			//}
		}

	?>

	<div class="container" style="width: 100%; 
						   background: #0575e6; 
						   background: -webkit-linear-gradient(to right, #0575e6, #021b79); 
						   background: linear-gradient(to right, #0575e6, #021b79);
						   min-width: 1100px; overflow-x: scroll;
						   padding-left: 300px; padding-right: 300px; padding-top: 120px; padding-bottom: 200px;"

						   >
			<div style="width: 100%; float: left;
			-webkit-box-shadow: 3px 3px 5px 6px black;
			-moz-box-shadow:    0px 0px 20px 3px black;
			box-shadow:         0px 0px 20px 3px black;
			border-radius: 3px; background-color: #fafbfc;">
				<div style="width: 50%; float: left; background-color: #fafbfc; padding: 90px;">
					<div style="width: 100%; float: left;">
						<img src="images/logo.png" style="float: left; height: 40px; width: 40px; border-radius: 50%; border: 0.5px solid #eee;"><p align="left" style="font-size: 25px; font-family: 'Lacquer', sans-serif; color: #656565; margin-bottom: 0px; margin-top: 1px; margin-left: 48px;"><b>Shipdots</b></p>
					</div>
					<div style="width: 100%; float: left; margin-top: 40px;">
						<p align="left" style="color: #656565; font-size: 18px;">Welcome To <br><span style="font-size: 40px"><b>Shipdots</b></span></p>
					</div>
					<div style="width: 100%; float: left; margin-top: 50px;">
						<form method="post" enctype="multipart/form-data">
							<div style="width: 100%; float: left;">
								<p align="left" style="color: gray; font-size: 13px;">Operator ID</p>
								<input type="text" name="OPERATOR_ID" style="border-radius: 3px; height: 43px; outline: none; float: left; width: 75%; padding: 5px; border-top: 1px solid #CFD4D6; border-left: 1px solid #CFD4D6; border-right: 1px solid #CFD4D6;">
							</div>
							<div style="width: 100%; float: left; margin-top: 10px;">
								<p align="left" style="color: gray; font-size: 13px;">Password</p>
								<input type="password" name="PASSWORD" style="border-radius: 3px; height: 43px; outline: none; float: left; width: 75%; padding: 5px; border-top: 1px solid #CFD4D6; border-left: 1px solid #CFD4D6; border-right: 1px solid #CFD4D6;">
							</div>
							<div style="width: 100%; float: left; margin-top: 30px;">
								<button class="btn btn-info" name="login" style="width: 75%; float: left; background-color: #0575e6;">LOGIN</button>
							</div>
						</form>
					</div>
				</div>
				<div style="width: 50%; float: left; background-color: #fafbfc; padding: 90px; padding-left: 0px;">
					<p style="width: 100%; font-size: 25px; color: #656565;"><b>Shipdots HUB</b></p>
					<div style="width: 100%; float: left; margin-top: 20px;">
						<p style="font-size: 15px; color: #0575e6;"><b>Enter your HUB ID and Password to continue.</b></p>
					</div>
					<div style="width: 100%; float: left; margin-top: 20px;">
						<img src="images/welcome.jpg">
					</div>
				</div>
			</div>
			
	</div>

	<script type="text/javascript">
		function showPassword() {
			var x = document.getElementById("showPass");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
	</script>
</body>
</html>