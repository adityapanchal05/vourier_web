<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistics Company | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" type="text/css" href="css/dashboard.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<?php

include '../conn.php';

    session_start();
    
    $operator_id = $_SESSION['operator_id'];
    $name = $_SESSION['Name'];
    $hub_id = $_SESSION['hub_id'];
    $dbhost = $_SESSION['dbhost'];
    $dbuser = $_SESSION['dbuser'];
    $dbpass = $_SESSION['dbpass'];
    $dbname = $_SESSION['dbname'];

?>

<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b> Da</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Shipdots</b> Dashboard</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $name; ?>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="all-orders.php">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="rider_details.php">
            <i class="fa fa-ship"></i><span>Pickup Rider Details</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="support.php"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center;" id="id_dashboard">

    	<div class="welcome_div">
        <p style="font-size: 24px; padding: 29px; color: #3C414C;">Welcome to Shipdots Dashboard</p>
      </div><br>

      <div class="btn-holder">
        <p style="font-size: 18px; color: #3C414C; padding: 25px;">Lets Begin!</p>
        <hr style="border-top: 0.5px solid #eee; margin-top: -21px;">
        <br>
        <div class="img-holder">
          <img src="images/Shipping.png" style="width: 115px;">
          <br>
          <h3 style="color: #3C414C;"><b>All Orders</b></h3>
          <hr style="border-top: 2px solid #3C414C; width: 10%">
          <h4 style="font-size: 18px;">View all order details.</h4>
          <br>
          <a type="button" class="btn btn-primary" style="margin-bottom: 20px;" href="all-orders.php">Orders</a>
          <br>
        </div>
        <div class="img-holder_2">
          <img src="images/Multi-Channel.png" style="width: 115px;">
          <br>
          <h3 style="color: #3C414C;"><b>Connect Selling Channels</b></h3>
          <hr style="border-top: 2px solid #3C414C; width: 10%">
          <h4 style="font-size: 18px;">Have your website or sell on different marketplaces?<br> Add your store to sync your orders and start shipping.</h4>
          <br>
          <button type="button" class="btn btn-primary" style="margin-bottom: 20px;">Connect Channel</button>
          <br>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
