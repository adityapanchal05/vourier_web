<!DOCTYPE html>
<html>
<head>
	<title>Logistics Company | User Orders</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/user_orders.css">

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php

  include '../conn.php';

	function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $operator_id = $_SESSION['operator_id'];
    $name = $_SESSION['Name'];
    $hub_id = $_SESSION['hub_id'];
    /*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";*/
    restore_error_handler();

    $all_orders_array = array();

    /*$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if(!Opencon()) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {*/

      if(isset($_GET['items_per_page'])) {
        $items_per_page = $_GET['items_per_page'];
      }
      else {
        $items_per_page = 25;
      }
      $processing_page = "";
      if(isset($_GET['processing_page'])) {
        $processing_page = $_GET['processing_page'];
      }
      else {
        $processing_page1 = 0;
      }
      if ($processing_page=="" || $processing_page=="1") {
        $processing_page1 = 0;
      }
      else {
        $processing_page1 = ($processing_page*$items_per_page)-$items_per_page;
      }

      if(isset($_GET['query'])) {
        $query = $_GET['query'];
      }
      else {
        $query = "SELECT * FROM customer_orders WHERE Status = 'FAILED' AND HUB_ID = '".$hub_id."' AND AWB_Number != '' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      }

      $processing_orders_query = $query;
      $result = Opencon()->query($processing_orders_query);
      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $all_orders_array[] = $row;
        }
      }
      else {
      	$all_orders_array = array();
      }

      $processing_orders_number_query = $query;
      $result = Opencon()->query($processing_orders_number_query);
      $processing_orders_number = mysqli_num_rows($result);
    //}

    if(isset($_POST['search_by_id'])) {
        $id_to_search = $_POST['searchOrderId'];
        $id_query = "SELECT * FROM customer_orders WHERE Order_ID = '".$id_to_search."' AND HUB_ID = '".$hub_id."' ";
        header("Location: all-orders.php?query=".$id_query."");
      }

    if(isset($_POST['search_by_customer_id'])) {
        $id_to_search = $_POST['searchCustomerId'];
        $id_query = "SELECT * FROM customer_orders WHERE Email = '".$id_to_search."' AND HUB_ID = '".$hub_id."' ";
        header("Location: all-orders.php?query=".$id_query."");
      }

    if(isset($_POST['search_by_channel'])) {
    	$channel_filter = $_POST['channel_select'];
    	$channel_query = "SELECT * FROM customer_orders WHERE operator = '".$channel_filter."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$channel_query."");
    }

    if(isset($_POST['today_date_orders'])) {
    	$date_today = date('Y-m-d');
    	$date_query = "SELECT * FROM customer_orders WHERE date_created = '".$date_today."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	$url = "all-orders.php?query=".$date_query."";
    	header("Location: ".strtok($url, '#')."");
    }

    if(isset($_POST['yesterday_date_orders'])) {
    	$date_yesterday = date('Y-m-d', strtotime("-1 days"));
    	$date_query = "SELECT * FROM customer_orders WHERE date_created = '".$date_yesterday."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$date_query."");
    }

    if(isset($_POST['seven_days_orders'])) {
    	$date_seven_days = date('Y-m-d', strtotime("-7 days"));
    	$date_query = "SELECT * FROM customer_orders WHERE date_created >= '".$date_seven_days."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$date_query."");
    }

    if(isset($_POST['thirty_days_orders'])) {
    	$date_thirty_days = date('Y-m-d', strtotime("-30 days"));
    	$date_query = "SELECT * FROM customer_orders WHERE date_created >= '".$date_thirty_days."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$date_query."");
    }

    if(isset($_POST['month_orders'])) {
    	$first_date = date('Y-m-01');
    	$date_month = date('Y-m-d');
    	$date_query = "SELECT * FROM customer_orders WHERE date_created >= '".$first_date."' AND date_created <= '".$date_month."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$date_query."");
    }

    if(isset($_POST['last_month_orders'])) {
    	$first_date_last = date('Y-m-01', strtotime("-1 monnths"));
    	$date_month_last = date('Y-m-31', strtotime("-1 months"));
    	$date_query = "SELECT * FROM customer_orders WHERE date_created >= '".$first_date_last."' AND date_created <= '".$date_month_last."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
    	header("Location: all-orders.php?query=".$date_query."");
    }

    if(isset($_POST['custom_date_range'])) {
      $from_date = $_POST['from_date'];
      $to_date = $_POST['to_date'];
      $custom_date_query = "SELECT * FROM customer_orders WHERE date_created >= '".$from_date."' AND date_created <= '".$to_date."' AND HUB_ID = '".$hub_id."'  ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: all-orders.php?query=".$custom_date_query."");
    }

    function generate_token() {

		$url = 'https://apiv2.shiprocket.in/v1/external/auth/login';
		$ch = curl_init($url);
		$data = array(
		    'email' => 'pubg61890@gmail.com',
		    'password' => 'Akshay123@'
		);

		$payload = json_encode($data);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
    }

	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $name; ?>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="rider_details.php">
            <i class="fa fa-ship"></i><span>Pickup Rider Details</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; width: 100%; overflow-x: scroll; background-color: #fafafa; padding-left: 0px;">

    	<div class="individual_order_heading" style="width: 100%; min-width: 2000px; overflow-x: hidden; margin-bottom: 10px;">
    		<div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 2px;">

          <button id="processing_orders" class="order-button" onclick="window.location.href='all-orders.php'" style="width: 18%;"><i class="fa fa-gears"></i>  Orders  <span><p style="display: inline-block; background-color: #ff902b; width: 15%; border-radius: 25px; color: white;"><?php echo $processing_orders_number; ?></p></span></button>

          <button id="readytoship_orders"  onclick="window.location.href='assign_riders.php'" style="margin: 2px;border-radius: 12px; color: white; background-color:#2E8B57; height: 42px; font-size: 17px;"><i class="fa fa-truck"></i>  Assign Riders  </button>

          <button id="status_orders" class="order-button" onclick="window.location.href='failed-orders.php'"  style="width: 15%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: 0; margin-left: 20px;"><i class="far fa-window-close"></i>  Failed Orders  <span><p style="display: inline-block; background-color: #ff5733; width: 15%; border-radius: 25px; color: white;">  Failed  </p></span></button></button>
           <button id="all-orders" class="order-button" onclick="window.location.href='all-sort-orders.php'"  style="width: 18%;"><i class="fas fa-list"></i>  All Orders  <span><p style="display: inline-block; background-color: #285fdb ; width: 15%; border-radius: 25px; color: white;"> Orders </p></span></button></button>
    		</div>
    		<div id="processing_orders_div" style="width: 100%; float: left; min-width: 2000px;">

    			<div style="width: 100%; float: left; background-color: rgba(0,0,0,0.07); ">

    				<div style="width: 10%; float: left; border: 0.5px solid #ddd;">
    					<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>ORDER DATE</b><span style="color: #285fdb; padding: 10px;"><a href="#choose_date" style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showDateBox();"><i class="fa fa-calendar"></i></a></span></p>
    					<div id="SEARCH_BY_DATE" style="width: 100%; padding: 5px; display: none;">
	    					<form method="post" enctype="multipart/form-data">
	    						
	    					</form>
	    				</div>
    				</div>

    				<div style="width: 8%; float: left; border: 0.5px solid #ddd;">
    					<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>OPERATOR</b><span style="color: #285fdb; padding: 10px;"><button style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showChannelBox();"><i class="fa fa-filter"></i></button></span></p>
    					<div id="SEARCH_BY_CHANNEL" style="width: 100%; padding: 5px; float: left; display: none;">
	    					<form method="post" enctype="multipart/form-data">
	    						<p align="Left" style="margin-left: 5px; width: 100%; margin-bottom: 5px;"><b>Filter by Operator</b></p>
	    					  <select name="channel_select" style="float: left; margin-left: 2px; width: 98%; border-radius: 3px;">
                    <option value="FedEx">FedEx</option>
                    <option value="DHL">DHL</option>
                    <option value="DTDC">DTDC</option>
                    <option value="Speed Post">Speed Post</option>
                  </select><br><br>
	    						<button name="search_by_channel" class="btn btn-success" style="float: left; margin-left: 20px; margin-top: -12px; margin-bottom: 5px;"><i class="fa fa-check"></i></button>
	    					</form>
	    					<button class="btn btn-danger" onclick="hideChannelBox();" style="float: left; margin-left: 20px; margin-top: -12px; margin-bottom: 5px;"><i class="fa fa-close"></i></button>
	    				</div>
    				</div>

	    			<div style="width: 10%; float: left; border: 0.5px solid #ddd;">
	    				<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>ORDER ID / STATUS</b><span style="color: #285fdb; padding: 10px;"><button style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showSearchBox();"><i class="fa fa-search"></i></button></span></p>
	    				<div id="SEARCH_BY_ID" style="width: 100%; padding: 5px; display: none; float: left;">
	    					<form method="post" enctype="multipart/form-data">
	    						<p align="Left" style="margin-bottom: 5px;"><b>Search by Order ID</b></p>
	    						<input name="searchOrderId" style="border-radius: 3px; padding: 5px; margin-top: 7px;" type="text" name="ORDER_ID">
	    						<button name="search_by_id" class="btn btn-success" style="float: left; margin-left: 20px; margin-top: 10px; margin-bottom: 5px;"><i class="fa fa-check"></i></button>
	    					</form>
	    					<button class="btn btn-danger" onclick="hideSearchBox();" style="float: left; margin-left: 20px; margin-top: 10px; margin-bottom: 5px;"><i class="fa fa-close"></i></button>
	    				</div>
    				</div>

            <div style="width: 11%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>CUSTOMER ID</b><span style="color: #285fdb; padding: 10px;"><button style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showCustomerBox();"><i class="fa fa-search"></i></button></span></p>
              <div id="SEARCH_BY_CUSTOMER_ID" style="width: 100%; padding: 5px; display: none; float: left;">
                <form method="post" enctype="multipart/form-data">
                  <p align="Left" style="margin-bottom: 5px;"><b>Search by Customer ID</b></p>
                  <input name="searchCustomerId" style="border-radius: 3px; padding: 5px; margin-top: 7px;" type="text" name="CUSTOMER_ID">
                  <button name="search_by_customer_id" class="btn btn-success" style="float: left; margin-left: 20px; margin-top: 10px; margin-bottom: 5px;"><i class="fa fa-check"></i></button>
                </form>
                <button class="btn btn-danger" onclick="hideCustomerBox();" style="float: left; margin-left: 20px; margin-top: 10px; margin-bottom: 5px;"><i class="fa fa-close"></i></button>
              </div>
            </div>

    				<div style="width: 9%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
    					<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>PRODUCT DETAILS</b></p>
    				</div>

    				<div style="width: 9%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">

    					<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>PAYMENT</b><span style="color: #285fdb; padding: 10px;">

    					<button id="payment_asc" onclick="sort_payment(this.id);" title="Sort orders in Increasing order" style="background: transparent; border: none;"><i class="fa  fa-arrow-up"></i></button>

    					<button id="payment_desc" onclick="sort_payment(this.id);" title="Sort orders in Decreasing order" style="background: transparent; border: none;"><i class="fa  fa-arrow-down"></i></button>

    					</span></p>
    				</div>

    				<div style="width: 9%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
    					<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>CUSTOMER DETAILS</b></p>
    				</div>
    				<div style="width: 9%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
    					<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>PICKUP ADDRESS</b></p>
    				</div>
            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>DIMENSIONS & WEIGHT</b></p>
            </div>
    				<div style="width: 5%; float: left; border: 0.5px solid #ddd;">
    					<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>ORDER PRICE</b></p>
    				</div>
            <div style="width: 6%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>RE-ASSIGN/CANCEL</b></p>
            </div>
            
        	</div>

        	<?php

            $all_orders_query_page = "SELECT * FROM customer_orders WHERE Status = 'FAILED' AND HUB_ID = '".$hub_id."' ";
            $result = Opencon()->query($all_orders_query_page);
            if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $all_orders_array_page[] = $row;
              }
            }
            else {
                $all_orders_array_page = array();
              }

            $number_of_orders = count($all_orders_array_page);
            $number_of_pages = ceil($number_of_orders/$items_per_page);

            if(count($all_orders_array) == 0) {
              echo '<div style="width: 100%;">';
                echo '<img src="images/no_data.png" style="margin-top: 50px;"';
                echo '<br>';
                echo '<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>';
              echo '</div>';
            }
            else {
              foreach ($all_orders_array as $individual_order) {
              $order_date = $individual_order['order_date'];
              $channel = $individual_order['operator'];
              $order_id = $individual_order['Order_ID'];
              $product_details = $individual_order['product_details'];

              $customer_id = $individual_order['Email'];

              $sender_name = $individual_order['Sender_Name'];
              $sender_mobile = $individual_order['sender_mobile'];
              $sender_address_line1 = $individual_order['sender_address_line_1'];
              $sender_address_line2 = $individual_order['sender_address_line_2'];
              $sender_pincode = $individual_order['sender_pincode'];

              $customer_name = $individual_order['Customer_Name'];
              $customer_mobile = $individual_order['customer_mobile'];
              $customer_address_line1 = $individual_order['customer_address_line_1'];
              $customer_address_line2 = $individual_order['customer_address_line_2'];
              $customer_pincode = $individual_order['customer_pincode'];

              $length = $individual_order['length'];
              $breadth = $individual_order['breadth'];
              $height = $individual_order['height'];

              $weight = $individual_order['Weight'];
              $price = $individual_order['Price'];
              $isCOD = $individual_order['COD'];
              $final_price = $individual_order['Price_Paid_By_Customer'];

             //$a = array(
             //	array('Status'=> 'Failed'));
            //$new_invoice_status = array_column($a, 'Status');
             $new_invoice_status = $individual_order['Status'];
             //$str = implode(',', $new_invoice_status);


              //$sql = "SELECT STATUS FROM customer Orders WHERE "
            
             //$result1 = Opencon()->query($new_invoice_status);

              $pickup_id = $individual_order['Pickupboy_ID'];

              $volumetric_weight = ($length * $breadth * $height)/5000;

            echo'<div style="width: 100%; float: left; background-color: #fafafa; border: 2px solid #ddd;">';

            echo'<div style="width: 10%; float: left; padding-left: 10px;">';
              echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$order_date.'</p>';
            echo'</div>';

            echo'<div style="width: 8%; float: left; padding-left: 10px;">';
              echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$channel.'</p>';
            echo'</div>';
      
         

            echo'<div style="width: 8%; float: left; padding-left: 20px; padding-top: 8px;">';
              echo'<a href="documents/show_invoice.php?order='.$order_id.'" style="font-size: 15px; color: #285fdb;">'.$order_id.'</a> <br> <span style="color: orange;"><b>'.$new_invoice_status.'</b></span>';
            echo'</div>';

            echo'<div style="width: 13%; float: left; padding-left: 20px;">';
              echo'<p style="font-size: 15px; color: #27c24c; margin-top: 10px;">'.$customer_id.'</p>';
            echo'</div>';

            echo'<div style="width: 9%; float: left; padding-left: 20px;">';
              echo'<p align="left" style="font-size: 15px; color: #404040; margin-top: 10px;">'.$product_details.'</p>';
            echo'</div>';

            if(strcmp($isCOD, 'No')==0) {
              echo'<div style="width: 9%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-inr"></i> '.$price.' <br> <span style="background-color:#27c24c; color: white; padding: 4px; border-radius: 12px; font-size: 10px;"><b>PREPAID</b></span> </p>';
              echo'</div>';
            }
            else {
            echo'<div style="width: 9%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-inr"></i> '.$price.'<br> <span style="background-color: orange; color: white; padding: 4px; border-radius: 12px; font-size: 10px;"><b>POSTPAID</b></span> </p>';
            echo'</div>';
            }

            echo'<div style="width: 9%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$customer_name.' <br> '.$customer_mobile.' <br> '.$customer_address_line1.' <br>'.$customer_address_line2.' <br>'.$customer_pincode.'</p>';
            echo'</div>';

            echo'<div style="width: 9%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$sender_name.' <br> '.$sender_mobile.' <br> '.$sender_address_line1.' <br>'.$sender_address_line2.' <br>'.$sender_pincode.'</p>';
            echo'</div>';

            echo'<div style="width: 11%; float: left; padding-left: 16px;">';
	              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><b>Weight : </b>'.$weight.' kg<br><b> Dimensions : </b>'.$length.' x '.$breadth.' x '.$height.' <br><b> Volumetric : </b>'.$volumetric_weight.' kg</p>';
            echo'</div>';

              echo'<div style="width: 5%; float: left;">';
                echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-inr"></i> '.$final_price.'</p>';
              echo'</div>';

              echo'<div style="width: 6%; float: left;">';
                echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;"><button id="failed" onclick="" style="background-color:green; color:white;"><a href="re-assign_button.php?" style="background-color:green; color:white;">RE-ASSIGN</a></button></p>';
                echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;"><button id="failed" onclick="" style="background-color:red; color:white;"><a href="cancel_button.php?" style="background-color:red; color:white;">CANCEL</a></button></p>';
              echo'</div>';

              }
            }
        		?>

            <div class="footer" style="margin-bottom: 20px;">

              <?php

              echo'<div style="width: 100%; float:left;">';
      
                echo'<div style="width: 40%; float: left; margin-top: 15px;">';
                  echo '<p style="color: #656565; float: left; padding-left: 20px; font-size: 13px;">Show&nbsp<b><span> <select id="selectBox" onchange="window.location.href = this.value">  
                    <option label="25" value="all-orders.php?items_per_page=25">25</option>
                    <option label="50" value="all-orders.php?items_per_page=50">50</option>
                    <option label="100" value="all-orders.php?items_per_page=100">100</option> 
                    </select> </span></b>&nbspitems per page</p>';
                echo'</div>';

                echo'<div style="width: 39%; float: left; margin-top: 15px;">';
                  if($processing_page=="") {
                        $processing_page = 1;
                      }
                  echo '<p style="color: #656565; float: left; font-size: 13px; ">Page Number : '.$processing_page.'</p>';
                echo'</div>';

                echo'<div style="float: left; margin-top: 10px;">';
                  for($b=1;$b<=$number_of_pages;$b++) {
                      ?><a class="page_number_list" href="all-orders.php?processing_page=<?php echo $b; ?>&items_per_page=<?php echo $items_per_page; ?>" style="text-decoration: none;"><?php echo $b." "; ?></a> <?php
                      }
                 echo'</div>';
              echo'</div>';

              ?>
            </div>
    		</div>

    		<div id="choose_date" class="overlay">
	          <div class="popup">
	            <a id="close_date_chooser" class="close" href="#">&times;</a>
	            <div class="content">
	              <form method="post" enctype="multipart/form-data">
	                <div style="width: 100%; float: left;">
	                	<button name="all_orders" class="date_range">All</button>
	                </div>
	                <div style="width: 100%; float: left;">
	                	<button name="today_date_orders" class="date_range">Today</button>
	                </div>
	                <div style="width: 100%; float: left;">
	                	<button name="yesterday_date_orders" class="date_range">Yesterday</button>
	                </div>
	                <div style="width: 100%; float: left;">
	                	<button name="seven_days_orders" class="date_range">Last 7 days</button>
	                </div>
	                <div style="width: 100%; float: left;">
	                	<button name="thirty_days_orders" class="date_range">Last 30 days</button>
	                </div>
	                <div style="width: 100%; float: left;">
	                	<button name="month_orders" class="date_range">This Month</button>
	                </div>
	                <div style="width: 100%; float: left;">
                    <button name="last_month_orders" class="date_range">Last Month</button>
                  </div>
	              </form>
                <div style="width: 100%; float: left;">
                  <?php $date = date('Y-m-d'); ?>
                    <button class="date_range" onclick="showCalendar();">Custom Range</button>
                    <div id="date_picker" style="float: left; padding: 10px; display: none;">
                      <form method="post" enctype="multipart/form-data">
                        <label style="float: left; margin-top: 2px; margin-right: 2px;">From : </label>
                        <input id="from_date" style="float: left; border-radius: 3px;" type="date" name="from_date" value="<?php echo $date; ?>" required>

                        <label style="float: left; margin-top: 2px; margin-right: 2px; margin-left: 10px;">To : </label>
                        <input id="to_date" style="float: left; border-radius: 3px;" type="date" name="to_date" value="<?php echo $date; ?>" required>

                        <div style="width: 42%; float: left;">
                          <button id="apply_button" name="custom_date_range" class="btn btn-success" style="float: left; margin-top: 7px; margin-left: 40px; background-color: #27c24c; border: none;">Apply</button>
                        </div>
                      </form>
                      <div style="float: left;">
                          <button class="btn btn-danger" style="float: left; margin-top: 7px; margin-left: -50px;" onclick="hideCalendar();">Cancel</button>
                        </div>
                      <div id="wrong_date" style="width: 100%; float: left; margin-top: 10px; display: none;">
                        <p align="left" style="color: red;">'From' Date cannot be greater than 'To' Date!</p>
                      </div>
                    </div>
                </div>
	            </div>
	          </div>
        	</div>

    	</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/showOrders.js"></script>
<script src="js/changeEntryHide.js"></script>

<script type="text/javascript">
  window.onload = function() {
    var items_value = '<?php echo $items_per_page ?>';
    if(items_value == 25) {
      var index  = 0;
    }
    else if(items_value == 50) {
      var index  = 1;
    }
    else if(items_value == 100) {
      var index  = 2;
    }
    document.getElementById("selectBox").selectedIndex = index;
    document.getElementById('close_date_chooser').click();

    var url = window.location.href;
    if(url.includes("?")) {
      var new_url = url.substr(0, url.indexOf("?"));
      window.history.pushState("NEW", "NEW", new_url);
    }
}
</script>

<script type="text/javascript">
	function sort_payment(id) {
  	if((id.localeCompare("payment_asc"))==0) {
  		var email = '<?php echo($Email); ?>';
  		var processing_page_1 = '<?php echo($processing_page1); ?>';
  		var items_per_page = '<?php echo($items_per_page); ?>';

  		window.location.href = "all-orders.php?query=SELECT * FROM customer_orders ORDER BY Price LIMIT "+processing_page_1+","+items_per_page;
  	}
  	else {
  		var email = '<?php echo($Email); ?>';
  		var processing_page_1 = '<?php echo($processing_page1); ?>';
  		var items_per_page = '<?php echo($items_per_page); ?>';

  		window.location.href = "all-orders.php?query=SELECT * FROM customer_orders ORDER BY Price DESC LIMIT "+processing_page_1+","+items_per_page;
  	}
  }
</script>

<script type="text/javascript">
	function showSearchBox() {
		document.getElementById("SEARCH_BY_ID").style.display = 'block';
	}
	function hideSearchBox() {
		document.getElementById("SEARCH_BY_ID").style.display = 'none';
	}
  function showCustomerBox() {
    document.getElementById("SEARCH_BY_CUSTOMER_ID").style.display = 'block';
  }
  function hideCustomerBox() {
    document.getElementById("SEARCH_BY_CUSTOMER_ID").style.display = 'none';
  }
	function showChannelBox() {
		document.getElementById("SEARCH_BY_CHANNEL").style.display = 'block';
	}
	function hideChannelBox() {
		document.getElementById("SEARCH_BY_CHANNEL").style.display = 'none';
	}
  function showCalendar() {
    document.getElementById("date_picker").style.display = 'block';
  }
  function hideCalendar() {
    document.getElementById("date_picker").style.display = 'none';
  }
  function showSKUBox() {
  	document.getElementById("SEARCH_BY_SKU").style.display = 'block';
  }
  function hideSKUBox() {
  	document.getElementById("SEARCH_BY_SKU").style.display = 'none';
  }
  function showWeightChangeBox() {
  	document.getElementById("weight_dimension").style.display = 'block';
  }
  function hideWeightChangeBox() {
  	document.getElementById("weight_dimension").style.display = 'none';
  }
</script>

<script type="text/javascript">
  window.setInterval(function() {
    var from_date = document.getElementById("from_date").value;
    var to_date = document.getElementById("to_date").value;
    
    if(from_date > to_date) {
      document.getElementById("wrong_date").style.display = 'block';
      document.getElementById("apply_button").style.cursor = 'not-allowed';
      document.getElementById("apply_button").disabled = true;
      document.getElementById("apply_button").style.backgroundColor = "#eee";
      document.getElementById("apply_button").style.color = "#ddd";
    }
    else {
      document.getElementById("wrong_date").style.display = 'none';
      document.getElementById("apply_button").style.cursor = 'pointer';
      document.getElementById("apply_button").disabled = false;
      document.getElementById("apply_button").style.backgroundColor = "#27c24c";
      document.getElementById("apply_button").style.color = "white";
      
    }
  }, 1);
</script>

</body>
</html>