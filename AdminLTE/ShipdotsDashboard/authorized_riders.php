<!DOCTYPE html>
<html>
<head>
	<title>Authorized Riders</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
  include '../conn.php';

	function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $operator_id = $_SESSION['operator_id'];
    $name = $_SESSION['Name'];
    $hub_id = $_SESSION['hub_id'];
    /*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";
    */
    restore_error_handler();

    /*$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    */

   /* if(!$conn) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {

     
    }*/

	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $name; ?>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="rider_details.php">
            <i class="fa fa-ship"></i><span>Pickup Rider Details</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding-left: 0px;">
    	<div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding-left: 0px;">

    	<div style="width: 100%; min-width: 1500px; padding-left: 20px;">
    	  <div style="width: 100%; float: left; background-color: #fafafa; padding-top: 5px; padding-bottom: 5px;">

             <div style="width: 23%; float: left; padding-left: 10px;">
              <input type="text" name="searchRiderNumber" style="border: 1px solid #CFD4D6; width: 92%; padding-left: 10px; height: 32px; border-radius: 3px; float: left;" placeholder="Search by Rider Number">
              <button name="search_by_id" style="margin-left: -3px; padding-top: 3px; padding-bottom: 5px; float: left;"><i class="fa fa-search"></i></button>
             </div>

             <div style="width: 23%; float: left; padding-left: 10px;">
              <input type="text" name="searchName" style="border: 1px solid #CFD4D6; width: 92%; padding-left: 10px; height: 32px; border-radius: 3px; float: left;" placeholder="Search by Rider Name">
              <button name="search_by_name" style="margin-left: -3px; padding-top: 3px; padding-bottom: 5px; float: left;"><i class="fa fa-search"></i></button>
             </div>

          </div>	
    	</div>

    	<div style="width: 100%; float: left; min-width: 1500px; padding-left: 20px; background-color: #eeeeee; margin-top: 10px;">
    		<div style="float: left; width: 10%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>RIDER JOINED DATE</b></p><br>
    		</div>
    		<div style="float: left; width: 10%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>REGISTRED NUMBER</b></p><br>
    		</div>
    		<div style="float: left; width: 10%; border-right: 1px solid white; font-size: 15px;">
    			<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>REGISTERED NAME</b></p><br>
    		</div>
        	<div style="float: left; width: 10%; border-right: 1px solid white; font-size: 15px;">
          		<p align="left" style="padding: 10px; margin-bottom: 0px;"><b>ASSIGNED HUB ID</b></p><br>
        	</div>
        	<div style="float: left; width: 30%; border-right: 1px solid white; font-size: 15px;">
          		<p style="padding: 10px; margin-bottom: 0px;"><b>AADHAR CARD</b></p><br>
        	</div>
        	<div style="float: left; width: 30%; border-right: 1px solid white; font-size: 15px;">
          		<p style="padding: 10px; margin-bottom: 0px;"><b>DRIVER LICENSE</b></p><br>
        	</div>
    	</div>

    	<?php  

    	$authorized_riders = "SELECT * FROM authorized_riders";
    	$result = Opencon()->query($authorized_riders);
    	if($result->num_rows > 0) {
    		while ($row = $result->fetch_assoc()) {
    			$all_authorized_riders[] = $row;
    		}
    	}
    	else {
    		$all_authorized_riders = array();
    	}

    	if(count($all_authorized_riders) == 0) {
    		echo '<div style="width: 100%;">';
    		echo '<img src="images/no_data.png" style="margin-top: 50px;"';
    		echo '<br>';
    		echo '<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>';
    		echo '</div>';
    	}
    	else {
    		foreach ($all_authorized_riders as $rider) {
    			$joined_date = $rider['Authorized_Date'];
    			$registered_number = $rider['Registered_Number'];
    			$registered_name = $rider['Name'];
    			$assigned_hub_id = $rider['HUB_ID'];
    			$aadhar_card = $rider['Aadhar_Card'];
    			$driver_license = $rider['Driver_License'];

    			echo'<div style="width: 100%; float: left; background-color: #fafafa; border: 2px solid #ddd;">';

    				echo'<div style="width: 10%; float: left; padding-left: 10px;">';
    					echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$joined_date.'</p>';
    				echo'</div>';

    				echo'<div style="width: 10%; float: left; padding-left: 10px;">';
    					echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$registered_number.'</p>';
    				echo'</div>';

    				echo'<div style="width: 10%; float: left; padding-left: 10px;">';
    					echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$registered_name.'</p>';
    				echo'</div>';

    				echo'<div style="width: 10%; float: left; padding-left: 10px;">';
    					echo'<p style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$assigned_hub_id.'</p>';
    				echo'</div>';

    				echo'<div style="width: 30%; float: left;">';
    					echo '<img src="'.$aadhar_card.'" alt="Aadhar Card" id="aadharCard" onerror="this.onerror=null; this.src="dist/img/not_available.png" style="height: 250px; width: 300px; padding: 10px;" ">';
    				echo'</div>';

    				echo'<div style="width: 30%; float: left;">';
    					echo '<img src="'.$driver_license.'" alt="Driving License" id="drivingLicense" onerror="this.onerror=null; this.src="dist/img/not_available.png" style="height: 250px; width: 300px; padding: 10px;" ">';
    				echo'</div>';

    			echo'</div>';
    		}
    	}

    	?>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/showOrders.js"></script>
<script src="js/changeEntryHide.js"></script>
</body>
</html>