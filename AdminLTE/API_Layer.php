<?php

include 'conn.php';

/*$dbhost = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "logistics_v2";

Opencon() = mysqli_connect($dbhost, $db_user, $db_password, $db_name);
*/

if(isset($_POST['fromName'])) {

	$getToken = "SELECT * FROM shiprocket_token WHERE serial_number = '0000000001'";
    	$result = Opencon()->query($getToken);
    	if($result->num_rows > 0) {
    		while ($row = $result->fetch_assoc()) {
    			$token = $row['value'];
    		}
    	}

	$url = 'https://apiv2.shiprocket.in/v1/external/courier/serviceability/';
	
	$toPincode = $_POST['toPincode'];
	$fromPincode = $_POST['fromPincode'];
	$length = $_POST['length'];
	$breadth = $_POST['breadth'];
	$height = $_POST['height'];
	$weight = $_POST['weight'];
	$cod = 0;
	$price = $_POST['price'];

	if(strcmp($price, "NOT AVAILABLE")==0) {
		$price = 100;
	}

	$length = (int)$length;
	$breadth = (int)$breadth;
	$height = (int)$height;

	if($length <= 0) {
		$length = 0.5;
	}

	if($breadth <= 0) {
		$breadth = 0.5;
	}

	if($height <= 0) {
		$height = 0.5;
	}

	$ch = curl_init($url);
	$data = array(
		"pickup_postcode" => (int)$fromPincode,
		"delivery_postcode" => (int)$toPincode,
		"length" => $length/100,
		"breadth" => $breadth/100,
		"height" => $height/100,
		"weight" => $weight,
		"cod" => 0,
		"declared_value" => (int)$price,
	);
	$payload = json_encode($data);

	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	echo $result;
 }

?>