<!DOCTYPE html>
<html>
<head>
  <title>Logistics Company | User Orders</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" type="text/css" href="css/user_orders.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <?php

  include 'conn.php';

  function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";
    */

    $selected_orderid = array();
    $processing_orders_array = array();
    $readytoship_orders_array = array();
    $pickups_orders_array = array();
    $returns_orders_array = array();
    $all_orders_array = array();


   // Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    $address_query = "SELECT * from customer_addresses WHERE Email = '".$Email."' ";
    $result = Opencon()->query($address_query);
    if($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $total_addresses[] = $row;
      }
    }

    $sku_query = "SELECT SKU from customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN'";
    $result = Opencon()->query($sku_query);
    if($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $total_SKU[] = $row;
      }
    }


   /* if(!Opencon()) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {
      */
      $updateDetails = "SELECT * from customer_details WHERE Email = '".$Email."'";
      $result = Opencon() -> query($updateDetails);

      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $wallet = $row['Wallet'];
        }
      }
      $_SESSION['Wallet'] = $wallet;


      $order_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."'";
      $result = Opencon()->query($order_query);
      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $all_orders_array[] = $row;
        }
      }
      else {
        echo '<script language="javascript">';
        echo 'alert("No orders found!.");';
        echo '</script>';
      }


      #Paging for Processing Orders
      if(isset($_GET['items_per_page'])) {
        $items_per_page = $_GET['items_per_page'];
      }
      else {
        $items_per_page = 25;
      }
      $processing_page = "";
      if(isset($_GET['processing_page'])) {
        $processing_page = $_GET['processing_page'];
      }
      else {
        $processing_page1 = 0;
      }
      if ($processing_page=="" || $processing_page=="1") {
        $processing_page1 = 0;
      }
      else {
        $processing_page1 = ($processing_page*$items_per_page)-$items_per_page;
      }

      if(isset($_GET['query'])) {
        $query = $_GET['query'];
      }
      else {
        $query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      }

      $processing_orders_query = $query;
      $result = Opencon()->query($processing_orders_query);
      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $processing_orders_array[] = $row;
        }
      }

      $processing_orders_number_query = $query;
      $result = Opencon()->query($processing_orders_number_query);
      $return_orders_number = mysqli_num_rows($result);
    //}

    if(isset($_POST['search_by_id'])) {
        $id_to_search = $_POST['searchOrderId'];
        $id_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND Order_ID = '".$id_to_search."'";
        header("Location: return_orders.php?query=".$id_query."");
      }

    if(isset($_POST['search_by_channel'])) {
      $channel_filter = $_POST['channel_select'];
      $channel_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND Channel = '".$channel_filter."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$channel_query."");
    }

    if(isset($_POST['all_orders'])) {
      header("Location: return_orders.php");
    }

    if(isset($_POST['today_date_orders'])) {
      $date_today = date('Y-m-d');
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created = '".$date_today."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      $url = "return_orders.php?query=".$date_query."";
      header("Location: ".strtok($url, '#')."");
    }

    if(isset($_POST['yesterday_date_orders'])) {
      $date_yesterday = date('Y-m-d', strtotime("-1 days"));
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created = '".$date_yesterday."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$date_query."");
    }

    if(isset($_POST['seven_days_orders'])) {
      $date_seven_days = date('Y-m-d', strtotime("-7 days"));
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created >= '".$date_seven_days."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$date_query."");
    }

    if(isset($_POST['thirty_days_orders'])) {
      $date_thirty_days = date('Y-m-d', strtotime("-30 days"));
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created >= '".$date_thirty_days."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$date_query."");
    }

    if(isset($_POST['month_orders'])) {
      $first_date = date('Y-m-01');
      $date_month = date('Y-m-d');
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created >= '".$first_date."' AND date_created <= '".$date_month."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$date_query."");
    }

    if(isset($_POST['last_month_orders'])) {
      $first_date_last = date('Y-m-01', strtotime("-1 monnths"));
      $date_month_last = date('Y-m-31', strtotime("-1 months"));
      $date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created >= '".$first_date_last."' AND date_created <= '".$date_month_last."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$date_query."");
    }

    if(isset($_POST['custom_date_range'])) {
      $from_date = $_POST['from_date'];
      $to_date = $_POST['to_date'];
      $custom_date_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN' AND date_created >= '".$from_date."' AND date_created <= '".$to_date."' ORDER BY timestamp DESC LIMIT $processing_page1, $items_per_page";
      header("Location: return_orders.php?query=".$custom_date_query."");
    }

    if(isset($_POST['chooseOperator'])) {
      $individual_order_id = $_POST['orderID'];
      $order_type = $_POST['order_type'];

      $sender_name = $_POST['fromName'];
      $sender_mobile = $_POST['fromMobile'];
      $sender_address_line1 = $_POST['fromAddressLine1'];
      $sender_address_line2 = $_POST['fromAddressLine2'];
      $sender_pincode = $_POST['fromCode'];

      $customer_name = $_POST['toName'];
      $customer_mobile = $_POST['toMobile'];
      $customer_address_line1 = $_POST['toAddressLine1'];
      $customer_address_line2 = $_POST['toAddressLine2'];
      $customer_pincode = $_POST['toCode'];

      $product_details = $_POST['product_details'];
      $length = $_POST['length'];
      $breadth = $_POST['breadth'];
      $height = $_POST['height'];
      $weight = $_POST['weight'];
      $isCOD = $_POST['COD'];

      $_SESSION['orderID'] = $individual_order_id;

      $_SESSION['fromName'] = $sender_name;
      $_SESSION['fromMobile'] = $sender_mobile;
      $_SESSION['fromAddressLine1'] = $sender_address_line1;
      $_SESSION['fromAddressLine2'] = $sender_address_line2;
      $_SESSION['fromCode'] = $sender_pincode;

      $_SESSION['toName'] = $customer_name;
      $_SESSION['toMobile'] = $customer_mobile;
      $_SESSION['toAddressLine1'] = $customer_address_line1;
      $_SESSION['toAddressLine2'] = $customer_address_line2;
      $_SESSION['toCode'] = $customer_pincode;

      $_SESSION['product_details'] = $product_details;
      $_SESSION['length'] = $length;
      $_SESSION['breadth'] = $breadth;
      $_SESSION['height'] = $height;
      $_SESSION['weight'] = $weight;
      $_SESSION['COD'] = $isCOD;

      if(strcmp($order_type, 'INDIVIDUAL - DOCUMENT')==0) {
        echo '<script language="javascript">';
        echo 'location.href="individual-document.php";';
        echo '</script>';
      }

      else if(strcmp($order_type, 'INDIVIDUAL - PARCEL')==0) {
        echo '<script language="javascript">';
        echo 'location.href="individual-parcel.php";';
        echo '</script>';
      }

      else if(strcmp($order_type, 'BUSINESS - DOCUMENT')==0) {
        echo '<script language="javascript">';
        echo 'location.href="business-document.php";';
        echo '</script>';
      }
      else if(strcmp($order_type, 'BUSINESS - PARCEL')==0) {
        echo '<script language="javascript">';
        echo 'location.href="business-parcel.php";';
        echo '</script>';
      }
    }

    if(isset($_POST['cancelOrder'])) {

      $orderID = $_POST['orderID'];

      $cancelOrder = Opencon()->query("UPDATE customer_orders SET Status = 'CANCELLED' WHERE Order_ID = '".$orderID."'");
      if($cancelOrder) {
        echo '<script language="javascript">';
        echo 'alert("Order Cancelled Successfully!");';
        echo 'location.href="user_orders.php";';
        echo '</script>';
      }
      else {
        echo '<script language="javascript">';
        echo 'alert("Some Error Occured!");';
        echo 'location.href="user_orders.php";';
        echo '</script>';
      }
    }

    if(isset($_POST['printOrInvoice'])) {
      $task = $_POST['printOrInvoice'];

      if(strcmp($task, 'Invoice')==0) {
        $id = array();
        $all_order_id = $_POST['hiddenF-print-invoice'];
        $all_ids = array($all_order_id);
        $individual_id = $all_ids[0];
        $id = explode(",", $individual_id);

        foreach ($id as $key) {
          $order_ids[] = $key;
        }
        $serializedArray = serialize($order_ids);
        header('Location: documents/groupInvoice.php?order_list='.$serializedArray.'');
      }
    }

    if(isset($_POST['changePickupAddress'])) {
      $alias = $_POST['changePickupAddress'];
      $alias = str_replace("'", "''", $alias);
      $get_details_of_alias = "SELECT * FROM customer_addresses WHERE Address_Alias = '".$alias."' AND Email = '".$Email."' ";
      $result = Opencon()->query($get_details_of_alias);

      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $pickup_name = $row['Address_Name'];
          $pickup_mobile = $row['Address_Mobile'];
          $pickup_address_1 = $row['Address_Line_1'];
          $pickup_address_2 = $row['Address_Line_2'];
          $pickup_pincode = $row['Address_Pincode'];
        }
      }
      $id = array();
      $all_order_id = $_POST['hiddenF-address'];
      $all_ids = array($all_order_id);
      $individual_id = $all_ids[0];
      $id = explode(",", $individual_id);

      foreach ($id as $key) {
        $updateAddressQuery = Opencon()->query("UPDATE customer_orders SET Sender_Name = '".$pickup_name."', sender_mobile = '".$pickup_mobile."', sender_address_line_1 = '".$pickup_address_1."', sender_address_line_2 = '".$pickup_address_2."', sender_pincode = '".$pickup_pincode."' WHERE Order_ID = '".$key."'");
        echo '<script language="javascript">';
        echo 'alert("Address(es) Changed Successfully!");';
        echo 'location.href="user_orders.php";';
        echo '</script>';
      }
    }

    if(isset($_POST['search_sku'])) {
      $sku = $_POST['search_sku'];
      $sku = str_replace("'", "''", $sku);
      $query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'PROCESSING' AND SKU = '".$sku."' ";
      header("Location: user_orders.php?query=".$query."");
    }

    if(isset($_POST['cancelSelected'])) {
      $id = array();
      $all_order_id = $_POST['hiddenF'];
      $all_ids = array($all_order_id);
      $individual_id = $all_ids[0];
      $id = explode(",", $individual_id);

      foreach ($id as $key) {
        $cancelQueryResult = Opencon()->query("UPDATE customer_orders SET Status = 'CANCELLED' WHERE Order_ID = '".$key."'");
        echo '<script language="javascript">';
        echo 'alert("Order(s) Cancelled Successfully!");';
        echo 'location.href="user_orders.php";';
        echo '</script>';
      }
    }

    if(isset($_POST['change_w_d'])) {
      $id = array();
      $all_order_id = $_POST['hiddenF-weight'];
      $all_ids = array($all_order_id);
      $individual_id = $all_ids[0];
      $id = explode(",", $individual_id);

      $new_weight = $_POST['new_weight'];
      $new_length = $_POST['new_length'];
      $new_breadth = $_POST['new_breadth'];
      $new_height = $_POST['new_height'];

      foreach ($id as $key) {
        $updateWeightDimensionQuery = Opencon()->query("UPDATE customer_orders SET Weight = '".$new_weight."', length = '".$new_length."', breadth = '".$new_breadth."', height = '".$new_height."' WHERE Order_ID = '".$key."'");
        echo '<script language="javascript">';
        echo 'alert("Weight and Dimensions Changed Successfully!");';
        echo 'location.href="user_orders.php";';
        echo '</script>';
    }
    }

    if(isset($_POST['ship_selected'])) {
    $id = array();
    $all_order_id = $_POST['hiddenF'];
    $all_ids = array($all_order_id);
    $individual_id = $all_ids[0];
    $id = explode(",", $individual_id);

    foreach ($id as $key) {
      echo $key."<br>";
     }
    }

    if(isset($_POST['submit_new_sender_entries'])) {
      $for_order = $_POST['for_order'];
      $new_sender_name = $_POST['new_sender_name'];
      $new_sender_mobile = $_POST['new_sender_mobile'];
      $new_sender_address_1 = $_POST['new_sender_address_1'];
      $new_sender_address_2 = $_POST['new_sender_address_2'];
      $new_sender_pincode = $_POST['new_sender_pincode'];

      if(strlen($new_sender_mobile) == 14) {
        if(strlen($new_sender_pincode) == 6) {
          $result = Opencon()->query("UPDATE customer_orders SET Sender_Name = '".$new_sender_name."', sender_mobile = '".$new_sender_mobile."', sender_address_line_1 = '".$new_sender_address_1."', sender_address_line_2 = '".$new_sender_address_2."', sender_pincode = '".$new_sender_pincode."' WHERE Order_ID = '".$for_order."' ");

          if($result) {
            echo '<script language="javascript">';
              echo 'alert("Successfully Updated!");';
              echo 'location.href="user_orders.php";';
              echo '</script>';
          }
          else {
            echo '<script language="javascript">';
              echo 'alert("Some Error Occured!");';
              echo '</script>';
          }
        }
        else {
          echo '<script language="javascript">';
            echo 'alert("Invalid PinCode!");';
            echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
          echo 'alert("Invalid Mobile Number!");';
          echo '</script>';
      }
    }

    if(isset($_POST['submit_new_weight_entries'])) {
      $for_order = $_POST['for_order'];
      $new_length = $_POST['new_length'];
      $new_breadth = $_POST['new_breadth'];
      $new_height = $_POST['new_height'];
      $new_weight = $_POST['new_weight'];

      if($new_weight <= 0) {
        echo '<script language="javascript">';
          echo 'alert("Weight cannot be 0 or less than 0.");';
          echo '</script>';
      }
      else if($new_length > 0 && $new_breadth > 0 && $new_height > 0) {
        $result = Opencon()->query("UPDATE customer_orders SET Weight = '".$new_weight."', length = '".$new_length."', breadth = '".$new_breadth."', height = '".$new_height."' WHERE Order_ID = '".$for_order."' ");
        if($result) {
          echo '<script language="javascript">';
            echo 'alert("Successfully Updated!");';
            echo 'location.href="user_orders.php";';
            echo '</script>';
        }
        else {
          echo '<script language="javascript">';
            echo 'alert("Some Error Occured!");';
            echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
          echo 'alert("Dimensions cannot be 0 or less than 0.");';
          echo '</script>';
      }
    }

  ?>

  <div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="Recharge" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="Plans" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="Logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="KYC">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; background-color: #fafafa; padding-left: 0px;">

      <div class="individual_order_heading" style="width: 100%; min-width: 1000px; overflow-x: scroll; margin-bottom: 10px;">
        <div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 13px;">

          <button id="returns_orders" class="order-button" onclick="window.location.href='RETURNS'" style="width: 15%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: 0;"><i class="fa fa-undo"></i>  Returns  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;"><?php echo $return_orders_number;?></p></span></button>
        </div>
        <div id="processing_orders_div" style="width: 100%; float: left; min-width: 1000px;">

          <div style="width: 100%; float: left; background-color: #fafafa; padding-top: 5px; padding-bottom: 5px;">

            <form method="post" enctype="multipart/form-data" name="print_invoice_form">
              <input type="hidden" id="hiddenF-print-invoice" name="hiddenF-print-invoice">
              <div style="width: 7%; float: left; margin-top: 10px;">
                <select id="printOrInvoice" name="printOrInvoice" onchange="print_invoice_form.submit();" style="width: 80%; height: 25px; border-radius: 3px;"><option>Print</option><option value="Print">Label</option></select>
              </div>
            </form>
            

            <form method="post" enctype="multipart/form-data" name="address_change_form">
              <input type="hidden" id="hiddenF-address" name="hiddenF-address">
              <div style="width: 21%; float: left; margin-top: 10px;">
                <select id="address" name="changePickupAddress" onchange="address_change_form.submit();" style="float: left; width: 95%; height: 25px; border-radius: 3px;"><option value="None">Select Pickup Address</option><?php    
                foreach ($total_addresses as $address) {
                  $alias = $address['Address_Alias'];
                  echo '<option value="'.$alias.'">'.$alias.'</option>';
                }

                ?></select><br><br>
              </div>
            </form>

            <form method="post" enctype="multipart/form-data">
              <input type="hidden" id="hiddenF" name="hiddenF">
              
              <div style="width: 9%; float: left; margin-top: 10px;">
              <button id="ship_selected" name="ship_selected" style="float: left; border: 0.5px solid rgb(169, 169, 169); background-color: transparent; padding: 3px 20px 2px 20px;">Initiate Returns</button>
              </div>
              <div style="width: 9%; float: left; margin-top: 10px;">
                <button id="cancelSelected" name="cancelSelected" style="float: left; border: 0.5px solid rgb(169, 169, 169); background-color: transparent; padding: 3px 20px 2px 20px;">Cancel Returns</button>
              </div>
            </form>

            <div style="width: 14%; float: left; margin-top: 10px;">
                <button onclick="showWeightChangeBox();" id="change_weight_dimension" style="float: left; border: 0.5px solid rgb(169, 169, 169); background-color: transparent; padding: 3px 20px 2px 20px;">Change Weight and Dimension</button>

                <div id="weight_dimension" style="float: left; width: 100%; background-color: white; display: none;">
                <form method="post" enctype="multipart/form-data">
                  <input type="hidden" id="hiddenF-weight" name="hiddenF-weight">

                  <div style="float: left; margin-top: 10px; width: 100%;">
                    <p align="left"><b>Weight : </b><span><input type="number" name="new_weight" min="0.1" step="0.1" value="0.1" style="border-radius: 3px; width: 60%;"><b> (kg)</b></span></p>
                  </div>

                  <div style="float: left; width: 100%; display: inline-block;">
                    <input type="number" name="new_length" min="0.1" step="0.1" value="10" style="border-radius: 3px; width: 21%; float: left;" placeholder="L">
                    <p style="float: left; margin-left: 5px; margin-right: 5px;"><b> X </b></p>
                    <input type="number" name="new_breadth" min="0.1" step="0.1" value="10" style="border-radius: 3px; width: 21%; float: left;" placeholder="B">
                    <p style="float: left; margin-left: 5px; margin-right: 5px;"><b> X </b></p>
                    <input type="number" name="new_height" min="0.1" step="0.1" value="10" style="border-radius: 3px; width: 21%; float: left;" placeholder="H">
                    <p style="float: left; margin-left: 5px; margin-right: 5px;"><b> (cms) </b></p>
                  </div>
                  <button name="change_w_d" class="btn btn-success" style="float: left;"><i class="fa fa-check"></i></button>
                </form>
                <button onclick="hideWeightChangeBox()" class="btn btn-danger" style="float: left; margin-left: 10px;"><i class="fa fa-close"></i></button>
              </div>

            </div>

            <div style="float: right; margin-right: 10px;">
              <button class="addOrderButton" onclick="location.href='create_return_order';"><i class="fa fa-plus"></i>&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAdd Order</button>
              <button class="otherProcessingButtons" title="Download Orders"><i class="fa fa-level-down"></i></button>
            </div>
          </div>

          <div style="width: 100%; float: left; background-color: rgba(0,0,0,0.07); ">
            <div style="width: 2%; float: left; border: 0.5px solid #ddd; border-bottom: none;">
              <input type="checkbox" id="checkUncheckAll" onclick="CheckUncheckAll()" style="margin-top: 13px; padding-bottom: 10px;">
            </div>

            <div style="width: 11%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>ORDER DATE</b><span style="color: #285fdb; padding: 10px;"><a href="#choose_date" style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showDateBox();"><i class="fa fa-calendar"></i></a></span></p>
              <div id="SEARCH_BY_DATE" style="width: 100%; padding: 5px; display: none;">
                <form method="post" enctype="multipart/form-data">
                  
                </form>
              </div>
            </div>

            <div style="width: 11%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>CHANNEL</b><span style="color: #285fdb; padding: 10px;"><button style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showChannelBox();"><i class="fa fa-filter"></i></button></span></p>
              <div id="SEARCH_BY_CHANNEL" style="width: 100%; padding: 5px; float: left; display: none;">
                <form method="post" enctype="multipart/form-data">
                  <p align="Left" style="margin-left: 5px; width: 100%;"><b>Filter by Channel</b></p>
                  <input style="float: left; margin-left: 10px;" type="radio" name="channel_select" value="CUSTOM"><span><p style="float: left; color: #656565; width: 37%;">Custom</p></span><br><br>
                  <input style="float: left; margin-left: 10px; margin-top: -5px;" type="radio" name="channel_select" value="AMAZON"><span><p style="float: left; color: #656565; width: 37%; margin-top: -8px;">Amazon</p></span><br><br>
                  <button name="search_by_channel" class="btn btn-success" style="float: left; margin-left: 20px; margin-top: -12px; margin-bottom: 5px;"><i class="fa fa-check"></i></button>
                </form>
                <button class="btn btn-danger" onclick="hideChannelBox();" style="float: left; margin-left: 20px; margin-top: -12px; margin-bottom: 5px;"><i class="fa fa-close"></i></button>
              </div>
            </div>

            <div style="width: 12%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>ORDER ID / STATUS</b><span style="color: #285fdb; padding: 10px;"><button style="background-color: Transparent;background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none;" onclick="showSearchBox();"><i class="fa fa-search"></i></button></span></p>
              <div id="SEARCH_BY_ID" style="width: 100%; padding: 5px; display: none; float: left;">
                <form method="post" enctype="multipart/form-data">
                  <p align="Left"><b>Search by Order ID</b></p>
                  <input name="searchOrderId" style="border-radius: 3px; padding: 5px; margin-top: 7px;" type="text" name="ORDER_ID">
                  <button name="search_by_id" class="btn btn-success" style="float: left; margin-left: 20px; margin-top: 27px; margin-bottom: 5px;"><i class="fa fa-check"></i></button>
                </form>
                <button class="btn btn-danger" onclick="hideSearchBox();" style="float: left; margin-left: 20px; margin-top: 27px; margin-bottom: 5px;"><i class="fa fa-close"></i></button>
              </div>
            </div>
            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>PRODUCT DETAILS</b><span style="color: #285fdb; padding: 10px;">
              <button onclick="showSKUBox();" title="Search By SKU" style="background: transparent; border: none;"><i class="fa fa-filter"></i></button></span></p>
              <div id="SEARCH_BY_SKU" style="width: 100%; padding: 5px; float: left; display: none;">
                <form method="post" enctype="multipart/form-data" name="search_sku_form">
                  <p align="Left" style="margin-left: 5px; width: 100%;"><b>Search By SKU</b></p>
                  <select name="search_sku" style="float: left; width: 100%;" onchange="search_sku_form.submit();">
                    <option value="None">Select SKU</option>
                    <?php 
                    foreach ($total_SKU as $sku) {
                      $SKU_name = $sku['SKU'];
                      if(!empty($SKU_name)) {
                        echo '<option value="'.$SKU_name.'">'.$SKU_name.'</option>';
                      }
                    }
                     ?>
                  </select>
                </form>
                <button class="btn btn-danger" onclick="hideSKUBox();" style="float: left; margin-top: 5px;"><i class="fa fa-close"></i></button>
              </div>
            </div>

            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">

              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>PAYMENT</b><span style="color: #285fdb; padding: 10px;">

              <button id="payment_asc" onclick="sort_payment(this.id);" title="Sort orders in Increasing order" style="background: transparent; border: none;"><i class="fa  fa-arrow-up"></i></button>

              <button id="payment_desc" onclick="sort_payment(this.id);" title="Sort orders in Decreasing order" style="background: transparent; border: none;"><i class="fa  fa-arrow-down"></i></button>

              </span></p>
            </div>

            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>CUSTOMER DETAILS</b></p>
            </div>
            <div style="width: 10%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>PICKUP ADDRESS</b></p>
            </div>
            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>DIMENSIONS & WEIGHT</b></p>
            </div>
            <div style="width: 10%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 12px;"><b>ACTIONS</b></p>
            </div>
          </div>

          <?php

            $processing_orders_query_page = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'PROCESSING'";
            $result = Opencon()->query($processing_orders_query_page);
            if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $processing_orders_array_page[] = $row;
              }
            }
            else {
                $processing_orders_array_page = array();
              }

            $number_of_processing_orders = count($processing_orders_array_page);
            $number_of_pages = ceil($number_of_processing_orders/$items_per_page);

            if(count($processing_orders_array) == 0) {
              echo '<div style="width: 100%;">';
                echo '<img src="images/no_data.png" style="margin-top: 50px;"';
                echo '<br>';
                echo '<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>';
              echo '</div>';
            }
            else {
              foreach ($processing_orders_array as $individual_processing_order) {
              $order_date = $individual_processing_order['order_date'];
              $channel = $individual_processing_order['Channel'];
              $order_id = $individual_processing_order['Order_ID'];
              $status = $individual_processing_order['Status'];
              $Order_Type = $individual_processing_order['Order_Type'];
              $product_details = $individual_processing_order['product_details'];

              $sender_name = $individual_processing_order['Sender_Name'];
              $sender_mobile = $individual_processing_order['sender_mobile'];
              $sender_address_line1 = $individual_processing_order['sender_address_line_1'];
              $sender_address_line2 = $individual_processing_order['sender_address_line_2'];
              $sender_pincode = $individual_processing_order['sender_pincode'];

              $customer_name = $individual_processing_order['Customer_Name'];
              $customer_mobile = $individual_processing_order['customer_mobile'];
              $customer_address_line1 = $individual_processing_order['customer_address_line_1'];
              $customer_address_line2 = $individual_processing_order['customer_address_line_2'];
              $customer_pincode = $individual_processing_order['customer_pincode'];

              $length = $individual_processing_order['length'];
              $breadth = $individual_processing_order['breadth'];
              $height = $individual_processing_order['height'];

              $weight = $individual_processing_order['Weight'];
              $price = $individual_processing_order['Price'];
              $isCOD = $individual_processing_order['COD'];

              $new_invoice_status = $individual_processing_order['new_invoice_status'];

              $volumetric_weight = ($length * $breadth * $height)/5000;

            echo'<div style="width: 100%; float: left; background-color: #fafafa; border: 2px solid #ddd;">';
            
            echo'<form method="post" enctype="multipart/form-data">';
              echo'<div style="width: 2%; float: left;">';
              echo'<input type="checkbox" name="rowSelectCheckBox" value="'.$order_id.'" style="margin-top: 13px; padding-bottom: 10px;" onclick="CheckUncheckOne()">';
            echo'</div>';
            echo'</form>';

            echo'<div style="width: 11%; float: left; padding-left: 10px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$order_date.'</p>';
            echo'</div>';
            echo'<div style="width: 11%; float: left; padding-left: 15px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-shopping-cart"></i> <br> '.$channel.'</p>';
            echo'</div>';
            echo'<div style="width: 12%; float: left; padding-left: 20px; padding-top: 8px;">';
              echo'<a href="documents/show_invoice.php?order='.$order_id.'" style="font-size: 15px; float: left; color: #285fdb;">'.$order_id.'</a> <br> <span style="color: orange; float: left; margin-left: -104px;"><b>'.$new_invoice_status.'</b></span>';
            echo'</div>';
            echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style="font-size: 15px; color: #404040; margin-top: 10px;">'.$product_details.'</p>';
            echo'</div>';
            if(strcmp($isCOD, 'No')==0) {
              echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-inr"></i> '.$price.' <br> <span style="background-color:#27c24c; color: white; padding: 4px; border-radius: 12px; font-size: 10px;"><b>PREPAID</b></span> </p>';
              echo'</div>';
            }
            else {
            echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><i class="fa fa-inr"></i> '.$price.'<br> <span style="background-color: orange; color: white; padding: 4px; border-radius: 12px; font-size: 10px;"><b>POSTPAID</b></span> </p>';
            echo'</div>';
            }
            echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$customer_name.' <br> '.$customer_mobile.' <br> '.$customer_address_line1.' <br>'.$customer_address_line2.' <br>'.$customer_pincode.'</p>';
            echo'</div>';

            echo'<div style="width: 10%; float: left; padding-left: 20px;">';

              echo'<div id="'.$order_id.'second_div_sender" style="display: none;">';
                echo'<div style="float: left; margin-left: -16px; margin-top: 10px;">';
                  echo '<form method="post" enctype="multipart/form-data">';
                    echo '<input type="text" name="new_sender_name" value="'.$sender_name.'" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<input type="text" name="new_sender_mobile" value="'.$sender_mobile.'" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<input type="text" name="new_sender_address_1" value="'.$sender_address_line1.'" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<input type="text" name="new_sender_address_2" value="'.$sender_address_line2.'" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<input type="number" name="new_sender_pincode" value="'.$sender_pincode.'" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<input type="hidden" name="for_order" value="'.$order_id.'" placeholder="Pincode" style="border-radius: 3px; margin-bottom: 2px;">';
                    echo '<button name="submit_new_sender_entries" class="btn btn-success" style="display:inline-block; float: left; padding: 5px 10px 5px 10px; margin-top: 2px; margin-left: 3px;"><i class="fa fa-check"></i></button>';
                  echo '</form>';
                  echo '<button id="'.$order_id.'_sender" onclick="hideDiv(this.id)" class="btn btn-success" style="background-color: #f05050; display:inline-block; float: left; padding: 5px 10px 5px 10px; margin-top: 2px; margin-left: 3px;"><i class="fa fa-close"></i></button>';
                echo'</div>';
              echo'</div>';

              echo'<div id="'.$order_id.'main_div_sender">';
              echo '<button id="'.$order_id.'_sender" onclick="showDiv(this.id)" style="background-color: transparent; border: none; float: right; margin-top: 10px;"><i class="fa fa-edit"></i></button>';
              echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;">'.$sender_name.' <br> '.$sender_mobile.' <br> '.$sender_address_line1.' <br>'.$sender_address_line2.' <br>'.$sender_pincode.'</p>';
              echo'</div>';

            echo'</div>';

            echo'<div style="width: 11%; float: left; padding-left: 20px;">';

              echo'<div id="'.$order_id.'second_div" style="display: none;">';
                  echo'<div style="float: left; margin-left: -16px; margin-top: 10px;">';
                    echo '<form method="post" enctype="multipart/form-data">';
                      echo '<input type="text" size="16" name="new_weight" value="'.$weight.'" style="border-radius: 3px; display: inline-block; margin-bottom: 10px;"> (kg)';
                      echo '<input type="text" size="1" name="new_length" value="'.$length.'" style="border-radius: 3px; margin-bottom: 2px; float: left; margin-left: 10px;">';
                      echo'<p style="float: left; margin-left: 5px; margin-right: 5px;"> x </p>';
                      echo '<input type="text" size="1" name="new_breadth" value="'.$breadth.'" style="border-radius: 3px; margin-bottom: 2px; float: left; ">';
                      echo'<p style="float: left; margin-left: 5px; margin-right: 5px;"> x </p>';
                      echo '<input type="text" size="1" name="new_height" value="'.$height.'" style="border-radius: 3px; margin-bottom: 2px; float: left;">';
                      echo '<input type="hidden" name="for_order" value="'.$order_id.'" placeholder="Pincode" style="border-radius: 3px; margin-bottom: 2px;">';
                      echo '<button name="submit_new_weight_entries" class="btn btn-success" style="display:inline-block; float: left; padding: 5px 10px 5px 10px; margin-top: 10px; margin-left: -19px;"><i class="fa fa-check"></i></button>';
                    echo '</form>';
                    echo '<button id="'.$order_id.'" onclick="hideDiv(this.id)" class="btn btn-success" style="background-color: #f05050; display:inline-block; float: left; padding: 5px 10px 5px 10px; margin-top: 10px; margin-left: 3px;"><i class="fa fa-close"></i></button>';
                  echo'</div>';
                echo'</div>';

                echo'<div id="'.$order_id.'main_div">';
                echo '<button id="'.$order_id.'" onclick="showDiv(this.id)" style="background-color: transparent; border: none; float: right; margin-top: 10px;"><i class="fa fa-edit"></i></button>';
                echo'<p align="left" style=" font-size: 15px; color: #404040; margin-top: 10px;"><b>Weight : </b>'.$weight.' <br><b> Dimensions : </b>'.$length.' x '.$breadth.' x '.$height.' <br><b> Volumetric : </b>'.$volumetric_weight.' kg</p>';
                echo'</div>';

            echo'</div>';


            echo'<form method="post" enctype="multipart/form-data">';
              echo'<div style="width: 10%; float: left; padding-left: 20px;">';
                echo'<button class="shipButton" name="chooseOperator">Initate Return</button>';

                echo'<button class="cancelButton" name="cancelOrder">Cancel Return</button>';
                echo'<input type="hidden" name="orderID" value="'.$order_id.'" style="float: left;"></p>';
                echo'<input type="hidden" name="order_type" value="'.$Order_Type.'" style="float: left;"></p>';

                echo'<input type="hidden" name="fromName" value="'.$sender_name.'" style="float: left;"></p>';
                echo'<input type="hidden" name="fromMobile" value="'.$sender_mobile.'" style="float: left;"></p>';
                echo'<input type="hidden" name="fromAddressLine1" value="'.$sender_address_line1.'" style="float: left;"></p>';
                echo'<input type="hidden" name="fromAddressLine2" value="'.$sender_address_line2.'" style="float: left;"></p>';
                echo'<input type="hidden" name="fromCode" value="'.$sender_pincode.'" style="float: left;"></p>';

                echo'<input type="hidden" name="toName" value="'.$customer_name.'" style="float: left;"></p>';
                echo'<input type="hidden" name="toMobile" value="'.$customer_mobile.'" style="float: left;"></p>';
                echo'<input type="hidden" name="toAddressLine1" value="'.$customer_address_line1.'" style="float: left;"></p>';
                echo'<input type="hidden" name="toAddressLine2" value="'.$customer_address_line2.'" style="float: left;"></p>';
                echo'<input type="hidden" name="toCode" value="'.$customer_pincode.'" style="float: left;"></p>';

                echo'<input type="hidden" name="product_details" value="'.$product_details.'" style="float: left;"></p>';
                echo'<input type="hidden" name="length" value="'.$length.'" style="float: left;"></p>';
                echo'<input type="hidden" name="breadth" value="'.$breadth.'" style="float: left;"></p>';
                echo'<input type="hidden" name="height" value="'.$height.'" style="float: left;"></p>';
                echo'<input type="hidden" name="weight" value="'.$weight.'" style="float: left;"></p>';
                echo'<input type="hidden" name="COD" value="'.$isCOD.'" style="float: left;"></p>';

              echo'</div>';
            echo'</form>';
            echo'</div>';

            }
            }
            ?>

            <div class="footer">

              <?php

              echo'<div style="width: 100%; float:left;">';
      
                echo'<div style="width: 40%; float: left; margin-top: 15px;">';
                  echo '<p style="color: #656565; float: left; padding-left: 20px; font-size: 13px;">Show&nbsp<b><span> <select id="selectBox" onchange="window.location.href = this.value">  
                    <option label="25" value="RETURNS?items_per_page=25">25</option>
                    <option label="50" value="RETURNS?items_per_page=50">50</option>
                    <option label="100" value="RETURNS?items_per_page=100">100</option> 
                    </select> </span></b>&nbspitems per page</p>';
                echo'</div>';

                echo'<div style="width: 39%; float: left; margin-top: 15px;">';
                  if($processing_page=="") {
                        $processing_page = 1;
                      }
                  echo '<p style="color: #656565; float: left; font-size: 13px; ">Page Number : '.$processing_page.'</p>';
                echo'</div>';

                echo'<div style="float: left; margin-top: 10px;">';
                  for($b=1;$b<=$number_of_pages;$b++) {
                      ?><a class="page_number_list" href="RETURNS?processing_page=<?php echo $b; ?>&items_per_page=<?php echo $items_per_page; ?>" style="text-decoration: none;"><?php echo $b." "; ?></a> <?php
                      }
                 echo'</div>';
              echo'</div>';

              ?>
            </div>
        </div>

        <div id="choose_date" class="overlay">
            <div class="popup">
              <a id="close_date_chooser" class="close" href="#">&times;</a>
              <div class="content">
                <form method="post" enctype="multipart/form-data">
                  <div style="width: 100%; float: left;">
                    <button name="all_orders" class="date_range">All</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="today_date_orders" class="date_range">Today</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="yesterday_date_orders" class="date_range">Yesterday</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="seven_days_orders" class="date_range">Last 7 days</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="thirty_days_orders" class="date_range">Last 30 days</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="month_orders" class="date_range">This Month</button>
                  </div>
                  <div style="width: 100%; float: left;">
                    <button name="last_month_orders" class="date_range">Last Month</button>
                  </div>
                </form>
                <div style="width: 100%; float: left;">
                  <?php $date = date('Y-m-d'); ?>
                    <button class="date_range" onclick="showCalendar();">Custom Range</button>
                    <div id="date_picker" style="float: left; padding: 10px; display: none;">
                      <form method="post" enctype="multipart/form-data">
                        <label style="float: left; margin-top: 2px; margin-right: 2px;">From : </label>
                        <input id="from_date" style="float: left; border-radius: 3px;" type="date" name="from_date" value="<?php echo $date; ?>" required>

                        <label style="float: left; margin-top: 2px; margin-right: 2px; margin-left: 10px;">To : </label>
                        <input id="to_date" style="float: left; border-radius: 3px;" type="date" name="to_date" value="<?php echo $date; ?>" required>

                        <div style="width: 42%; float: left;">
                          <button id="apply_button" name="custom_date_range" class="btn btn-success" style="float: left; margin-top: 7px; margin-left: 40px; background-color: #27c24c; border: none;">Apply</button>
                        </div>
                      </form>
                      <div style="float: left;">
                          <button class="btn btn-danger" style="float: left; margin-top: 7px; margin-left: -50px;" onclick="hideCalendar();">Cancel</button>
                        </div>
                      <div id="wrong_date" style="width: 100%; float: left; margin-top: 10px; display: none;">
                        <p align="left" style="color: red;">'From' Date cannot be greater than 'To' Date!</p>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/showOrders.js"></script>
<script src="js/changeEntryHide.js"></script>

<script type="text/javascript">
  function hideDiv(id) {
    var ID = id;
    if(ID.indexOf('_') > -1) {
      ID = ID.substring(0, ID.indexOf("_"));
      document.getElementById(ID+'main_div_sender').style.display='block';
      document.getElementById(ID+'second_div_sender').style.display='none';
    }
    document.getElementById(ID+'main_div').style.display='block';
    document.getElementById(ID+'second_div').style.display='none';
  }
</script>

<script type="text/javascript">
  function showDiv(id) {
    var ID = id;
    if(ID.indexOf('_') > -1) {
      ID = ID.substring(0, ID.indexOf("_"));
      if(document.getElementById(ID+'main_div_sender').style.display=='block') {
        document.getElementById(ID+'second_div_sender').style.display='block';
        document.getElementById(ID+'main_div_sender').style.display='none';
      }
      else {
        document.getElementById(ID+'second_div_sender').style.display='none';
        document.getElementById(ID+'main_div_sender').style.display='block';
      }
    }
    else {
      if(document.getElementById(ID+'main_div').style.display=='block') {
        document.getElementById(ID+'second_div').style.display='block';
        document.getElementById(ID+'main_div').style.display='none';
      }
      else {
        document.getElementById(ID+'second_div').style.display='none';
        document.getElementById(ID+'main_div').style.display='block';
      }
    }
  }
</script>

<script type="text/javascript">
  window.onload = function() {
    var items_value = '<?php echo $items_per_page ?>';
    if(items_value == 25) {
      var index  = 0;
    }
    else if(items_value == 50) {
      var index  = 1;
    }
    else if(items_value == 100) {
      var index  = 2;
    }
    document.getElementById("selectBox").selectedIndex = index;
    document.getElementById('close_date_chooser').click();

    var url = window.location.href;
    if(url.includes("?")) {
      var new_url = url.substr(0, url.indexOf("?"));
      window.history.pushState("NEW", "NEW", new_url);
    }
}
</script>

<script type="text/javascript">
  
  function CheckUncheckAll(){
  var all_order_id = new Array();
   var  selectAllCheckbox=document.getElementById("checkUncheckAll");
   if(selectAllCheckbox.checked==true){
    var checkboxes =  document.getElementsByName("rowSelectCheckBox");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = true;
      all_order_id[i] = checkboxes[i].value;
     }
     document.getElementById("hiddenF").value = all_order_id;
     document.getElementById("hiddenF-address").value = all_order_id;
     document.getElementById("hiddenF-print-invoice").value = all_order_id;
     document.getElementById("hiddenF-weight").value = all_order_id;
    }else {
     var checkboxes =  document.getElementsByName("rowSelectCheckBox");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = false;
     }
    }
   }

   function CheckUncheckOne() {
    var all_order_id = new Array();
    checkboxes = document.getElementsByName("rowSelectCheckBox");
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if(checkboxes[i].checked == true) {
        all_order_id[i] = checkboxes[i].value;
      }
    }
    document.getElementById("hiddenF").value = all_order_id;
    document.getElementById("hiddenF-address").value = all_order_id;
    document.getElementById("hiddenF-print-invoice").value = all_order_id;
    document.getElementById("hiddenF-weight").value = all_order_id;
   }
   
</script>
<script type="text/javascript">
  window.setInterval(function(){
  var p=0;
  var checkboxes = document.getElementsByName("rowSelectCheckBox");
  for(var i=0, n=checkboxes.length;i<n;i++) {
    if(checkboxes[i].checked==true) {
      p++;
    }
  }
  if(p > 0) {
    document.getElementById("ship_selected").style.cursor = "pointer";
    document.getElementById("cancelSelected").style.cursor = "pointer";
    document.getElementById("printOrInvoice").style.cursor = "pointer";
    document.getElementById("change_weight_dimension").style.cursor = "pointer";
    document.getElementById("address").style.cursor = "pointer";
    document.getElementById("ship_selected").disabled = false;
    document.getElementById("cancelSelected").disabled = false;
    document.getElementById("printOrInvoice").disabled = false;
    document.getElementById("change_weight_dimension").disabled = false;
    document.getElementById("address").disabled = false;
    document.getElementById("ship_selected").style.backgroundColor = "#fafafa";
    document.getElementById("ship_selected").style.color = "#404040";
    document.getElementById("cancelSelected").style.backgroundColor = "#fafafa";
    document.getElementById("cancelSelected").style.color = "#404040";
    document.getElementById("printOrInvoice").style.backgroundColor = "#fafafa";
    document.getElementById("printOrInvoice").style.color = "#404040";
    document.getElementById("change_weight_dimension").style.backgroundColor = "#fafafa";
    document.getElementById("change_weight_dimension").style.color = "#404040";
    document.getElementById("address").style.backgroundColor = "#fafafa";
    document.getElementById("address").style.color = "#404040";
  }
  else {
    document.getElementById("ship_selected").style.cursor = "not-allowed";
    document.getElementById("cancelSelected").style.cursor = "not-allowed";
    document.getElementById("printOrInvoice").style.cursor = "not-allowed";
    document.getElementById("change_weight_dimension").style.cursor = "not-allowed";
    document.getElementById("address").style.cursor = "not-allowed";
    document.getElementById("ship_selected").disabled = true;
    document.getElementById("cancelSelected").disabled = true;
    document.getElementById("printOrInvoice").disabled = true;
    document.getElementById("change_weight_dimension").disabled = true;
    document.getElementById("address").disabled = true;
    document.getElementById("ship_selected").style.backgroundColor = "#eee";
    document.getElementById("ship_selected").style.color = "#ddd";
    document.getElementById("cancelSelected").style.backgroundColor = "#eee";
    document.getElementById("cancelSelected").style.color = "#ddd";
    document.getElementById("printOrInvoice").style.backgroundColor = "#eee";
    document.getElementById("printOrInvoice").style.color = "#ddd";
    document.getElementById("change_weight_dimension").style.backgroundColor = "#eee";
    document.getElementById("change_weight_dimension").style.color = "#ddd";
    document.getElementById("address").style.backgroundColor = "#eee";
    document.getElementById("address").style.color = "#ddd";
  }
}, 1);
</script>

<script type="text/javascript">
  function sort_payment(id) {
    if((id.localeCompare("payment_asc"))==0) {
      var email = '<?php echo($Email); ?>';
      var processing_page_1 = '<?php echo($processing_page1); ?>';
      var items_per_page = '<?php echo($items_per_page); ?>';

      window.location.href = "RETURNS?query=SELECT * FROM customer_orders WHERE Email = '"+email+"' AND Status = 'RETURN' ORDER BY Price LIMIT "+processing_page_1+","+items_per_page;
    }
    else {
      var email = '<?php echo($Email); ?>';
      var processing_page_1 = '<?php echo($processing_page1); ?>';
      var items_per_page = '<?php echo($items_per_page); ?>';

      window.location.href = "RETURNS?query=SELECT * FROM customer_orders WHERE Email = '"+email+"' AND Status = 'RETURN' ORDER BY Price DESC LIMIT "+processing_page_1+","+items_per_page;
    }
  }
</script>

<script type="text/javascript">
  function showSearchBox() {
    document.getElementById("SEARCH_BY_ID").style.display = 'block';
  }
  function hideSearchBox() {
    document.getElementById("SEARCH_BY_ID").style.display = 'none';
  }
  function showChannelBox() {
    document.getElementById("SEARCH_BY_CHANNEL").style.display = 'block';
  }
  function hideChannelBox() {
    document.getElementById("SEARCH_BY_CHANNEL").style.display = 'none';
  }
  function showCalendar() {
    document.getElementById("date_picker").style.display = 'block';
  }
  function hideCalendar() {
    document.getElementById("date_picker").style.display = 'none';
  }
  function showSKUBox() {
    document.getElementById("SEARCH_BY_SKU").style.display = 'block';
  }
  function hideSKUBox() {
    document.getElementById("SEARCH_BY_SKU").style.display = 'none';
  }
  function showWeightChangeBox() {
    document.getElementById("weight_dimension").style.display = 'block';
  }
  function hideWeightChangeBox() {
    document.getElementById("weight_dimension").style.display = 'none';
  }
</script>

<script type="text/javascript">
  window.setInterval(function() {
    var from_date = document.getElementById("from_date").value;
    var to_date = document.getElementById("to_date").value;
    
    if(from_date > to_date) {
      document.getElementById("wrong_date").style.display = 'block';
      document.getElementById("apply_button").style.cursor = 'not-allowed';
      document.getElementById("apply_button").disabled = true;
      document.getElementById("apply_button").style.backgroundColor = "#eee";
      document.getElementById("apply_button").style.color = "#ddd";
    }
    else {
      document.getElementById("wrong_date").style.display = 'none';
      document.getElementById("apply_button").style.cursor = 'pointer';
      document.getElementById("apply_button").disabled = false;
      document.getElementById("apply_button").style.backgroundColor = "#27c24c";
      document.getElementById("apply_button").style.color = "white";
      
    }
  }, 1);
</script>

</body>
</html>
