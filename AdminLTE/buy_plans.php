<!DOCTYPE html>
<html>
<head>
  <title>Plans</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Logistics Company | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" type="text/css" href="css/individual-document.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<?php

include 'conn.php';
  function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $user_id = $_SESSION['User_ID'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if(!Opencon()) {
    die("Connection Failed :".mysqli_connect_error());
    }
    else {
      */
      $updateDetails = "SELECT * from customer_details WHERE Email = '".$Email."'";
      $result = Opencon() -> query($updateDetails);

      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $wallet = $row['Wallet'];
        }
      }
      $_SESSION['Wallet'] = $wallet;
   // }

?>

<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="Recharge" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="Plans" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="KYC">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: white;">

    <!-- Main content -->
    <section class="content" style="text-align: center; width: 100%; overflow-x: scroll; min-width: 1000px;">

    	<div>
        	<p style="font-size: 30px; padding: 29px; color: #656565;">Launch your business today</p>
        	<p style="margin-top: -36px; font-size: 12px; color: #656565; padding-bottom: 2px;">Select the plan in different options that best fit your project</p>
      	</div>

      	<div class="individual-order-heading" style="width: 80%; background-color: white;">

      		<div style="width: 29%; float: left;">
      			<div>
      				<p align="Left" style="font-size: 30px; color: #3c414c; margin-top: 50px;">Plans We Offer!</p>
      			</div>
      			<div>
      				<p align="Left" style="font-size: 13px; color: #888888;">Check out our plans and choose the right plan according to your business needs. Or you can call at NUMBER to talk to our shipping experts.</p>
      			</div>
      		</div>

      		<div style="width: 8%; float: left; border-left: solid 1px #f0f0f0; padding-left: 60px; padding-right: 110px;">
      			<img src="images/Lite.png" style="margin-top: 30px; margin-left: -25px; height: 87px;">
      			<p style="font-size: 20px; color: #3c414c;"><b>LITE</b></p>
      			<p style="font-size: 20px; color: #0071b2;"><b>FREE</b></p>
      		</div>
      		<div style="width: 15%; float: left; border-left: solid 1px #f0f0f0;">
      			<img src="images/Basic.png" style="margin-top: 33px; height: 87px;">
      			<p style="font-size: 20px; color: #3c414c;"><b>BASIC</b></p>
      			<p style="font-size: 20px; color: #0071b2;"><b><i class="fa fa-rupee"></i>1000/Month</b></p>
      		</div>
      		<div style="width: 15%; float: left; border-left: solid 1px #f0f0f0;">
      			<img src="images/Advanced.png" style="margin-top: 33px; height: 87px;">
      			<p style="font-size: 20px; color: #3c414c;"><b>ADVANCED</b></p>
      			<p style="font-size: 20px; color: #0071b2;"><b><i class="fa fa-rupee"></i>2000/Month</b></p>
      		</div>
      		<div style="width: 15%; float: left; border-left: solid 1px #f0f0f0;">
      			<img src="images/Pro.png" style="margin-top: 33px; height: 87px;">
      			<p style="font-size: 20px; color: #3c414c;"><b>PRO</b></p>
      			<p style="font-size: 20px; color: #0071b2;"><b><i class="fa fa-rupee"></i>3000/Month</b></p>
      		</div>

      	</div>
      	<div class="individual-order-heading" style="width: 80%; background-color: #edf1f2;">
      		<div style="width: 71.5%; float: right;">
      			<?php
      			  if(strcmp($member_plan, 'LITE')==0) {
      			  	echo '<input type="hidden" id="hiddenF" name="hiddenF">
      			  		  <div style="width: 20%; float: left;">
      					  <button class="btn btn-success" style="background-color: #3f9c3f; padding: 6px 16px;"><i class="fa fa-check-circle"></i>&nbsp&nbspActive</button>
      					  </div>';
      			  }
      			  else {
      			  	echo '<input type="hidden" id="hiddenF" name="hiddenF">
      			  		  <div style="width: 20%; float: left;">
      					  <button class="btn btn-primary" id="LITE" onclick="trial(this.id);" style="background-color: #1b72e2; padding: 6px 16px;">Activate</button>
      					  </div>';
      			  }
      			  if(strcmp($member_plan, 'BASIC')==0) {
      			  	echo '<div style="width: 20%; float: left;">
      					  <button class="btn btn-success" style="background-color: #3f9c3f; padding: 6px 16px;"><i class="fa fa-check-circle"></i>&nbsp&nbspActive</button>
      					  </div>';
      			  }
      			  else {
      			  	echo '<div style="width: 20%; float: left;">
      					  <button class="btn btn-primary" id="BASIC" onclick="trial(this.id);" style="background-color: #1b72e2; padding: 6px 16px;">Activate</button>
      					  </div>';
      			  }
      			  if(strcmp($member_plan, 'ADVANCED')==0) {
      			  	echo '<div style="width: 22%; float: left;">
      					  <button class="btn btn-success" style="background-color: #3f9c3f; padding: 6px 16px;"><i class="fa fa-check-circle"></i>&nbsp&nbspActive</button>
      					  </div>';
      			  }
      			  else {
      			  	echo '<div style="width: 22%; float: left;">
      					  <button class="btn btn-primary" id="ADVANCED" onclick="trial(this.id);" style="background-color: #1b72e2; padding: 6px 16px;">Activate</button>
      					  </div>';
      			  }
      			  if(strcmp($member_plan, 'PRO')==0) {
      			  	echo '<div style="width: 21%; float: left;">
      					  <button class="btn btn-success" style="background-color: #3f9c3f; padding: 6px 16px;"><i class="fa fa-check-circle"></i>&nbsp&nbspActive</button>
      					  </div>';
      			  }
      			  else {
      			  	echo '<div style="width: 21%; float: left;">
      					  <button class="btn btn-primary" id="PRO" onclick="trial(this.id);" style="background-color: #1b72e2; padding: 6px 16px;">Activate</button>
      					  </div>';
      			  }
      			?>
      		</div>
      	</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript">
	function trial(id) {
		var ID = id;
		if (confirm('Are you sure you want to activate this plan?')) {
			var val = document.getElementById("hiddenF").value = ID;
			window.location.href = 'activate_plan.php?plan='+val;
		}
	}
</script>

</body>
</html>
