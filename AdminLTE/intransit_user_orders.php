<!DOCTYPE html>
<html>
<head>
	<title>Return Orders</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/user_orders.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php

  include 'conn.php';

	function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

  /*  $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";
*/
    $selected_orderid = array();
    $processing_orders_array = array();
    $readytoship_orders_array = array();
    $pickups_orders_array = array();
    $return_orders_array = array();
    $all_orders_array = array();


  /* Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    if(!Opencon()) {
      die("Connection Failed :".mysqli_connect_error());
    }
    else {*/
      $updateDetails = "SELECT * from customer_details WHERE Email = '".$Email."'";
      $result = Opencon() -> query($updateDetails);

      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $wallet = $row['Wallet'];
        }
      }
      $_SESSION['Wallet'] = $wallet;


      $order_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."'";
      $result = Opencon()->query($order_query);
      if($result->num_rows > 0) {
      	while ($row = $result->fetch_assoc()) {
      		$all_orders_array[] = $row;
      	}
      }


      #Paging for Processing Orders
      $processing_orders_number_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'PROCESSING'";
      $result = Opencon()->query($processing_orders_number_query);
      $processing_orders_number = mysqli_num_rows($result);


      #Paging for Ready To Ship Orders
      $readytoship_orders_number_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'READY TO SHIP'";
      $result = Opencon()->query($readytoship_orders_number_query);
      $readytoship_orders_number = mysqli_num_rows($result);

      #Paging for Pickup Orders
      $pickup_orders_number_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'PICKUP'";
      $result = Opencon()->query($pickup_orders_number_query);
      $pickup_orders_number = mysqli_num_rows($result);

      #Paging for In-Transit Orders
      if(isset($_GET['items_per_page'])) {
        $items_per_page = $_GET['items_per_page'];
      }
      else {
        $items_per_page = 25;
      }
      $return_page = "";
      if(isset($_GET['return_page'])) {
        $return_page = $_GET['return_page'];
      }
      else {
        $return_page1 = 0;
      }
      if ($return_page=="" || $return_page=="1") {
        $return_page1 = 0;
      }
      else {
        $return_page1 = ($return_page*$items_per_page)-$items_per_page;
      }

      $return_orders_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'IN_TRANSIT' ORDER BY timestamp DESC LIMIT $return_page1, $items_per_page";
      $result = Opencon()->query($return_orders_query);
      if($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
          $return_orders_array[] = $row;
        }
      }

      $return_orders_number_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'IN_TRANSIT'";
      $result = Opencon()->query($return_orders_number_query);
      $return_orders_number = mysqli_num_rows($result);

      #Paging for All Orders
      $all_orders_number_query = "SELECT * FROM customer_orders WHERE Email = '".$Email."'";
      $result = Opencon()->query($all_orders_number_query);
      $all_orders_number = mysqli_num_rows($result);
  //  }

    if(isset($_POST['chooseOperator'])) {
      $individual_order_id = $_POST['orderID'];
      $order_type = $_POST['order_type'];

      $sender_name = $_POST['fromName'];
      $sender_mobile = $_POST['fromMobile'];
      $sender_address_line1 = $_POST['fromAddressLine1'];
      $sender_address_line2 = $_POST['fromAddressLine2'];
      $sender_pincode = $_POST['fromCode'];

      $customer_name = $_POST['toName'];
      $customer_mobile = $_POST['toMobile'];
      $customer_address_line1 = $_POST['toAddressLine1'];
      $customer_address_line2 = $_POST['toAddressLine2'];
      $customer_pincode = $_POST['toCode'];

      $product_details = $_POST['product_details'];
      $length = $_POST['length'];
      $breadth = $_POST['breadth'];
      $height = $_POST['height'];
      $weight = $_POST['weight'];
      $isCOD = $_POST['COD'];

      $_SESSION['orderID'] = $individual_order_id;

      $_SESSION['fromName'] = $sender_name;
      $_SESSION['fromMobile'] = $sender_mobile;
      $_SESSION['fromAddressLine1'] = $sender_address_line1;
      $_SESSION['fromAddressLine2'] = $sender_address_line2;
      $_SESSION['fromCode'] = $sender_pincode;

      $_SESSION['toName'] = $customer_name;
      $_SESSION['toMobile'] = $customer_mobile;
      $_SESSION['toAddressLine1'] = $customer_address_line1;
      $_SESSION['toAddressLine2'] = $customer_address_line2;
      $_SESSION['toCode'] = $customer_pincode;

      $_SESSION['product_details'] = $product_details;
      $_SESSION['length'] = $length;
      $_SESSION['breadth'] = $breadth;
      $_SESSION['height'] = $height;
      $_SESSION['weight'] = $weight;
      $_SESSION['COD'] = $isCOD;

      if(strcmp($order_type, 'INDIVIDUAL - DOCUMENT')==0) {
        echo '<script language="javascript">';
        echo 'location.href="individual-document.php";';
        echo '</script>';
      }

      else if(strcmp($order_type, 'INDIVIDUAL - PARCEL')==0) {
        echo '<script language="javascript">';
        echo 'location.href="individual-parcel.php";';
        echo '</script>';
      }

      else if(strcmp($order_type, 'BUSINESS - DOCUMENT')==0) {
        echo '<script language="javascript">';
        echo 'location.href="business-document.php";';
        echo '</script>';
      }
      else if(strcmp($order_type, 'BUSINESS - PARCEL')==0) {
        echo '<script language="javascript">';
        echo 'location.href="business-parcel.php";';
        echo '</script>';
      }
    }

	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="Recharge" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="Plans" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="KYC">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; background-color: #fafafa; padding-left: 0px;">

    	<div class="individual_order_heading" style="width: 100%; min-width: 1000px; overflow-x: scroll; margin-bottom: 10px;">
    		<div style="width: 100%; float: left; background-color: rgba(0,0,0,0.1); padding-left: 2px;">

          <button class="order-button" id='myNav' style="padding: 12px;" onclick="location.href='addorder';"><i class="fa fa-plus"></i>&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAdd Order</button>

    			<button id="processing_orders" class="order-button" onclick="window.location.href='NEW'" style="width: 15%"><i class="fa fa-gears"></i>  New  <span><p style="display: inline-block; background-color: #ff902b; width: 15%; border-radius: 25px; color: white;"><?php echo $processing_orders_number; ?></p></span></button>
    			
    			<button id="readytoship_orders" class="order-button" onclick="window.location.href='READY_TO_SHIP'" style="width: 18%;"><i class="fa fa-truck"></i>  Ready to Ship  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;"><?php echo $readytoship_orders_number; ?></p></span></button>
    			
    			<button id="pickups_orders" class="order-button" onclick="window.location.href='PICKUP'" style="width: 13%;"><i class="fa fa-cart-arrow-down"></i>  Pickups  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;"><?php echo $pickup_orders_number; ?></p></span></button>

    			<button id="returns_orders" class="order-button" onclick="window.location.href='IN_TRANSIT'" style="width: 12%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #01a0e0 #b3b3b3 #b3b3b3; color: #285fdb; background-color: #fafafa; outline: 0;"><i class="fa fa-undo"></i>  In-Transit  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;"><?php echo $return_orders_number;?></p></span></button>

    			<button id="all_orders" class="order-button" onclick="window.location.href='ALL'" style="width: 14%;"><i class="fa fa-shopping-cart"></i>  All Orders  <span><p style="display: inline-block; background-color: #27c24c; width: 15%; border-radius: 25px; color: white;"><?php echo $all_orders_number; ?></p></span></button>
    		</div>
    		
    		<div id="return_orders_div" style="width: 100%; float: left; min-width: 1000px;">
          <div style="width: 100%; float: left; background-color: #fafafa; min-width: 1500px;">
            <div style="width: 8%; float: left; border: 0.5px solid #ddd;">
            <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>ORDER DATE</b></p>
            </div>
            <div style="width: 8%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>CHANNEL</b></p>
            </div>
            <div style="width: 9%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>ORDER ID</b></p>
            </div>
            <div style="width: 8%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>STATUS</b></p>
            </div>
            <div style="width: 10%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>OPERATOR</b></p>
            </div>
            <div style="width: 11%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>PICKUP DETAILS</b></p>
            </div>
            <div style="width: 15%; float: left; padding-left: 20px; border: 0.5px solid #ddd;">
              <p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>PICKUP REFERENCE NUMBER</b></p>
            </div>
            <div style="width: 6%; float: left; border: 0.5px solid #ddd;">
              <p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><b>ACTION</b></p>
            </div>
          </div>

          <?php

          		$return_orders_query_page = "SELECT * FROM customer_orders WHERE Email = '".$Email."' AND Status = 'RETURN'";
	            $result = Opencon()->query($return_orders_query_page);
	            if($result->num_rows > 0) {
	              while ($row = $result->fetch_assoc()) {
	                $return_orders_array_page[] = $row;
	              }
	            }
	            else {
	              $return_orders_array_page = array();
	            }

	            $number_of_return_orders = count($return_orders_array_page);
	            $number_of_pages = ceil($number_of_return_orders/$items_per_page);

            if(count($return_orders_array) == 0) {

              echo '<div style="width: 100%;">';
                echo '<img src="images/no_data.png" style="margin-top: 50px;"';
                echo '<br>';
                echo '<p style="color: #656565; font-size: 12px; width: 100%; margin-top: 10px; margin-left: 5px;">No Data Available</p>';
              echo '</div>';
            }
            else {
              foreach ($return_orders_array as $individual_return_order) {
              $order_date = $individual_return_order['order_date'];
              $channel = $individual_return_order['Channel'];
              $order_id = $individual_return_order['Order_ID'];
              $status = $individual_return_order['Status'];
              $operator = $individual_return_order['operator'];
              $product_details = $individual_return_order['Product_Details'];
              $sender_details = $individual_return_order['Sender_Details'];
              $customer_details = $individual_return_order['Customer_Details'];
              $dimensions = $individual_return_order['Dimensions'];
              $weight = $individual_return_order['Weight'];
              $price = $individual_return_order['Price'];

              if(strcmp($operator, 'FedEx')==0) {
                $image_name = 'Fedex.png';
              }

              if(strcmp($operator, 'DHL')==0) {
                $image_name = 'DHL.png';
              }

              if(strcmp($operator, 'DTDC')==0) {
                $image_name = 'DTDC.png';
              }

              if(strcmp($operator, 'Speed_Post')==0) {
                $image_name = 'Speed_Post.png';
              }


            echo'<div style="width: 100%; float: left; background-color: #fafafa;">';
            echo'<div style="width: 7.5%; float: left; padding-left: 10px;">';
              echo'<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;">'.$order_date.'</p>';
            echo'</div>';
            echo'<div style="width: 8%; float: left; padding-left: 15px;">';
              echo'<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;"><i class="fa fa-shopping-cart"></i>   '.$channel.'</p>';
            echo'</div>';
            echo'<div style="width: 9%; float: left; padding-left: 20px; padding-top: 8px;">';
              echo'<a href="#" style="font-size: 13px;">'.$order_id.'</a>';
            echo'</div>';
            echo'<div style="width: 8%; float: left; padding-left: 20px;">';
              echo'<p style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;">'.$status.'</p>';
            echo'</div>';
            echo'<div style="width: 10%; float: left; padding-left: 20px;">';
              echo'<img src="images/'.$image_name.'" style="margin-top: 10px; max-height: 24px; max-width: 93px;">';
            echo'</div>';
            echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;">'.$sender_details.'</p>';
            echo'</div>';
            echo'<div style="width: 11%; float: left; padding-left: 20px;">';
              echo'<p align="left" style=" font-size: 13px; color: rgb(69, 69, 69); margin-top: 10px;">Reference Number</p>';
            echo'</div>';
            echo'<form method="post" enctype="multipart/form-data">';
              echo'<div style="width: 6%; float: left; padding-left: 20px;">';
                echo'<button class="btn btn-info" style="background-color: #3c8dbc; margin-top: 40px; padding-top: 10px; padding-bottom: 0px;">Ship Now</p>';
              echo'</div>';
            echo'</form>';
            echo'<hr style="border-top: 2px solid #ddd; width: 100%;">';
            echo'</div>';

              }
            }

            ?>

            <div class="footer">

              <?php

              echo'<div style="width: 100%; float:left;">';
      
                echo'<div style="width: 40%; float: left; margin-top: 15px;">';
                  echo '<p style="color: #656565; float: left; padding-left: 20px; font-size: 13px;">Show&nbsp<b><span> <select id="selectBox" onchange="window.location.href = this.value">  
                    <option label="25" value="return_user_orders.php?items_per_page=25">25</option>
                    <option label="50" value="return_user_orders.php?items_per_page=50">50</option>
                    <option label="100" value="return_user_orders.php?items_per_page=100">100</option> 
                    </select> </span></b>&nbspitems per page</p>';
                echo'</div>';

                echo'<div style="width: 39%; float: left; margin-top: 15px;">';
                  if($return_page=="") {
                        $return_page = 1;
                      }
                  echo '<p style="color: #656565; float: left; font-size: 13px; ">Page Number : '.$return_page.'</p>';
                echo'</div>';

                echo'<div style="float: left; margin-top: 10px;">';
                  for($b=1;$b<=$number_of_pages;$b++) {
                      ?><a class="page_number_list" href="return_user_orders.php?return_page=<?php echo $b; ?>&items_per_page=<?php echo $items_per_page; ?>" style="text-decoration: none;"><?php echo $b." "; ?></a> <?php
                      }
                 echo'</div>';
              echo'</div>';

              ?>
            </div>

        </div>
    	</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/showOrders.js"></script>

<script type="text/javascript">
  window.onload = function() {
    var items_value = '<?php echo $items_per_page ?>';
    if(items_value == 25) {
      var index  = 0;
    }
    else if(items_value == 50) {
      var index  = 1;
    }
    else if(items_value == 100) {
      var index  = 2;
    }
    document.getElementById("selectBox").selectedIndex = index;
}
</script>

<script type="text/javascript">
  
  function CheckUncheckAll(){
  var all_order_id = new Array();
   var  selectAllCheckbox=document.getElementById("checkUncheckAll");
   if(selectAllCheckbox.checked==true){
    var checkboxes =  document.getElementsByName("rowSelectCheckBox");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = true;
      all_order_id[i] = checkboxes[i].value;
     }
     document.getElementById("hiddenF").value = all_order_id;
    }else {
     var checkboxes =  document.getElementsByName("rowSelectCheckBox");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = false;
     }
    }
   }

   function CheckUncheckAll_readytoship(){
  var all_order_id = new Array();
   var  selectAllCheckbox=document.getElementById("checkUncheckAll_readytoship");
   if(selectAllCheckbox.checked==true){
    var checkboxes =  document.getElementsByName("rowSelectCheckBox_readytoship");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = true;
      all_order_id[i] = checkboxes[i].value;
     }
     document.getElementById("hiddenF_readytoship").value = all_order_id;
    }else {
     var checkboxes =  document.getElementsByName("rowSelectCheckBox_readytoship");
     for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = false;
     }
    }
   }

   function CheckUncheckOne() {
    var all_order_id = new Array();
    checkboxes = document.getElementsByName("rowSelectCheckBox");
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if(checkboxes[i].checked == true) {
        all_order_id[i] = checkboxes[i].value;
      }
    }
    document.getElementById("hiddenF").value = all_order_id;
   }

   function CheckUncheckOne_readytoship() {
    var all_order_id = new Array();
    checkboxes = document.getElementsByName("rowSelectCheckBox_readytoship");
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if(checkboxes[i].checked == true) {
        all_order_id[i] = checkboxes[i].value;
      }
    }
    document.getElementById("hiddenF_readytoship").value = all_order_id;
   }
</script>
<script type="text/javascript">
  window.setInterval(function(){
  var p=0;
  var checkboxes = document.getElementsByName("rowSelectCheckBox");
  for(var i=0, n=checkboxes.length;i<n;i++) {
    if(checkboxes[i].checked==true) {
      p++;
    }
  }
  if(p > 0) {
    document.getElementById("ship_selected").style.cursor = "pointer";
    document.getElementById("cancelSelected").style.cursor = "pointer";
    document.getElementById("printOrInvoice").style.cursor = "pointer";
    document.getElementById("address").style.cursor = "pointer";
    document.getElementById("ship_selected").disabled = false;
    document.getElementById("cancelSelected").disabled = false;
    document.getElementById("printOrInvoice").disabled = false;
    document.getElementById("address").disabled = false;
    document.getElementById("ship_selected").style.backgroundColor = "#fafafa";
    document.getElementById("ship_selected").style.color = "#404040";
    document.getElementById("cancelSelected").style.backgroundColor = "#fafafa";
    document.getElementById("cancelSelected").style.color = "#404040";
    document.getElementById("printOrInvoice").style.backgroundColor = "#fafafa";
    document.getElementById("printOrInvoice").style.color = "#404040";
    document.getElementById("address").style.backgroundColor = "#fafafa";
    document.getElementById("address").style.color = "#404040";
  }
  else {
    document.getElementById("ship_selected").style.cursor = "not-allowed";
    document.getElementById("cancelSelected").style.cursor = "not-allowed";
    document.getElementById("printOrInvoice").style.cursor = "not-allowed";
    document.getElementById("address").style.cursor = "not-allowed";
    document.getElementById("ship_selected").disabled = true;
    document.getElementById("cancelSelected").disabled = true;
    document.getElementById("printOrInvoice").disabled = true;
    document.getElementById("address").disabled = true;
    document.getElementById("ship_selected").style.backgroundColor = "#eee";
    document.getElementById("ship_selected").style.color = "#ddd";
    document.getElementById("cancelSelected").style.backgroundColor = "#eee";
    document.getElementById("cancelSelected").style.color = "#ddd";
    document.getElementById("printOrInvoice").style.backgroundColor = "#eee";
    document.getElementById("printOrInvoice").style.color = "#ddd";
    document.getElementById("address").style.backgroundColor = "#eee";
    document.getElementById("address").style.color = "#ddd";
  }
}, 1);
</script>
<script type="text/javascript">
  window.setInterval(function(){
  var p=0;
  var checkboxes = document.getElementsByName("rowSelectCheckBox_readytoship");
  for(var i=0, n=checkboxes.length;i<n;i++) {
    if(checkboxes[i].checked==true) {
      p++;
    }
  }
  if(p > 0) {
    document.getElementById("pickup_selected").style.cursor = "pointer";
    document.getElementById("printOrInvoice_readytoship").style.cursor = "pointer";
    document.getElementById("pickup_selected").disabled = false;
    document.getElementById("printOrInvoice_readytoship").disabled = false;
    document.getElementById("pickup_selected").style.backgroundColor = "#fafafa";
    document.getElementById("pickup_selected").style.color = "#404040";
    document.getElementById("printOrInvoice_readytoship").style.backgroundColor = "#fafafa";
    document.getElementById("printOrInvoice_readytoship").style.color = "#404040";
  }
  else {
    document.getElementById("pickup_selected").style.cursor = "not-allowed";
    document.getElementById("printOrInvoice_readytoship").style.cursor = "not-allowed";
    document.getElementById("pickup_selected").disabled = true;
    document.getElementById("printOrInvoice_readytoship").disabled = true;
    document.getElementById("pickup_selected").style.backgroundColor = "#eee";
    document.getElementById("pickup_selected").style.color = "#ddd";
    document.getElementById("printOrInvoice_readytoship").style.backgroundColor = "#eee";
    document.getElementById("printOrInvoice_readytoship").style.color = "#ddd";
  }
}, 1);
</script>
</body>
</html>
