<!DOCTYPE html>
<html>
<head>
	<title>Logistics Company | Create Return Order</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
  include 'conn.php';

	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

    /*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    */

    $address_query = "SELECT * from customer_addresses WHERE Email = '".$Email."' ";
    $result = Opencon()->query($address_query);
    if($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $total_addresses[] = $row;
      }
    }

    $sku_query = "SELECT * from customer_product_sku WHERE Email = '".$Email."' ";
    $result = Opencon()->query($sku_query);
    if($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $total_SKU[] = $row;
      }
    }


    if($_POST) {
    	if(isset($_POST['verify-document'])) {
    		verifyDocument();
    	}
    	if(isset($_POST['verify-parcel'])) {
    		verifyParcel();
    	}
    	if(isset($_POST['business-verify-document'])) {
    		businessVerifyDocument();
    	}
    	if(isset($_POST['business-verify-parcel'])) {
    		businessVerifyParcel();
    	}
      if(isset($_POST['save_address'])) {
        saveAddress();
      }
    }

    function saveAddress() {
     /* $dbhost = "localhost";
      $dbuser = "root";
      $dbpass = "";
      $dbname = "logistics_v2";

      Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
      */

      $Email = $_SESSION['Email'];
      $alias = $_POST['address_alias'];
      $address_name = $_POST['address_name'];
      $address_mobile = $_POST['address_mobile'];
      $address_line1 = $_POST['addressLine1'];
      $address_line2 = $_POST['addressLine2'];
      $address_pincode = $_POST['address_pincode'];

      if(strlen($address_mobile) == 10 && strlen($address_pincode) == 6) {
        $result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$alias', '$address_name', '$address_mobile', '$address_line1', '$address_line2', '$address_pincode')");
        if($result) {
          echo '<script language="javascript">';
          echo 'alert("Address Saved!");';
          echo 'location.href="NEW";';
          echo '</script>';
        }
        else {
          echo '<script language="javascript">';
          echo 'alert("Some Error Occured");';
          echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
        echo 'alert("Invalid Details");';
        echo '</script>';
      }
    }

    function verifyDocument() {
      $price = 100;
      $Email = $_SESSION['Email'];
    	$fromName = $_POST['fromName'];
      $fromName = str_replace("'", "''", $fromName);
    	$fromMobile = $_POST['fromMobile'];
    	$fromAddressLine1 = $_POST['fromAddressLine1'];
      $fromAddressLine1 = str_replace("'", "''", $fromAddressLine1);
    	$fromAddressLine2 = $_POST['fromAddressLine2'];
      $fromAddressLine2 = str_replace("'", "''", $fromAddressLine2);
    	$fromCode = $_POST['fromCode'];

    	$toName = $_POST['toName'];
      $toName = str_replace("'", "''", $toName);
    	$toMobile = $_POST['toMobile'];
    	$toAddressLine1 = $_POST['toAddressLine1'];
      $toAddressLine1 = str_replace("'", "''", $toAddressLine1);
    	$toAddressLine2 = $_POST['toAddressLine2'];
      $toAddressLine2 = str_replace("'", "''", $toAddressLine2);
    	$toCode = $_POST['toCode'];

    	$document_type = $_POST['document-type'];
    	$document_weight = $_POST['document-weight'];
    	$individual_document_quantity = $_POST['individual-document-quantity'];
    	$isCOD = $_POST['document-cash-on-delivery'];
    	$saveAddress = $_POST['Save_Address'];
        $address_alias_name = $_POST['alias_for_address'];

    	if(strlen($fromMobile) == 10 && strlen($toMobile) == 10) {
    		if(strlen($fromCode) == 6 && strlen($toCode) == 6) {

          date_default_timezone_set("Asia/Calcutta");
          $date_today = date("d/M/Y, h:i:s A");
          $date_orderID = date("dmyhis");
          $date_created = date('Y-m-d');

        /*  $dbhost = "localhost";
          $dbuser = "root";
          $dbpass = "";
          $dbname = "logistics_v2";

          Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
          */

          $number_query = "SELECT * FROM customer_orders";
          $result = Opencon()->query($number_query);
          if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $total_orders[] = $row;
              }
              $total_orders_number = count($total_orders);
          }
          else {
            $total_orders_number = 0;
          }

          $result = Opencon()->query("INSERT INTO customer_orders(Email, Order_ID, order_date, Channel, Status, Order_Type, product_details ,Sender_Name, sender_mobile, sender_address_line_1, sender_address_line_2, sender_pincode, Customer_Name, customer_mobile, customer_address_line_1, customer_address_line_2, customer_pincode, length, breadth, height, Weight, COD, Price, new_invoice_status, date_created) VALUES ('$Email', '$total_orders_number$date_orderID', '$date_today', 'CUSTOM', 'RETURN', 'INDIVIDUAL - DOCUMENT', 'Name : $document_type<br> Quantity : $individual_document_quantity', '$fromName', '+91-$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode', '$toName', '+91-$toMobile', '$toAddressLine1', '$toAddressLine2', '$toCode', '1', '1', '1', '$document_weight', '$isCOD', '$price' , 'RETURN PENDING', '$date_created')");

          if($result) {
          	if(strcmp($saveAddress, 'Yes')==0) {
          		$result = Opencon()->query("SELECT * from customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          		if($result->num_rows > 0) {
          			$result = Opencon()->query("DELETE FROM customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$fromName', '$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode')");
          			echo '<script language="javascript">';
		            echo 'alert("Address Alias Overwritten!");';
		            echo '</script>';
          		}
          		else {
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$fromName', '$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode')");
          		}
          	}
            echo '<script language="javascript">';
            echo 'alert("Order Added Successfully");';
            echo 'location.href="NEW";';
            echo '</script>';
          }
          else{
            echo '<script language="javascript">';
            echo 'alert("Some Error Occured");';
            echo '</script>';
          }
    		}
    		else {
    			echo '<script language="javascript">';
	        	echo 'alert("Invalid PinCode!");';
	        	echo '</script>';
    		}
    	}
    	else {
    		echo '<script language="javascript">';
        	echo 'alert("Invalid Mobile Number!");';
        	echo '</script>';
    	}
    }

    function verifyParcel() {
      $Email = $_SESSION['Email'];
      $user_id = $_SESSION['User_ID'];
    	$fromName = $_POST['fromName'];
      $fromName = str_replace("'", "''", $fromName);
      $fromMobile = $_POST['fromMobile'];
      $fromAddressLine1 = $_POST['fromAddressLine1'];
      $fromAddressLine1 = str_replace("'", "''", $fromAddressLine1);
      $fromAddressLine2 = $_POST['fromAddressLine2'];
      $fromAddressLine2 = str_replace("'", "''", $fromAddressLine2);
      $fromCode = $_POST['fromCode'];

      $toName = $_POST['toName'];
      $toName = str_replace("'", "''", $toName);
      $toMobile = $_POST['toMobile'];
      $toAddressLine1 = $_POST['toAddressLine1'];
      $toAddressLine1 = str_replace("'", "''", $toAddressLine1);
      $toAddressLine2 = $_POST['toAddressLine2'];
      $toAddressLine2 = str_replace("'", "''", $toAddressLine2);
      $toCode = $_POST['toCode'];

      $product_name = $_POST['product_name'];
      $product_name = str_replace("'", "''", $product_name);
      $quantity = $_POST['quantity'];
      $weight = $_POST['weight'];
      $unit_price = $_POST['unit_price'];
      $length = $_POST['length'];
      $breadth = $_POST['breadth'];
      $height = $_POST['height'];
      $isCOD = $_POST['cash-on-delivery'];
      $saveAddress = $_POST['Save_Address'];
      $address_alias_name = $_POST['alias_for_address'];
      $address_alias_name = str_replace("'", "''", $address_alias_name);

      if(strlen($fromMobile) == 10 && strlen($toMobile) == 10) {
        if(strlen($fromCode) == 6 && strlen($toCode) == 6) {

          date_default_timezone_set("Asia/Calcutta");
          $order_date = date("d/M/Y, h:i:s A");
          $date_orderID = date("dmyhis");
          $date_created = date('Y-m-d');

       /*   $dbhost = "localhost";
          $dbuser = "root";
          $dbpass = "";
          $dbname = "logistics_v2";

          Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
          */

          $number_query = "SELECT * FROM customer_orders";
          $result = Opencon()->query($number_query);
          if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $total_orders[] = $row;
              }
              $total_orders_number = count($total_orders);
          }
          else {
            $total_orders_number = 0;
          }

          $result = Opencon()->query("INSERT INTO customer_orders(Email, Order_ID, order_date, Channel, Status, Order_Type, product_details, Sender_Name, sender_mobile, sender_address_line_1, sender_address_line_2, sender_pincode, Customer_Name, customer_mobile, customer_address_line_1, customer_address_line_2, customer_pincode, length, breadth, height, Weight, COD, Price, new_invoice_status, date_created) VALUES ('$Email', '$total_orders_number$date_orderID', '$order_date', 'CUSTOM', 'RETURN', 'INDIVIDUAL - PARCEL', 'Name : $product_name<br>  Quantity: $quantity', '$fromName', '+91-$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode', '$toName', '+91-$toMobile', '$toAddressLine1', '$toAddressLine2', '$toCode', '$length', '$breadth', '$height','$weight', '$isCOD', '$unit_price', 'RETURN PENDING', '$date_created')");

          if($result) {
          	if(strcmp($saveAddress, 'Yes')==0) {
          		$result = Opencon()->query("SELECT * from customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          		if($result->num_rows > 0) {
          			$result = Opencon()->query("DELETE FROM customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$fromName', '$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode')");
          			echo '<script language="javascript">';
		            echo 'alert("Address Alias Overwritten!");';
		            echo '</script>';
          		}
          		else {
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$fromName', '$fromMobile', '$fromAddressLine1', '$fromAddressLine2', '$fromCode')");
          		}
          	}
            echo '<script language="javascript">';
            echo 'alert("Order Added Succesfully!");';
            echo 'location.href="NEW";';
            echo '</script>';
          }
          else{
            echo '<script language="javascript">';
            echo 'alert("Some Error Occured");';
            echo '</script>';
          }

        }
        else {
          echo '<script language="javascript">';
            echo 'alert("Invalid PinCode!");';
            echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
          echo 'alert("Invalid Mobile Number!");';
          echo '</script>';
      }
    }

    function businessVerifyDocument() {
      $price = 100;
      $Email = $_SESSION['Email'];
      $user_id = $_SESSION['User_ID'];
      $business_fromName = $_POST['business-fromName'];
      $business_fromName = str_replace("'", "''", $business_fromName);
      $business_fromMobile = $_POST['business-fromMobile'];
      $business_fromAddressLine1 = $_POST['business-fromAddressLine1'];
      $business_fromAddressLine1 = str_replace("'", "''", $business_fromAddressLine1);
      $business_fromAddressLine2 = $_POST['business-fromAddressLine2'];
      $business_fromAddressLine2 = str_replace("'", "''", $business_fromAddressLine2);
      $business_fromCode = $_POST['business-fromCode'];

      $business_toName = $_POST['business-toName'];
      $business_toName = str_replace("'", "''", $business_toName);
      $business_toMobile = $_POST['business-toMobile'];
      $business_toAddressLine1 = $_POST['business-toAddressLine1'];
      $business_toAddressLine1 = str_replace("'", "''", $business_toAddressLine1);
      $business_toAddressLine2 = $_POST['business-toAddressLine2'];
      $business_toAddressLine2 = str_replace("'", "''", $business_toAddressLine2);
      $business_toCode = $_POST['business-toCode'];

      $business_document_type = $_POST['business-document-type'];
      $business_document_weight = $_POST['business-document-weight'];
      $business_document_quantity = $_POST['business-document-quantity'];
      $business_isCOD = $_POST['business-document-cash-on-delivery'];
      $saveAddress = $_POST['Save_Address'];
      $address_alias_name = $_POST['alias_for_address'];
      $address_alias_name = str_replace("'", "''", $address_alias_name);

      if(strlen($business_fromMobile) == 10 && strlen($business_toMobile) == 10) {
        if(strlen($business_fromCode) == 6 && strlen($business_toCode) == 6) {

          date_default_timezone_set("Asia/Calcutta");
          $date_today = date("d/M/Y, h:i:s A");
          $date_orderID = date("dmyhis");
          $date_created = date('Y-m-d');

         /* $dbhost = "localhost";
          $dbuser = "root";
          $dbpass = "";
          $dbname = "logistics_v2";

          Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
          */

          $number_query = "SELECT * FROM customer_orders";
          $result = Opencon()->query($number_query);
          if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $total_orders[] = $row;
              }
              $total_orders_number = count($total_orders);
          }
          else {
            $total_orders_number = 0;
          }

          $result = Opencon()->query("INSERT INTO customer_orders(Email, Order_ID, order_date, Channel, Status, Order_Type, product_details, Sender_Name, sender_mobile, sender_address_line_1, sender_address_line_2, sender_pincode, Customer_Name, customer_mobile, customer_address_line_1, customer_address_line_2, customer_pincode, length, breadth, height, Weight, COD, Price, new_invoice_status, date_created) VALUES ('$Email', '$total_orders_number$date_orderID', '$date_today', 'CUSTOM', 'RETURN', 'BUSINESS - DOCUMENT', 'Name : $business_document_type<br> Quantity : $business_document_quantity', '$business_fromName', '+91-$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode', '$business_toName', '+91-$business_toMobile', '$business_toAddressLine1', '$business_toAddressLine2', '$business_toCode', '1', '2', '3', '$business_document_weight', '$business_isCOD', '$price', 'RETURN PENDING', '$date_created')");

          if($result) {
          	if(strcmp($saveAddress, 'Yes')==0) {
          		$result = Opencon()->query("SELECT * from customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          		if($result->num_rows > 0) {
          			$result = Opencon()->query("DELETE FROM customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$business_fromName', '$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode')");
          			echo '<script language="javascript">';
		            echo 'alert("Address Alias Overwritten!");';
		            echo '</script>';
          		}
          		else {
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$business_fromName', '$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode')");
          		}
          	}
            echo '<script language="javascript">';
            echo 'alert("Order Added Succesfully!");';
            echo 'location.href="NEW";';
            echo '</script>';
          }
          else{
            echo("Error description: " . mysqli_error(Opencon()));
            echo '<script language="javascript">';
            echo 'alert("Some Error Occured");';
            echo '</script>';
          }
        }
        else {
          echo '<script language="javascript">';
            echo 'alert("Invalid PinCode!");';
            echo '</script>';
        }
      }
      else {
        echo '<script language="javascript">';
          echo 'alert("Invalid Mobile Number!");';
          echo '</script>';
      }    }

    function businessVerifyParcel() {
      $Email = $_SESSION['Email'];
      $user_id = $_SESSION['User_ID'];
      $business_fromName = $_POST['business-fromName'];
      $business_fromName = str_replace("'", "''", $business_fromName);
      $business_fromMobile = $_POST['business-fromMobile'];
      $business_fromAddressLine1 = $_POST['business-fromAddressLine1'];
      $business_fromAddressLine1 = str_replace("'", "''", $business_fromAddressLine1);
      $business_fromAddressLine2 = $_POST['business-fromAddressLine2'];
      $business_fromAddressLine2 = str_replace("'", "''", $business_fromAddressLine2);
      $business_fromCode = $_POST['business-fromCode'];

      $business_toName = $_POST['business-toName'];
      $business_toName = str_replace("'", "''", $business_toName);
      $business_toMobile = $_POST['business-toMobile'];
      $business_toAddressLine1 = $_POST['business-toAddressLine1'];
      $business_toAddressLine1 = str_replace("'", "''", $business_toAddressLine1);
      $business_toAddressLine2 = $_POST['business-toAddressLine2'];
      $business_toAddressLine2 = str_replace("'", "''", $business_toAddressLine2);
      $business_toCode = $_POST['business-toCode'];

      $business_product_name = $_POST['business_product_name'];
      $business_product_name = str_replace("'", "''", $business_product_name);
      $business_product_sku = $_POST['business_product_sku'];
      $business_product_sku = str_replace("'", "''", $business_product_sku);
      $business_product_hsn = $_POST['business_product_hsn'];
      $business_product_hsn = str_replace("'", "''", $business_product_hsn);
      $business_quantity = $_POST['business_quantity'];
      $business_tax = $_POST['business_tax'];
      $business_unit_price = $_POST['business_unit_price'];
      $business_discount = $_POST['business_discount'];
      $business_weight = $_POST['business_weight'];
      $business_length = $_POST['business_length'];
      $business_breadth = $_POST['business_breadth'];
      $business_height = $_POST['business_height'];
      $isCOD = $_POST['business-cash-on-delivery'];
      $saveSKU = $_POST['Save_SKU'];
      $saveAddress = $_POST['Save_Address'];
      $address_alias_name = $_POST['alias_for_address'];
      $address_alias_name = str_replace("'", "''", $address_alias_name);

      if(strlen($business_fromMobile) == 10 && strlen($business_toMobile) == 10) {
        if(strlen($business_fromCode) == 6 && strlen($business_toCode) == 6) {

          date_default_timezone_set("Asia/Calcutta");
          $date_today = date("d/M/Y, h:i:s A");
          $date_orderID = date("dmyhis");
          $date_created = date('Y-m-d');

       /*   $dbhost = "localhost";
          $dbuser = "root";
          $dbpass = "";
          $dbname = "logistics_v2";

          Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
          */

          $number_query = "SELECT * FROM customer_orders";
          $result = Opencon()->query($number_query);
          if($result->num_rows > 0) {
              while ($row = $result->fetch_assoc()) {
                $total_orders[] = $row;
              }
              $total_orders_number = count($total_orders);
          }
          else {
            $total_orders_number = 0;
          }

          $result = Opencon()->query("INSERT INTO customer_orders(Email, Order_ID, order_date, Channel, Status, Order_Type, product_details, SKU, Sender_Name, sender_mobile, sender_address_line_1, sender_address_line_2, sender_pincode, Customer_Name, customer_mobile, customer_address_line_1, customer_address_line_2, customer_pincode, length, breadth, height, Weight, COD, Price, new_invoice_status, date_created) VALUES ('$Email', '$total_orders_number$date_orderID', '$date_today', 'CUSTOM', 'RETURN', 'BUSINESS - PARCEL', 'Name : $business_product_name<br> SKU : $business_product_sku<br> HSN : $business_product_hsn<br> Quantity : $business_quantity<br> Tax : $business_tax<br> Discount : $business_discount<br>', '$business_product_sku', '$business_fromName', '+91-$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode', '$business_toName', '+91-$business_toMobile', '$business_toAddressLine1', '$business_toAddressLine2', '$business_toCode', '$business_length', '$business_breadth', '$business_height','$business_weight', '$isCOD', '$business_unit_price', 'RETURN PENDING', '$date_created')");

          if($result) {
          	if(strcmp($saveSKU, 'Yes')==0) {
          		$result = Opencon()->query("INSERT INTO customer_product_sku(Email, SKU_name, SKU_weight, SKU_length, SKU_breadth, SKU_height) VALUES ('$Email', '$business_product_sku', '$business_weight', '$business_length', '$business_breadth', '$business_height')");
          	}
          	if(strcmp($saveAddress, 'Yes')==0) {
          		$result = Opencon()->query("SELECT * from customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          		if($result->num_rows > 0) {
          			$result = Opencon()->query("DELETE FROM customer_addresses WHERE Email = '".$Email."' AND Address_Alias = '".$address_alias_name."'");
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$business_fromName', '$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode')");
          			echo '<script language="javascript">';
		            echo 'alert("Address Alias Overwritten!");';
		            echo '</script>';
          		}
          		else {
          			$result = Opencon()->query("INSERT INTO customer_addresses(Email, Address_Alias, Address_Name, Address_Mobile, Address_Line_1, Address_Line_2, Address_Pincode) VALUES ('$Email', '$address_alias_name', '$business_fromName', '$business_fromMobile', '$business_fromAddressLine1', '$business_fromAddressLine2', '$business_fromCode')");
          		}
          	}

            echo '<script language="javascript">';
            echo 'alert("Order Added Succesfully!");';
            echo 'location.href="NEW";';
            echo '</script>';
          }
          else{
            echo("Error description: " . mysqli_error(Opencon()));
            echo '<script language="javascript">';
            echo 'alert("Some Error Occured");';
            echo '</script>';
          }
        }
        else {
          echo '<script language="javascript">';
          echo 'alert("Invalid PinCode!");';
          echo '</script>';
        }
    }
    else {
    	echo '<script language="javascript">';
        echo 'alert("Invalid Mobile Number!");';
        echo '</script>';
    }
  }

	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="Dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="Recharge" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="Plans" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="Dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a href="pages/widgets.html">
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="KYC">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="Support"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center;" id="id_dashboard">
    	<div class="create-order-heading">
    		<div style="padding: 20px;">
    			<button style="color: #285fdb; padding: 10px; background-color: white; border: 2px solid #285fdb; display: inline-block; float: left;" name="individual" id="individual" style="" onclick="displayIndividualForm();"><b>Individual</b></button>
    			<button class="order-type" name="business" id="business" style="color: #285fdb; padding: 10px; background-color: white; border: 2px solid #285fdb; display: inline-block; float: left; margin-left: 20px;" onclick="displayBusinessForm();"><b>Business</b></button><br><br>
    			<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
    			<div id="individual_form" style="display: inline-block; float: left; width: 100%; display: none;">
    				<form method="post" enctype="multipart/form-data">
    					<p align="left" style="color: #656565; font-size: 24px;">Shipment Details</p>
    					<p align="left" style="color: #656565; font-size: 14px; margin-top: -16px;">Enter your Pickup & Delivery Address</p>
    					<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 3px;">

              <input style="float: left;" id="saved_address" onclick="set_address(this.id);" type="radio" name="address_choice" value="Saved" checked><span><p style="float: left;">&nbspSaved Address</p></span>
              <input style="float: left; margin-left: 20px;" id="new_address" onclick="set_address(this.id);" type="radio" name="address_choice" value="New"><span><p style="float: left;">&nbspNew Address</p></span><br><br>
              <select id="address_select" onchange="myUpdateFunction(event)" style="float: left; width: 20%; border-radius: 3px;"><option value="None">Select Pickup Address</option><?php    
              foreach ($total_addresses as $address) {
                $alias = $address['Address_Alias'];
                echo '<option value="'.$alias.'">'.$alias.'</option>';
              }
              foreach ($total_addresses as $address) {
                $alias = $address['Address_Alias'];
                echo '<input type="hidden" id="'.$alias.'_name" value="'.$address['Address_Name'].'">';
                echo '<input type="hidden" id="'.$alias.'_mobile" value="'.$address['Address_Mobile'].'">';
                echo '<input type="hidden" id="'.$alias.'_address_line_1" value="'.$address['Address_Line_1'].'">';
                echo '<input type="hidden" id="'.$alias.'_address_line_2" value="'.$address['Address_Line_2'].'">';
                echo '<input type="hidden" id="'.$alias.'_pincode" value="'.$address['Address_Pincode'].'">';
              }

              ?></select><br><br>

    					<div style="display: inline-block; float: left; width: 50%;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Pickup From*</b></p>

    						<input type="text" name="fromName" id="fromName" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 50%; color: #656565; height: 30px;" placeholder="Full Name*">

    						<input type="number" name="fromMobile" id="fromMobile" style="display: inline-block; margin-bottom: 10px; margin-left: 6px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 39%; color: #656565; height: 30px;" placeholder="Mobile Number*">

    						<input type="text" name="fromAddressLine1" id="fromAddressLine1" style="border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 1*">

    						<input type="text" name="fromAddressLine2" id="fromAddressLine2" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 2">

    						<input type="number" name="fromCode" id="fromCode" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 45%; color: #656565; height: 30px;" placeholder="Pin Code*">
    					</div>
    					<div style="display: inline-block; float: right; width: 50%;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Deliver To*</b></p>

    						<input type="text" name="toName" id="toName" style="margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 50%; color: #656565; height: 30px;" placeholder="Full Name*">

    						<input type="number" name="toMobile" id="toMobile" style="display: inline-block; margin-bottom: 10px; margin-left: 6px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 39%; color: #656565; height: 30px;" placeholder="Mobile Number*">

    						<input type="text" name="toAddressLine1" id="toAddressLine1" style="border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 1*">

    						<input type="text" name="toAddressLine2" id="toAddressLine2" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 2">

    						<input type="number" name="toCode" id="toCode" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 45%; color: #656565; height: 30px;" placeholder="Pin Code*">
    					</div>

    					<div id="save_address_div" style="float: left; width: 100%; display: inline-block; margin-top: 20px;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Would you like to save this Address?*</b></p>
    						<input style="float: left;" id="show_address_alias" onclick="showDiv(this.id)" type="radio" name="Save_Address" value="Yes"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    						<input style="float: left;"  id="hide_address_alias" onclick="showDiv(this.id)" type="radio" name="Save_Address" value="No" checked><p style="float: left;">&nbspNo</p>
    					</div>
    					<div id="show_address_alias_div" style="float: left; width: 100%; margin-bottom: 6px;">
    						<input style="border-radius: 25px; float: left; border: 0.5px solid rgba(0,0,0,0.15); width: 23.5%; color: #656565; height: 30px; padding-left: 10px;" type="text" name="alias_for_address" id="1alias_for_address" placeholder="Alias*">
    						<a class="alias_info" href="#" title="Create an Alias for your address. Duplicate alias will result in overwriting the previous address." style="float: left; margin-left: 5px;"><i class="fa fa-info"></i></a><br><br>
    					</div>

    					<p> </p>
    					<p align="left" style="color: #656565; font-size: 24px; margin-top: 230px;">Package Details</p>
    					<hr style="border-top: 1px solid rgba(0,0,0,0.15); margin-top: 3px;">
    					<div style="display: inline-block; float: left; width: 100%;">
    						<input style="float: left;" type="radio" name="package-type" value="document" id="document" checked><p align="left">&nbspDocument</p>
    						<input style="float: left;" type="radio" name="package-type" value="parcel" id="parcel"><p align="left">&nbspParcel</p>
    						<hr style="border-top: 1px solid rgba(0,0,0,0.15); margin-top: 3px;">
    					</div>
    					<div id="show-document" style="display: inline-block; float: left; width: 51%;">
    						<input style="float: left; display: inline-block;" type="radio" name="document-type" value="A4 Document" id="a4" checked><p align="left">&nbspA4 Document</p>
    						<input style="float: left; display: inline-block;" type="radio" name="document-type" value="A3 Document" id="a3"><p align="left">&nbspA3 Document</p>
    						<br>
    						<p style="color: #656565; float: left;"><b>Weight: </b><span>&nbsp&nbsp&nbsp<select name="document-weight" style="height: 30px;"><option value="Less than 0.5kg">Less than 0.5 kg</option><option value="More than 0.5kg">More than 0.5 kg</option></select><span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>Quantity: </b>&nbsp&nbsp&nbsp<input style="height: 30px;" type="number" name="individual-document-quantity" min="1" step="1" id="individual-document-quantity" value="1"></span></span></p><br><br><br>
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Is this a cash on delivery order?*</b></p>
    						<input type="radio" name="document-cash-on-delivery" value="Yes" id="yes" style="float: left;"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    						<input type="radio" name="document-cash-on-delivery" value="No" id="no" style="float: left;"  checked><p style="float: left">&nbspNo</p><br><br>
    						<button style="border-radius: 12px; color: white; background-color: #285fdb; float: left; padding: 10px; border: none;" class="submit-document" name="verify-document">Create Return</button>
    					</div>
    					<div id="show-parcel" style="display: inline-block; width: 100%; display: none;">
    						<div style="display: inline-block; float: left; width: 30%">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Product Name*</b></p>

    							<input type="text" name="product_name" id="product_name" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Quantity*</b></p>
    							<input type="text" name="quantity" id="quantity" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;" value="1">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Unit Price*</b></p>
    							<input type="text" name="unit_price" id="price" min="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Weight (in Kg)*.</b></p>
    							<input type="number" name="weight" id="weight" min="0.1" step="0.1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 70%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>&nbsp</b></p>
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Dimensions (in cms)*</b></p>
    							<input type="number" name="length" id="length" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="L">
    							<span><p style="display: inline-block; float: left; width: 1%; margin: 5px;">X</p></span>
    							<input type="number" name="breadth" id="breadth" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="B">
    							<span><p style="display: inline-block; float: left; width: 1%; margin: 5px;">X</p></span>
    							<input type="number" name="height" id="height" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="H">
    						</div>
    						<div style="display: inline-block; float: left; width: 100%;">
    							<br>
    							<p align="left" style="color: #656565; font-size: 12px;"><b>Is this a cash on delivery order?*</b></p>
    							<input type="radio" name="cash-on-delivery" value="Yes" id="yes" style="float: left;"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    							<input type="radio" name="cash-on-delivery" value="No" id="no" style="float: left;" checked><p style="float: left">&nbspNo</p><br><br>
    							<button style="border-radius: 12px; color: white; background-color: #285fdb; float: left; padding: 10px; border: none;" class="submit-document" name="verify-parcel">Create Return</button>
    						</div>
    					</div>
    				</form>
    			</div>
    			<div id="business-form" style="display: inline-block; float: left; width: 100%; display: none;">
    				<form method="post" enctype="multipart/form-data">
    					<p align="left" style="color: #656565; font-size: 24px;">Shipment Details</p>
    					<p align="left" style="color: #656565; font-size: 14px; margin-top: -16px;">Enter your Pickup & Delivery Address</p>
    					<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 3px;">

              <input style="float: left;" id="saved_address" onclick="set_address(this.id);" type="radio" name="address_choice" value="Saved" checked><span><p style="float: left;">&nbspSaved Address</p></span>
              <input style="float: left; margin-left: 20px;" id="new_address" onclick="set_address(this.id);" type="radio" name="address_choice" value="New"><span><p style="float: left;">&nbspNew Address</p></span><br><br>
              <select id="business-address_select" onchange="myUpdateFunction(event)" style="float: left; width: 20%; border-radius: 3px;"><option value="None">Select Pickup Address</option><?php    
              foreach ($total_addresses as $address) {
                $alias = $address['Address_Alias'];
                echo '<option value="'.$alias.'">'.$alias.'</option>';
              }
              foreach ($total_addresses as $address) {
                $alias = $address['Address_Alias'];
                echo '<input type="hidden" id="'.$alias.'_name" value="'.$address['Address_Name'].'">';
                echo '<input type="hidden" id="'.$alias.'_mobile" value="'.$address['Address_Mobile'].'">';
                echo '<input type="hidden" id="'.$alias.'_address_line_1" value="'.$address['Address_Line_1'].'">';
                echo '<input type="hidden" id="'.$alias.'_address_line_2" value="'.$address['Address_Line_2'].'">';
                echo '<input type="hidden" id="'.$alias.'_pincode" value="'.$address['Address_Pincode'].'">';
              }

              ?></select><br><br>


    					<div style="display: inline-block; float: left; width: 50%;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Pickup From*</b></p>

    						<input type="text" name="business-fromName" id="business-fromName" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 50%; color: #656565; height: 30px;" placeholder="Full Name*">

    						<input type="number" name="business-fromMobile" id="business-fromMobile" style="display: inline-block; margin-bottom: 10px; margin-left: 6px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 39%; color: #656565; height: 30px;" placeholder="Mobile Number*">

    						<input type="text" name="business-fromAddressLine1" id="business-fromAddressLine1" style="border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 1*">

    						<input type="text" name="business-fromAddressLine2" id="business-fromAddressLine2" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 2">

    						<input type="number" name="business-fromCode" id="business-fromCode" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 45%; color: #656565; height: 30px;" placeholder="Pin Code*">
    					</div>
    					<div style="display: inline-block; float: right; width: 50%;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Deliver To*</b></p>

    						<input type="text" name="business-toName" id="business-toName" style="margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 50%; color: #656565; height: 30px;" placeholder="Full Name*">

    						<input type="number" name="business-toMobile" id="business-toMobile" style="display: inline-block; margin-bottom: 10px; margin-left: 6px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 39%; color: #656565; height: 30px;" placeholder="Mobile Number*">

    						<input type="text" name="business-toAddressLine1" id="business-toAddressLine1" style="border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 1*">

    						<input type="text" name="business-toAddressLine2" id="business-toAddressLine2" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 90%; color: #656565; height: 30px;" placeholder="Address Line 2">

    						<input type="number" name="business-toCode" id="business-toCode" style="margin-top: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 45%; color: #656565; height: 30px;" placeholder="Pin Code*">
    					</div>

    					<div id="business-save_address_div" style="float: left; width: 100%; display: inline-block; margin-top: 20px;">
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Would you like to save this Address?*</b></p>
    						<input style="float: left;" id="show_address_alias" onclick="businessShowDiv(this.id)" type="radio" name="Save_Address" value="Yes"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    						<input style="float: left;"  id="business-hide_address_alias" onclick="businessShowDiv(this.id)" type="radio" name="Save_Address" value="No" checked><p style="float: left;">&nbspNo</p>
    					</div>
    					<div id="business-show_address_alias_div" style="float: left; width: 100%; margin-bottom: 6px;">
    						<input style="border-radius: 25px; float: left; border: 0.5px solid rgba(0,0,0,0.15); width: 23.5%; color: #656565; height: 30px; padding-left: 10px;" type="text" name="alias_for_address" id="alias_for_address" placeholder="Alias*">
    						<a class="alias_info" href="#" title="Create an Alias for your address. Duplicate alias will result in overwriting the previous address." style="float: left; margin-left: 5px;"><i class="fa fa-info"></i></a><br><br>
    					</div>

    					<p> </p>
    					<p align="left" style="color: #656565; font-size: 24px; margin-top: 230px;">Package Details</p>
    					<hr style="border-top: 1px solid rgba(0,0,0,0.15); margin-top: 3px;">
    					<div style="display: inline-block; float: left; width: 100%;">
    						<input style="float: left;" type="radio" name="business-package-type" value="document" id="business-document" checked><p align="left">&nbspDocument</p>
    						<input style="float: left;" type="radio" name="business-package-type" value="parcel" id="business-parcel"><p align="left">&nbspParcel</p>
    						<hr style="border-top: 1px solid rgba(0,0,0,0.15); margin-top: 3px;">
    					</div>
    					<div id="business-show-document" style="display: inline-block; float: left; width: 51%;">
    						<input style="float: left; display: inline-block;" type="radio" name="business-document-type" value="A4 Document" id="business-a4" checked><p align="left">&nbspA4 Document</p>
    						<input style="float: left; display: inline-block;" type="radio" name="business-document-type" value="A3 Document" id="business-a3"><p align="left">&nbspA3 Document</p>
    						<br>
    						<p style="color: #656565; float: left;"><b>Weight: </b><span>&nbsp&nbsp&nbsp<select name="business-document-weight" style="height: 30px;"><option value="Less than 0.5kg">Less than 0.5 kg</option><option value="More than 0.5kg">More than 0.5 kg</option></select><span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>Quantity:</b> &nbsp&nbsp&nbsp<input style="height: 30px;" type="number" name="business-document-quantity" min="1" step="1" id="business-document-quantity" value="1"></span></span></p><br><br><br>
    						<p align="left" style="color: #656565; font-size: 12px;"><b>Is this a cash on delivery order?*</b></p>
    						<input type="radio" name="business-document-cash-on-delivery" value="Yes" id="yes" style="float: left;"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    						<input type="radio" name="business-document-cash-on-delivery" value="No" id="no" style="float: left;" checked><p style="float: left">&nbspNo</p><br><br>
    						<button style="border-radius: 12px; color: white; background-color: #285fdb; float: left; padding: 10px; border: none;" class="submit-document" name="business-verify-document">Create Return</button>
    					</div>
    					<div id="business-show-parcel" style="display: inline-block; width: 100%; display: none;">
    						<div style="display: inline-block; float: left; width: 100%">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Product Name*</b></p>

    							<input type="text" name="business_product_name" id="business-product_name" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 40%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 71%; margin-top: 10px;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>SKU</b></p>

    							<input style="float: left;" id="new_sku" onclick="set_sku(this.id);" type="radio" name="sku_choice" value="New_SKU" checked><span><p style="float: left;">&nbspNew SKU</p></span>
    							<input style="float: left;  margin-left: 20px;" id="saved_sku" onclick="set_sku(this.id);" type="radio" name="sku_choice" value="Saved_SKU"><span><p style="float: left;">&nbspSaved SKU</p></span><br><br><br>

    							<div id="sku_select_div">
    								<select id="sku_select" onchange="updateSKU(event)" style="float: left; width: 20%; border-radius: 3px;"><option value="None">Select SKU</option><?php
	    							foreach ($total_SKU as $SKU) {
	    								$SKU_name = $SKU['SKU_name'];
	    								echo '<option value="'.$SKU_name.'">'.$SKU_name.'</option>';
	    							}
	    							foreach ($total_SKU as $SKU) {
						                $SKU_name = $SKU['SKU_name'];
						                echo '<input type="hidden" id="'.$SKU_name.'_SKU_name" value="'.$SKU['SKU_name'].'">';
						                echo '<input type="hidden" id="'.$SKU_name.'_SKU_weight" value="'.$SKU['SKU_weight'].'">';
						                echo '<input type="hidden" id="'.$SKU_name.'_SKU_length" value="'.$SKU['SKU_length'].'">';
						                echo '<input type="hidden" id="'.$SKU_name.'_SKU_breadth" value="'.$SKU['SKU_breadth'].'">';
						                echo '<input type="hidden" id="'.$SKU_name.'_SKU_height" value="'.$SKU['SKU_height'].'">';
						              }
	    							?></select><br><br>
    							</div>

    							<input type="text" name="business_product_sku" id="product_sku" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 45%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%; margin-top: 28px;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>HSN</b></p>

    							<input type="text" name="business_product_hsn" id="product_hsn" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 41%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Quantity*</b></p>
    							<input type="text" name="business_quantity" id="business-quantity" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;" value="1">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Tax (%)</b></p>
    							<input type="text" name="business_tax" id="business-tax" min="0.1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Unit Price*</b></p>
    							<input type="text" name="business_unit_price" id="business-price" min="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Discount (₹)</b></p>
    							<input type="text" name="business_discount" id="business-discount" min="0" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 30%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%; margin-top: 28px;"><b>Weight (in Kg)*.</b></p>
    							<input type="number" name="business_weight" id="business-weight" min="0.1" step="0.1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 65%; color: #656565; height: 30px;">
    						</div>
    						<div style="display: inline-block; float: left; width: 70%;">
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>&nbsp</b></p>
    							<p align="left" style="color: #656565; display: inline-block; width: 100%;"><b>Dimensions (in cms)*</b></p>
    							<input type="number" name="business_length" id="business-length" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="L">
    							<span><p style="display: inline-block; float: left; width: 1%; margin: 5px;">X</p></span>
    							<input type="number" name="business_breadth" id="business-breadth" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="B">
    							<span><p style="display: inline-block; float: left; width: 1%; margin: 5px;">X</p></span>
    							<input type="number" name="business_height" id="business-height" min="1" step="1" style="display: inline-block; margin-bottom: 10px; border-radius: 25px; float: left; padding-left: 10px; border: 0.5px solid rgba(0,0,0,0.15); width: 20%; color: #656565; height: 30px;" placeholder="H">
    						</div>
    						<div style="display: inline-block; float: left; width: 100%;">
    							<br>
    							<div style="display: inline-block; float: left; width: 30%;">
    								<p align="left" style="color: #656565; font-size: 12px;"><b>Is this a cash on delivery order?*</b></p>
    								<input type="radio" name="business-cash-on-delivery" value="Yes" id="yes" style="float: left;"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    								<input type="radio" name="business-cash-on-delivery" value="No" id="no" style="float: left;"  checked><p style="float: left">&nbspNo</p>
    							</div>

    							<div id="save_sku_div" style="float: left; width: 65%; display: inline-block;">
    								<p align="left" style="color: #656565; font-size: 12px;"><b>Would you like to save this SKU?*</b></p>
    								<input style="float: left;" type="radio" name="Save_SKU" value="Yes"><p style="float: left; margin-right: 20px;">&nbspYes</p>
    								<input style="float: left;" id="Save_SKU" type="radio" name="Save_SKU" value="No" checked><p style="float: left;">&nbspNo</p>
    							</div>
    							<div style="width: 100%; float: left;">
    								<button style="border-radius: 12px; margin-top: 10px; color: white; background-color: #285fdb; float: left; padding: 10px; border: none;" class="submit-document" name="business-verify-parcel">Create Return</button>
    							</div>
    						</div>
    					</div>
    				</form>
    			</div>
    		</div>
        
        <div id="popup1" class="overlay">
          <div class="popup">
            <a class="close" href="#">&times;</a>
            <div class="content">
              <form method="post" enctype="multipart/form-data">
                <div>
                  <p style="float: left; font-size: 24px; color: #656565;">Fill your Address Details</p><br><br><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -28px;">Alias should be unique*.</p>
                  <hr style="border-top: 1px solid #eee; margin-top: 2px;"><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -28px; margin-left: 10px;">Alias*</p>
                  <input type="text" name="address_alias" style="border-radius: 25px; float: left; margin-top: -5px; padding-left: 10px; border: 2px solid #609; width: 61%; margin-left: -42px; " required>
                </div><br><br>
                <div>
                  <p style="float: left; font-size: 24px; color: #656565;">Personal Details</p><br><br><br>
                  <hr style="border-top: 1px solid #eee; margin-top: -16px;"><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -28px; margin-left: 5px;">Contact Name*</p>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -28px; margin-left: 132px;">Mobile Number*</p><br>
                  <input type="text" name="address_name" style="border-radius: 25px; float: left; margin-top: -19px; padding-left: 10px; border: 2px solid #609; width: 40%;" required>
                  <input type="number" name="address_mobile" onkeypress="javascript:return isNumber(event)" style="border-radius: 25px; float: left; margin-top: -19px; padding-left: 10px; border: 2px solid #609; width: 41%; margin-left: 28px;" required><br><br><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -28px; margin-left: 5px;">Address Line 1*</p><br>
                  <input type="text" name="addressLine1" style="border-radius: 25px; float: left; margin-top: -19px; padding-left: 10px; border: 2px solid #609; width: 87%;" required><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: -2px; margin-left: 5px;">Address Line 2</p><br>
                  <input type="text" name="addressLine2" style="border-radius: 25px; float: left; margin-top: 1px; padding-left: 10px; border: 2px solid #609; width: 87%;"><br><br>
                  <p style="float: left; font-size: 14px; color: #656565; margin-top: 5px; margin-left: 5px;">Pincode*</p><br>
                  <input type="number" name="address_pincode" style="border-radius: 25px; float: left; margin-top: 11px; padding-left: 10px; border: 2px solid #609; width: 50%; margin-left: -62px;" required><br>
               </div>
                  
                <button class="button" name="save_address">Save Address</button>
              </form>
            </div>
          </div>
        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href=#>Vourier</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
$("input[name='package-type']").click(function () {
	$('#show-document').css('display', $(this).val() === 'document' ? 'block' : 'none');
	$('#show-parcel').css('display', $(this).val() === 'parcel' ? 'block' : 'none');
});	
</script>
<script>
$("input[name='business-package-type']").click(function () {
	$('#business-show-document').css('display', $(this).val() === 'document' ? 'block' : 'none');
	$('#business-show-parcel').css('display', $(this).val() === 'parcel' ? 'block' : 'none');
});	
</script>
<script src="js/displayDivs.js"></script>

<script>
	$("input[name='Save_Address']").click(function() {
		if($("#show_address_alias_div").is(":visible")) {
			$("#alias_for_address").prop("required", true);
      $("#1alias_for_address").prop("required", true);
		}
		else {
			$("#alias_for_address").prop("required", false);
      $("#1alias_for_address").prop("required", false);
		}
	});
</script>

<script>
$("button[name='individual']").click(function () {

      document.getElementById("individual").style.color = "#285fdb";
      document.getElementById("individual").style.border = "2px solid #285fdb";

      document.getElementById("business").style.color = "#656565";
      document.getElementById("business").style.border = "2px solid rgba(0,0,0,0.15)";

		if($("#individual_form").is(":visible")){
    	$("#fromName").prop("required", true);
    	$("#fromAddressLine1").prop("required", true);
    	$("#fromMobile").prop("required", true);
    	$("#fromCode").prop("required", true);

    	$("#toName").prop("required", true);
    	$("#toAddressLine1").prop("required", true);
    	$("#toMobile").prop("required", true);
    	$("#toCode").prop("required", true);

    	$("#individual-document-quantity").prop("required", true);

    	$("input[name='package-type']").click(function () {
    		$('#show-document').css('display', ($(this).val() === 'document') ? 'block':'none');
    		if($("#individual-document-quantity").is(":visible")){
    			$("#individual-document-quantity").prop("required", true);
    			$("#product_name").prop("required", false);
    			$("#quantity").prop("required", false);
    			$("#price").prop("required", false);
    			$("#weight").prop("required", false);
    			$("#length").prop("required", false);
    			$("#breadth").prop("required", false);
    			$("#height").prop("required", false);
    		} else{
    			$("#individual-document-quantity").prop("required", false);
    			$("#product_name").prop("required", true);
    			$("#quantity").prop("required", true);
    			$("#price").prop("required", true);
    			$("#weight").prop("required", true);
    			$("#length").prop("required", true);
    			$("#breadth").prop("required", true);
    			$("#height").prop("required", true);
    		}
});
  	}
});

$("button[name='business']").click(function () {

    document.getElementById("business").style.color = "#285fdb";
    document.getElementById("business").style.border = "2px solid #285fdb";

    document.getElementById("individual").style.color = "#656565";
    document.getElementById("individual").style.border = "2px solid rgba(0,0,0,0.15)";

		if($("#business-form").is(":visible")){
    	$("#business-fromName").prop("required", true);
    	$("#business-fromAddressLine1").prop("required", true);
    	$("#business-fromMobile").prop("required", true);
    	$("#business-fromCode").prop("required", true);

    	$("#business-toName").prop("required", true);
    	$("#business-toAddressLine1").prop("required", true);
    	$("#business-toMobile").prop("required", true);
    	$("#business-toCode").prop("required", true);
		
		$("#business-document-quantity").prop("required", true);

		$("input[name='business-package-type']").click(function () {
    		$('#business-show-document').css('display', ($(this).val() === 'document') ? 'block':'none');
    		if($("#business-document-quantity").is(":visible")){
    			$("#business-document-quantity").prop("required", true);
    			$("#business-product_name").prop("required", false);
    			$("#business-quantity").prop("required", false);
    			$("#business-price").prop("required", false);
    			$("#business-weight").prop("required", false);
    			$("#business-length").prop("required", false);
    			$("#business-breadth").prop("required", false);
    			$("#business-height").prop("required", false);
    		} else{
    			$("#business-document-quantity").prop("required", false);
    			$("#business-product_name").prop("required", true);
    			$("#business-quantity").prop("required", true);
    			$("#business-price").prop("required", true);
    			$("#business-weight").prop("required", true);
    			$("#business-length").prop("required", true);
    			$("#business-breadth").prop("required", true);
    			$("#business-height").prop("required", true);
    		}
});
  	}
});
</script>
<script type="text/javascript">
	window.onload = function(){
    document.getElementById('individual').click();
    document.getElementById('individual').focus();

    document.getElementById("fromName").style.cursor = "not-allowed";
    document.getElementById("fromName").disabled = true;

    document.getElementById("business-fromName").style.cursor = "not-allowed";
    document.getElementById("business-fromName").disabled = true;

    document.getElementById("fromMobile").style.cursor = "not-allowed";
    document.getElementById("fromMobile").disabled = true;

    document.getElementById("business-fromMobile").style.cursor = "not-allowed";
    document.getElementById("business-fromMobile").disabled = true;

    document.getElementById("fromAddressLine1").style.cursor = "not-allowed";
    document.getElementById("fromAddressLine1").disabled = true;

    document.getElementById("business-fromAddressLine1").style.cursor = "not-allowed";
    document.getElementById("business-fromAddressLine1").disabled = true;

    document.getElementById("fromAddressLine2").style.cursor = "not-allowed";
    document.getElementById("fromAddressLine2").disabled = true;

    document.getElementById("business-fromAddressLine2").style.cursor = "not-allowed";
    document.getElementById("business-fromAddressLine2").disabled = true;

    document.getElementById("fromCode").style.cursor = "not-allowed";
    document.getElementById("fromCode").disabled = true;

    document.getElementById("business-fromCode").style.cursor = "not-allowed";
    document.getElementById("business-fromCode").disabled = true;

    document.getElementById("address_select").style.cursor = "pointer";
    document.getElementById("address_select").style.backgroundColor = "#fafafa";
    document.getElementById("address_select").disabled = false;
    document.getElementById("business-address_select").style.cursor = "pointer";
    document.getElementById("business-address_select").style.backgroundColor = "#fafafa";
    document.getElementById("business-address_select").disabled = false;
    document.getElementById("sku_select_div").style.display = 'none';
    document.getElementById("save_address_div").style.display = 'none';
    document.getElementById("show_address_alias_div").style.display = 'none';
    document.getElementById("business-save_address_div").style.display = 'none';
    document.getElementById("business-show_address_alias_div").style.display = 'none';
}
</script>

<script type="text/javascript">
  function set_address(id) {
    if((id.localeCompare('saved_address'))==0) {
      document.getElementById("fromName").style.cursor = "not-allowed";
      document.getElementById("fromName").disabled = true;

      document.getElementById("business-fromName").style.cursor = "not-allowed";
      document.getElementById("business-fromName").disabled = true;


      document.getElementById("fromMobile").style.cursor = "not-allowed";
      document.getElementById("fromMobile").disabled = true;

      document.getElementById("business-fromMobile").style.cursor = "not-allowed";
      document.getElementById("business-fromMobile").disabled = true;



      document.getElementById("fromAddressLine1").style.cursor = "not-allowed";
      document.getElementById("fromAddressLine1").disabled = true;

      document.getElementById("business-fromAddressLine1").style.cursor = "not-allowed";
      document.getElementById("business-fromAddressLine1").disabled = true;


      document.getElementById("fromAddressLine2").style.cursor = "not-allowed";
      document.getElementById("fromAddressLine2").disabled = true;

      document.getElementById("business-fromAddressLine2").style.cursor = "not-allowed";
      document.getElementById("business-fromAddressLine2").disabled = true;


      document.getElementById("fromCode").style.cursor = "not-allowed";
      document.getElementById("fromCode").disabled = true;

      document.getElementById("business-fromCode").style.cursor = "not-allowed";
      document.getElementById("business-fromCode").disabled = true;


      document.getElementById("address_select").style.cursor = "pointer";
      document.getElementById("address_select").style.backgroundColor = "#fafafa";
      document.getElementById("address_select").disabled = false;
      document.getElementById("business-address_select").style.cursor = "pointer";
      document.getElementById("business-address_select").style.backgroundColor = "#fafafa";
      document.getElementById("business-address_select").disabled = false;
      document.getElementById("save_address_div").style.display = 'none';
      document.getElementById("show_address_alias_div").style.display = 'none';
      document.getElementById("business-save_address_div").style.display = 'none';
      document.getElementById("business-show_address_alias_div").style.display = 'none';
      document.getElementById("hide_address_alias").checked = true;
      document.getElementById("business-hide_address_alias").checked = true;
      document.getElementById("alias_for_address").value = "1";
      document.getElementById("1alias_for_address").value = "1";
    }
    else {
      document.getElementById("address_select").style.cursor = "not-allowed";
      document.getElementById("address_select").style.backgroundColor = "#eee";
      document.getElementById("address_select").disabled = true;
      document.getElementById("business-address_select").style.cursor = "not-allowed";
      document.getElementById("business-address_select").style.backgroundColor = "#eee";
      document.getElementById("business-address_select").disabled = true;

      document.getElementById("fromName").style.cursor = "auto";
      document.getElementById("fromName").disabled = false;

      document.getElementById("business-fromName").style.cursor = "auto";
      document.getElementById("business-fromName").disabled = false;

      document.getElementById("fromMobile").style.cursor = "auto";
      document.getElementById("fromMobile").disabled = false;

      document.getElementById("business-fromMobile").style.cursor = "auto";
      document.getElementById("business-fromMobile").disabled = false;


      document.getElementById("fromAddressLine1").style.cursor = "auto";
      document.getElementById("fromAddressLine1").disabled = false;

      document.getElementById("business-fromAddressLine1").style.cursor = "auto";
      document.getElementById("business-fromAddressLine1").disabled = false;


      document.getElementById("fromAddressLine2").style.cursor = "auto";
      document.getElementById("fromAddressLine2").disabled = false;

      document.getElementById("business-fromAddressLine2").style.cursor = "auto";
      document.getElementById("business-fromAddressLine2").disabled = false;


      document.getElementById("fromCode").style.cursor = "auto";
      document.getElementById("fromCode").disabled = false;

      document.getElementById("business-fromCode").style.cursor = "auto";
      document.getElementById("business-fromCode").disabled = false;
      document.getElementById("save_address_div").style.display = 'block';
      document.getElementById("business-save_address_div").style.display = 'block';
    }
  } 
</script>

<script type="text/javascript">
	function set_sku(id) {
		if((id.localeCompare('saved_sku'))==0) {
			document.getElementById('sku_select_div').style.display = 'block';
			document.getElementById('save_sku_div').style.display = 'none';
			document.getElementById("product_sku").disabled = true;
		    document.getElementById("business-weight").disabled = true;
		    document.getElementById("business-length").disabled = true;
		    document.getElementById("business-breadth").disabled = true;
		    document.getElementById("business-height").disabled = true;

		    document.getElementById("product_sku").style.cursor = "not-allowed";
		    document.getElementById("business-weight").style.cursor = "not-allowed";
		    document.getElementById("business-length").style.cursor = "not-allowed";
		    document.getElementById("business-breadth").style.cursor = "not-allowed";
		    document.getElementById("business-height").style.cursor = "not-allowed";
		    document.getElementById("Save_SKU").checked = true;
		}
		else {
			document.getElementById('sku_select_div').style.display = 'none';
			document.getElementById('save_sku_div').style.display = 'block';
			document.getElementById("product_sku").disabled = false;
		    document.getElementById("business-weight").disabled = false;
		    document.getElementById("business-length").disabled = false;
		    document.getElementById("business-breadth").disabled = false;
		    document.getElementById("business-height").disabled = false;

		    document.getElementById("product_sku").style.cursor = "auto";
		    document.getElementById("business-weight").style.cursor = "auto";
		    document.getElementById("business-length").style.cursor = "auto";
		    document.getElementById("business-breadth").style.cursor = "auto";
		    document.getElementById("business-height").style.cursor = "auto";
		}
	}
</script>

<script type="text/javascript">
  function myUpdateFunction(e) {
    var alias = e.target.value;
    if((alias.localeCompare("None"))==0) {
    document.getElementById("fromName").value = "";
    document.getElementById("fromMobile").value = "";
    document.getElementById("fromAddressLine1").value = "";
    document.getElementById("fromAddressLine2").value = "";
    document.getElementById("fromCode").value = "";
    document.getElementById("business-fromName").value = "";
    document.getElementById("business-fromMobile").value = "";
    document.getElementById("business-fromAddressLine1").value = "";
    document.getElementById("business-fromAddressLine2").value = "";
    document.getElementById("business-fromCode").value = "";

    document.getElementById("fromName").disabled = true;
    document.getElementById("fromMobile").disabled = true;
    document.getElementById("fromAddressLine1").disabled = true;
    document.getElementById("fromAddressLine2").disabled = true;
    document.getElementById("fromCode").disabled = true;
    document.getElementById("business-fromName").disabled = true;
    document.getElementById("business-fromMobile").disabled = true;
    document.getElementById("business-fromAddressLine1").disabled = true;
    document.getElementById("business-fromAddressLine2").disabled = true;
    document.getElementById("business-fromCode").disabled = true;

    document.getElementById("fromName").style.cursor = "not-allowed";
    document.getElementById("fromMobile").style.cursor = "not-allowed";
    document.getElementById("fromAddressLine1").style.cursor = "not-allowed";
    document.getElementById("fromAddressLine2").style.cursor = "not-allowed";
    document.getElementById("fromCode").style.cursor = "not-allowed";
    document.getElementById("business-fromName").style.cursor = "not-allowed";
    document.getElementById("business-fromMobile").style.cursor = "not-allowed";
    document.getElementById("business-fromAddressLine1").style.cursor = "not-allowed";
    document.getElementById("business-fromAddressLine2").style.cursor = "not-allowed";
    document.getElementById("business-fromCode").style.cursor = "not-allowed";
    }
    else {
    document.getElementById("fromName").value = document.getElementById(alias+'_name').value;
    document.getElementById("fromMobile").value = document.getElementById(alias+'_mobile').value;
    document.getElementById("fromAddressLine1").value = document.getElementById(alias+'_address_line_1').value;
    document.getElementById("fromAddressLine2").value = document.getElementById(alias+'_address_line_2').value;
    document.getElementById("fromCode").value = document.getElementById(alias+'_pincode').value;
    document.getElementById("business-fromName").value = document.getElementById(alias+'_name').value;
    document.getElementById("business-fromMobile").value = document.getElementById(alias+'_mobile').value;
    document.getElementById("business-fromAddressLine1").value = document.getElementById(alias+'_address_line_1').value;
    document.getElementById("business-fromAddressLine2").value = document.getElementById(alias+'_address_line_2').value;
    document.getElementById("business-fromCode").value = document.getElementById(alias+'_pincode').value;

    document.getElementById("fromName").style.cursor = "auto";
    document.getElementById("fromMobile").style.cursor = "auto";
    document.getElementById("fromAddressLine1").style.cursor = "auto";
    document.getElementById("fromAddressLine2").style.cursor = "auto";
    document.getElementById("fromCode").style.cursor = "auto";
    document.getElementById("business-fromName").style.cursor = "auto";
    document.getElementById("business-fromMobile").style.cursor = "auto";
    document.getElementById("business-fromAddressLine1").style.cursor = "auto";
    document.getElementById("business-fromAddressLine2").style.cursor = "auto";
    document.getElementById("business-fromCode").style.cursor = "auto";

    document.getElementById("fromName").disabled = false;
    document.getElementById("fromMobile").disabled = false;
    document.getElementById("fromAddressLine1").disabled = false;
    document.getElementById("fromAddressLine2").disabled = false;
    document.getElementById("fromCode").disabled = false;
    document.getElementById("business-fromName").disabled = false;
    document.getElementById("business-fromMobile").disabled = false;
    document.getElementById("business-fromAddressLine1").disabled = false;
    document.getElementById("business-fromAddressLine2").disabled = false;
    document.getElementById("business-fromCode").disabled = false;
    }
  }
</script>

<script type="text/javascript">
	function updateSKU(e) {
		var alias = e.target.value;
	    if((alias.localeCompare("None"))==0) {
	    	document.getElementById("product_sku").value = "";
		    document.getElementById("business-weight").value = "";
		    document.getElementById("business-length").value = "";
		    document.getElementById("business-breadth").value = "";
		    document.getElementById("business-height").value = "";

		    document.getElementById("product_sku").disabled = true;
		    document.getElementById("business-weight").disabled = true;
		    document.getElementById("business-length").disabled = true;
		    document.getElementById("business-breadth").disabled = true;
		    document.getElementById("business-height").disabled = true;

		    document.getElementById("product_sku").style.cursor = "not-allowed";
		    document.getElementById("business-weight").style.cursor = "not-allowed";
		    document.getElementById("business-length").style.cursor = "not-allowed";
		    document.getElementById("business-breadth").style.cursor = "not-allowed";
		    document.getElementById("business-height").style.cursor = "not-allowed";
	    }
	    else {
	    	document.getElementById("product_sku").value = document.getElementById(alias+'_SKU_name').value;
	    	document.getElementById("business-weight").value = document.getElementById(alias+'_SKU_weight').value;
		    document.getElementById("business-length").value = document.getElementById(alias+'_SKU_length').value;
		    document.getElementById("business-breadth").value = document.getElementById(alias+'_SKU_breadth').value;
		    document.getElementById("business-height").value = document.getElementById(alias+'_SKU_height').value;

		    document.getElementById("product_sku").style.cursor = "auto";
		    document.getElementById("business-weight").style.cursor = "auto";
		    document.getElementById("business-length").style.cursor = "auto";
		    document.getElementById("business-breadth").style.cursor = "auto";
		    document.getElementById("business-height").style.cursor = "auto";

		    document.getElementById("product_sku").disabled = false;
		    document.getElementById("business-weight").disabled = false;
		    document.getElementById("business-length").disabled = false;
		    document.getElementById("business-breadth").disabled = false;
		    document.getElementById("business-height").disabled = false;
	    }
	}
</script>

<script type="text/javascript">
	function showDiv(id) {
		if((id.localeCompare('show_address_alias'))==0) {
			document.getElementById("show_address_alias_div").style.display = 'block';
		}
		else {
			document.getElementById("show_address_alias_div").style.display = 'none';
		}
	}
</script>

<script type="text/javascript">
	function businessShowDiv(id) {
		if((id.localeCompare('show_address_alias'))==0) {
			document.getElementById("business-show_address_alias_div").style.display = 'block';
		}
		else {
			document.getElementById("business-show_address_alias_div").style.display = 'none';
		}
	}
</script>

<script type="text/javascript">
	function confirm_override() {
		if(confirm('Another address with similar alias found. Using this alias again, will overwrite the previous saved address. Would you still continue?')) {
			alert("Donje");
		}
	}
</script>

</body>
</html>
