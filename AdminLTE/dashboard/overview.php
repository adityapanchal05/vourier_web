<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | Activities</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>L</b> Co.</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Logistics</b> Company</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
    
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a style="display: inline-block; font-size: 18px; padding-bottom: 5px;">
              <i class="fa fa-inr"></i>
              <p style="display: inline-block;"><?php echo $wallet; ?></p>
            </a>
          </li>
          <li class="dropdown tasks-menu">
            <a href="recharge.php" style="padding-bottom: 5px;">
              <p style="cursor: pointer;"><i class="fa fa-bolt"></i> RECHARGE</p>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $image_link; ?>" class="user-image" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
              <span class="hidden-xs"><?php echo $first_name.' '.$last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">

                <p>
                  <?php echo $first_name.' '.$last_name; ?>
                  <small><?php echo $member_plan.' Member'; ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-left">
                  <a href="buy_plans.php" class="btn btn-default btn-flat" style="margin-left: 33px;">Plans</a>
                </div>
                <div class="pull-right">
                  <a href="users/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $image_link; ?>" class="img-circle" alt="User Image" id="userImage" onerror="this.onerror=null; this.src='dist/img/avatar5.png'">
        </div>
        <div class="pull-left info">
          <p><?php echo $first_name.' '.$last_name; ?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">WELCOME</li>
        <li>
          <a href="dashboard.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="NEW">
            <i class="fa fa-shopping-cart"></i><span>Orders</span>
          </a>
        </li>
        <li class="treeview">
          <a href="pages/widgets.html">
            <i class="fa fa-rotate-left"></i> <span style="cursor: pointer;">Returns</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="create_return_order"><i class="fa fa-plus-square"></i> Add Returns</a></li>
            <li><a href="RETURNS"><i class="fa fa-rotate-right"></i> All Return Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="tracking">
            <i class="fa fa-ship"></i><span>Shipments</span>
          </a>
        </li>
        <li>
          <a href="shipping-charges">
            <i class="fa fa-inr"></i><span>Billing</span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cogs"></i>
            <span style="cursor: pointer;">Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="rate-calculator"><i class="fa fa-check-square"></i> Rate Calculator</a></li>
            <li><a href="rate-calculator"><i class="fa fa-map-marker"></i> Pin-Code Zone Mapping</a></li>
            <li><a href="activities"><i class="fa fa-file-archive-o"></i> Activity</a></li>
            <li><a href="reports"><i class="fa fa-file-code-o"></i> Reports</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-database"></i> <span style="cursor: pointer;">Channels</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="channels-all"><i class="fa fa-database"></i> All Channels</a></li>
            <li><a href="listings"><i class="fa fa-briefcase"></i> Channel Products</a></li>
            <li><a href="#"><i class="fa fa-linkedin-square"></i> Manage Inventory</a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> All Products</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Manage Catalog</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-cog"></i> <span style="cursor: pointer;">Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="general-details"><i class="fa fa-home"></i> Company</a></li>
            <li><a href="couriers"><i class="fa fa-cube"></i> Courier</a></li>
            <li><a href="priority-couriers"><i class="fa fa-plane"></i> Couriers Priority</a></li>
            <li><a href="#"><i class="fa fa-globe"></i> International</a></li>
            <li><a href="#"><i class="fa fa-yen"></i> Tax Classes</a></li>
            <li><a href="#"><i class="fa fa-tag"></i> Category</a></li>
          </ul>
        </li>
        <li>
          <a href="kyc.php">
            <i class="fa fa-500px"></i> <span style="cursor: pointer;">KYC</span>
          </a>
        </li>
        <li><a href="support.php"><i class="fa fa-headphones"></i> <span style="cursor: pointer;">Support</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding-left: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding-left: 20px;">
        
        <div style="width: 100%; float: left; padding-left: 0px;">
          
          <button onclick="window.location.href='overview'" style="float: left; width: 6%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #285fdb #b3b3b3 #b3b3b3; color: #656565; background-color: #fafafa; outline: none; padding: 9px 20px;"><b> Overview </b></button>

          <button onclick="window.location.href='orders'" style="float: left; width: 6%; padding: 9.5px 20px; background-color: #d4d4d4; border: none;"><b> Orders </b></button>

          <button onclick="window.location.href='shipments'" style="float: left; width: 6%; padding: 9.5px 20px; background-color: #d4d4d4; border: none;"><b> Shipments </b></button>

          <button onclick="window.location.href='ndr'" style="float: left; width: 6%; padding: 9.5px 20px; background-color: #d4d4d4; border: none;"><b> NDR </b></button>

        </div>

        <div style="width: 100%; float: left; padding: 20px; border-style: solid; border-width: 1px 1px 1px 1px; border-color: #e6e6e6;">
          <div style="width: 35%; float: left; padding: 10px; padding-top: 10px;">
            <div style="width: 100%; float: left; background-color: #285fdb; padding: 20px; color: white; padding-bottom: 0px;">
              <p align="left" style="font-size: 32px; width: 13%; display: inline-block; float: left;"><i class="fa fa-cart-plus"></i></p>
              <p align="left" style="font-size: 24px; width: 37%; display: inline-block; float: left; margin-top: -10px;"> TODAY'S ORDERS <span style="font-size: 14px;">Yesterday : 0</span></p>
              <p align="right" style="font-size: 32px; margin-top: 5px; color: #ff902b; margin-right: 60px;"><b>0</b></p>
            </div>
            <div style="width: 100%; float: left; background-color: #285fdb; padding: 20px; color: white; padding-bottom: 0px;">
              <p align="left" style="font-size: 32px; width: 13%; display: inline-block; float: left;"><i class="fa fa-cart-plus"></i></p>
              <p align="left" style="font-size: 24px; width: 39%; display: inline-block; float: left; margin-top: -10px;"> TODAY'S REVENUE <span style="font-size: 14px;">Yesterday : <i class="fa fa-inr"></i> 0</span></p>
              <p align="right" style="font-size: 32px; margin-top: 5px; color: #27c24c; margin-right: 50px;"><b><i class="fa fa-inr"></i>0</b></p>
            </div>
            <div style="width: 100%; float: left; background-color: white; padding: 20px; color: #656565; padding-bottom: 0px;">
            <p align="left" style="font-size: 32px; width: 13%; display: inline-block; float: left;"><i class="fa fa-cart-plus"></i></p>
            <p align="left" style="font-size: 22px; width: 44%; display: inline-block; float: left;">AVG. SHIPPING COST</p>
              <p align="right" style="font-size: 22px; color: #285fdb; margin-right: 30px;"><b><i class="fa fa-inr"></i> 0.00</b></p>
          </div>
          </div>

          <div style="width: 65%; float: left; padding: 10px; padding-top: 10px;">
            <div style="width: 33%; float: left; background-color: #285fdb; padding: 20px; color: white;">
              <p style="font-size: 32px;"><b>0</b></p>
              <p style="font-size: 16px; margin-top: -10px;">UNMAPPED</p>
            </div>
            <div style="width: 33%; float: left; background-color: #285fdb; padding: 20px; color: white;">
              <p style="font-size: 32px;"><b>0</b></p>
              <p style="font-size: 16px; margin-top: -10px;">OUT OF STOCK</p>
            </div>
            <div style="width: 33%; float: left; background-color: #285fdb; padding: 20px; color: white;">
            <p style="font-size: 32px;"><b>0</b></p>
              <p style="font-size: 16px; margin-top: -10px;">PICKUP ERROR</p>
            </div>
            <div style="width: 99%; float: left; background-color: white; padding: 20px; color: #656565;">
              <div style="width: 25%; float: left;">
                <div style="background-color: #676767 !important; border-radius: 40px; padding: 20px 32px; width: 76px; margin: 0 auto; margin-bottom: 10px;">
                  <p style="color: white; margin: 0 auto; font-size: 24px;"><b>0</b></p>
                </div>
                <a href="ALL" style="color: #555555;">NEW ORDERS</a>
              </div>
              <div style="width: 25%; float: left;">
                <div style="background-color: #676767 !important; border-radius: 40px; padding: 20px 32px; width: 76px; margin: 0 auto; margin-bottom: 10px;">
                  <p style="color: white; margin: 0 auto; font-size: 24px;"><b>0</b></p>
                </div>
                <a href="ALL" style="color: #555555;">INVOICED</a>
              </div>
              <div style="width: 25%; float: left;">
                <div style="background-color: #676767 !important; border-radius: 40px; padding: 20px 32px; width: 76px; margin: 0 auto; margin-bottom: 10px;">
                  <p style="color: white; margin: 0 auto; font-size: 24px;"><b>0</b></p>
                </div>
                <a href="ALL" style="color: #555555;">READY TO SHIP</a>
              </div>
              <div style="width: 25%; float: left;">
                <div style="background-color: #676767 !important; border-radius: 40px; padding: 20px 32px; width: 76px; margin: 0 auto; margin-bottom: 10px;">
                  <p style="color: white; margin: 0 auto; font-size: 24px;"><b>0</b></p>
                </div>
                <a href="ALL" style="color: #555555;">PICKUP SCHEDULED</a>
              </div>
            </div>
          </div>

           <div style="width: 98%; float: left; background-color: #ffffff; padding: 15px; margin-left: 10px;">
            <div style="width: 100%; background-color: #285fdb; display: inline-block;">
              <div style="width: 25%; float: left; padding: 0px 15px 0px 15px;">
                <h5 style="color: white; font-size: 16px;">COD Available</h5>
                <h5 style="color: white; font-size: 14px;"><i class="fa fa-inr"></i> 0.00</h5>
              </div>
              <div style="width: 25%; float: left; padding: 0px 15px 0px 15px;">
                <h5 style="color: white; font-size: 16px;">Last COD Remitted</h5>
                <h5 style="color: white; font-size: 14px;"><i class="fa fa-inr"></i> 0.00</h5>
              </div>
              <div style="width: 25%; float: left; padding: 0px 15px 0px 15px;">
                <h5 style="color: white; font-size: 16px;">Total COD Remitted</h5>
                <h5 style="color: white; font-size: 14px;"><i class="fa fa-inr"></i> 0.00</h5>
              </div>
              <div style="width: 25%; float: left; padding: 0px 15px 0px 15px;">
                <h5 style="color: white; font-size: 16px;">Remitted Initiated</h5>
                <h5 style="color: white; font-size: 14px;"><i class="fa fa-inr"></i> 0.00</h5>
              </div>
            </div>
          </div>

          <div style="width: 100%; float: left; padding: 15px; padding-left: 10px;">
            <div style="width: 100%; float: left;">
              <button onclick="displayOrderDiv();" name="orderDiv" id="orderDiv" style="float: left; width: 6%; border-width: 2px 1px 0px 1px; border-style: solid; border-color: #285fdb #b3b3b3 #b3b3b3; color: #656565; background-color: white; outline: none; padding: 9px 20px;"><b> Orders </b></button>

              <button onclick="displayRevenueDiv();" name="revenueDiv" id="revenueDiv" style="float: left; width: 6%; border: none; color: #656565; background-color: #d4d4d4; outline: none; padding: 9px 20px;"><b> Revenue </b></button>
            </div>

            <div id="orders" style="width: 100%; float: left; padding: 15px; background-color: white; display: inline-block; display: none;">
              <div style="width: 100%; float: left; background-color: white; border: 1px solid #e4eaec; border-radius: 3px;">
                
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Orders</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b>12</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Orders Shipped</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b>3</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Orders Delivered</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b>0</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Orders Cancelled</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b>0</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Orders RTO</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b>1</b></span>
                </div>

              </div>
            </div>

            <div id="revenue" style="width: 100%; float: left; padding: 15px; background-color: white; display: inline-block; display: none;">
              <div style="width: 100%; float: left; background-color: white; border: 1px solid #e4eaec; border-radius: 3px;">
                
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Total Revenue</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b><i class="fa fa-inr"></i> 400</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Weekly Revenue</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b><i class="fa fa-inr"></i> 100</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Monthly Revenue</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b><i class="fa fa-inr"></i> 200</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Quarterly Revenue</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b><i class="fa fa-inr"></i> 100</b></span>
                </div>
                <hr style="width: 100%; color: #656565; margin-bottom: 0px;">
                <div style="width: 100%; float: left; padding: 10px;">
                  <span style="float: left; color: #656565; font-size: 14px; margin-top: 5px;">Yearly Revenue</span>
                  <span style="float: right; background-color: #285fdb; color: white; padding: .2em .6em .3em; border-radius: .25em;"><b><i class="fa fa-inr"></i> 400</b></span>
                </div>

              </div>
            </div>

          </div>

        </div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://www.linkedin.com/in/akshay-sharma-ab933b17a/">Akshay Sharma</a>.</strong> All rights
    reserved.
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
$("button[name='orderDiv']").click(function () {

      document.getElementById("orderDiv").style.backgroundColor = "white";
      document.getElementById("orderDiv").style.borderTop = "2px solid #285fdb";

      document.getElementById("revenueDiv").style.backgroundColor = "#d4d4d4";
      document.getElementById("revenueDiv").style.border = "none";
});

$("button[name='revenueDiv']").click(function () {

    document.getElementById("revenueDiv").style.backgroundColor = "white";
    document.getElementById("revenueDiv").style.borderTop = "2px solid #285fdb";

    document.getElementById("orderDiv").style.backgroundColor = "#d4d4d4";
    document.getElementById("orderDiv").style.border = "none";
});
</script>

<script src="js/displayDivs.js"></script>

<script type="text/javascript">
 window.onload = function() {
  document.getElementById('orderDiv').click();
 }  
</script>

</body>
</html>