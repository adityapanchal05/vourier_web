<?php
  function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    restore_error_handler();

if(isset($_GET['order'])) {
    $orderID = $_GET['order'];
}
    require "vendor/autoload.php";
    include 'conn.php';
       
    $invoiceQuery = "SELECT * from customer_orders WHERE Email = '".$Email."' AND Order_ID = '".$orderID."'";

        $result = Opencon() -> query($invoiceQuery);
        if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

	        $customer_name = $row['Customer_Name'];
                $customer_mobile = $row['customer_mobile'];
                $customer_address_line_1 = $row['customer_address_line_1'];
                $customer_address_line_2 = $row['customer_address_line_2'];
                $customer_pincode = $row['customer_pincode'];

                $sender_name = $row['Sender_Name'];
                $sender_mobile = $row['sender_mobile'];
                $sender_address_line_1 = $row['sender_address_line_1'];
                $sender_address_line_2 = $row['sender_address_line_2'];
                $sender_pincode = $row['sender_pincode'];

                $operator = $row['operator'];
                $cod = $row['COD'];

                $order_type = $row['Order_Type'];
                $order_date = $row['order_date'];
                $length = $row['length'];
                $breadth = $row['breadth'];
                $height = $row['height'];
                $Weight = $row['Weight'];
		
		$id = $row['Shipment_ID'];
		$code = $row['routing_code'];
                $barcode = $row['barcode_number'];
                $awb = $row['AWB_Number'];

    		$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
		file_put_contents('barcode.png', $generator->getBarcode($barcode, $generator::TYPE_CODE_128, 3, 50));

		if(strcmp($cod, 'Yes')==0) {
                    $cod = 'POSTPAID';
                }
                else {
                    $cod = 'PREPAID';
                }
	    }
	}
require('fpdf/fpdf.php');

$pdf = new FPDF('P', 'mm', array(100,150));
$pdf->AddPage();

$pdf->Rect(3, 3, 94, 144, 'D');

//Left
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetXY(5, 10);
$pdf->Cell(0, 0, 'DELIVER To:', 0, 0, 'L');

$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(54, 5);
$pdf->Cell(5, 20, $customer_name, 0, 0, 'L');
$pdf->SetXY(54, 5);
$pdf->Cell(5, 28, $customer_address_line_1, 0, 0, 'L');
$pdf->SetXY(54, 5);
$pdf->Cell(5, 36, $customer_address_line_2, 0, 0, 'L');
$pdf->SetXY(54, 5);
$pdf->Cell(5, 44, $customer_pincode, 0, 0, 'L');
$pdf->SetXY(54, 5);
$pdf->Cell(5, 52, 'Ph: '.$customer_mobile, 0, 0, 'L');
$pdf->SetXY(53, 5);

$pdf->SetDrawColor(0, 0, 0);
$pdf->Line(50, 5, 50, 36);

//Right
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetXY(52, 5);
$pdf->Cell(40, 10, 'Shipped By:', 0, 0, '');

$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(7, 5);
$pdf->Cell(0, 20, $sender_name, 0, 0, '');
$pdf->SetXY(7, 5);
$pdf->Cell(0, 28, $sender_address_line_1, 0, 0, '');
$pdf->SetXY(7, 5);
$pdf->Cell(0, 36, $sender_address_line_2, 0, 0, '');
$pdf->SetXY(7, 5);
$pdf->Cell(0, 44, $sender_pincode, 0, 0, '');
$pdf->SetXY(7, 5);
$pdf->Cell(0, 52, 'Ph: '.$sender_mobile, 0, 0, '');

$pdf->Line(3,39,97,39);

$pdf->SetFont('Arial', 'B', 13);
$pdf->SetXY(47, 37);
$pdf->Cell(0, 20, $cod, 0, 0, 'R');

$pdf->SetFont('Arial', 'B', 13);
$pdf->SetXY(5, 50);

$pdf->Cell(0, 15,'Routing Code: '. $code, 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(5, 42); 
$pdf->Write(5,'TRK # ');
$pdf->SetXY(18, 41.9);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Write(5,''.$awb.'');
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetXY(5, 48.5);
$pdf->Write(5,'Courier: '.$operator.'');
$pdf->Image('barcode.png',20,62,65,20);

$pdf->Line(3,87,97,87);

$pdf->SetFont('Arial', '', 8);
$pdf->SetXY(5, 89);
$pdf->Write(5,'DIMENSIONS : '.$length.'x'.$breadth.'x'.$height.'');


$pdf->SetDrawColor(0, 0, 0);
$pdf->Line(36, 93, 36, 90);

$pdf->SetXY(38, 91.5);
$pdf->Cell(0,0,'WEIGHT : '.$Weight.'');

$pdf->SetDrawColor(0, 0, 0);
$pdf->Line(74, 93, 74, 90);

$pdf->SetXY(5, 93);
$pdf->Write(5,'SHIPMENT DATE : '.$order_date.'');

$pdf->Output('shipping-label-'.$id.'-'.$awb.'.pdf', 'D');
    
?>
