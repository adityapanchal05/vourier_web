 
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="vourier.com">Vourier</a>.</strong> All rights
    reserved.
  </footer>
