<!DOCTYPE html>
<html>
<head>
	<title>Logistics Company | Support</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php

  include 'conn.php';

	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    */

	?>

	<div class="wrapper">
    <?php include 'aside.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%;">
    	<div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden;">
    		<p align="left" style="color: #656565; font-size: 24px;">Customer Support</p>
    		<a style="float: right; background-color: #285fdb !important; color: white; font-size: 15px; padding: 5px 80px; margin-top: -42px; border-radius: 3px;" href="#">View Old Tickets</a>
    		<a style="float: right; background-color: #285fdb !important; color: white; font-size: 15px; padding: 5px 80px; margin-top: -42px; border-radius: 3px; margin-right: 300px;" href="#">View Tickets</a>
    		<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">

    		<div style="width: 100%; margin-top: 30px; margin-left: 93px; min-width: 1500px;">
	    		<div style="width: 20%; float: left;">
	    			<img src="images/order_management.png" style="background-color: #ffdb71; padding: 35px; border-radius: 6px; padding-left: 51px; padding-right: 51px;">
	    			<p style="color: #656565; margin-top: 5px;"><b>Order Management</b></p>
	    		</div>
	    		<div style="width: 13%; float: left;">
	    			<img src="images/channel_integration.png" style="background-color: #98ecf8; padding: 35px; border-radius: 6px; padding-left: 51px; padding-right: 51px;">
	    			<p style="color: #656565; margin-top: 5px;"><b>Channel Integration</b></p>
	    		</div>
	    		<div style="width: 20%; float: left;">
	    			<img src="images/international_shipping.png" style="background-color: #89f6b3; padding: 35px; border-radius: 6px; padding-left: 51px; padding-right: 51px;">
	    			<p style="color: #656565; margin-top: 5px;"><b>International Shipping</b></p>
	    		</div>
	    		<div style="width: 13%; float: left;">
	    			<img src="images/billing.png" style="background-color: #d4b3e7; padding: 35px; border-radius: 6px; padding-left: 51px; padding-right: 51px;">
	    			<p style="color: #656565; margin-top: 5px;"><b>Billing and Remittances</b></p>
	    		</div>
	    		<div style="width: 20%; float: left;">
	    			<img src="images/weight_disputes.png" style="background-color: #ffed7e; padding: 35px; border-radius: 6px; padding-left: 51px; padding-right: 51px;">
	    			<p style="color: #656565; margin-top: 5px;"><b>Weight Disputes</b></p>
	    		</div>
	    		<br><br><br><br><br><br><br><br><br><br><br><br><br>
    		</div>

    		<div style="width: 100%; float: left;">
    			<p style="color: #656565; font-size: 25px; width: 100%; text-align: center;">Need Help? Our support team is here to assist you!</p>
    			<br>
    		</div>

    		<div style="width: 100%; float: left; padding: 20px;">
    			<div style="float: left;  border: 1px solid #CFD4D6; width: 30%; padding: 20px; border-radius: 6px;">
    				<p align="left" style="color: #656565; font-size: 24px;">KAM Support (Bronze)</p>
    				<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
    				<p align="left" style="color: #656565; font-size: 14px; margin-top: 20px;"><b>KAM Name</b></p>
    				<p align="right" style="color: #656565; font-size: 14px; margin-top: -30px;">Customer Support</p>
    				<p align="left" style="color: #656565; font-size: 14px; margin-top: 20px;"><b>Email ID</b></p>
    				<p align="right" style="color: #656565; font-size: 14px; margin-top: -30px;">support@logistics.in</p>
    				<p align="left" style="color: #656565; font-size: 14px; margin-top: 20px;"><b>Phone No.</b></p>
    				<p align="right" style="color: #656565; font-size: 14px; margin-top: -30px; margin-bottom: 37px;">+91-8272846915</p>
    			</div>
    			<div style="float: left;  border: 1px solid #CFD4D6; width: 30%; padding: 20px; border-radius: 6px; margin-left: 40px;">
    				<p align="left" style="color: #656565; font-size: 24px;">Helpdesk</p>
    				<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
    				<p align="left" style="color: #656565; font-size: 17px; margin-top: 19px;">Create a ticket to tell us more about your issue. Select your issue category, sub-category, and submit.</p>
    				<a href="#" style="float: left; color: white; background-color: #285fdb; padding: 10px 20px 10px 20px; border-radius: 3px; margin-top: 20px; margin-bottom: 20px;">Create Ticket</a>
    			</div>
    			<div style="float: left;  border: 1px solid #CFD4D6; width: 30%; padding: 20px; border-radius: 6px; margin-left: 40px;">
    				<p align="left" style="color: #656565; font-size: 24px;">Most Popular Topics</p>
    				<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
    				<a href="#" style="color: #656565; float: left; margin-top: 18px; font-size: 17px;"><span><i class="fa fa-caret-right"></i></span>&nbsp&nbspDelay in Delivery</a><br><br><br>
    				<a href="#" style="color: #656565; float: left; margin-top: -6px; font-size: 17px;"><span><i class="fa fa-caret-right"></i></span>&nbsp&nbspUndelivered Shipments</a><br><br>
    				<a href="#" style="color: #656565; float: left; margin-top: -10px; font-size: 17px;"><span><i class="fa fa-caret-right"></i></span>&nbsp&nbspPick Up Delay</a><br><br>
    				<a href="#" style="color: #656565; float: left; margin-top: -10px; font-size: 17px;"><span><i class="fa fa-caret-right"></i></span>&nbsp&nbspWeight Discrepancy</a>
    			</div>
    		</div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include 'footer.php';?>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>
