<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | Activities</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
  include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

/*    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);*/

	?>

<div class="wrapper">

<?php include '../aside.php';?>
<div class="content-wrapper">
  <section class="content" style="text-align: center; overflow-x: scroll; width: 100%;">
    <div class="create-order-heading" style="width: 100%; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden;">
      <p align="left" style="color: #656565; font-size: 24px;">Shipping Rates Calculator</p>
      <hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
      <div style="width: 100%; min-width: 1500px; padding: 20px; margin-top: 10px; background-color: #ededed; padding-top: 1px;">

          <div style="width: 100%; margin-bottom: 17px">
            <button style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" name="domestic" id="domestic" onclick="displayDomesticPlans();"><b>Domestic</b></button>
            <button name="international" id="international" style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" onclick="displayInternationalPlans();"><b>International</b></button>
            <div id="domestic_plans" style="width: 100%; float: left; background-color: #fafafa; display: none; margin-top: 30px; padding-left: 10px;">
              <form method="post" enctype="multipart/form-data">
              <div id="rest" style="width: 56%; float: left;">
                <div id="1" style="width: 100%; float: left;">
                  <div style="width: 100%; float: left;">
                    <p align="left" style="font-size: 14px; color: #656565;"><b>Shipment Type</b><span style="color: red;"><b> *</b></span></p>
                  </div>
                  <div style="width: 50%; float: left;">
                    <select name="order_type" style="float: left; width: 100%; height: 35px; padding: 6px 16px; font-size: 14px; color: #3a3f51; border-radius: 4px; border: 1px solid #CFD4D6;">
                      <option value="forward">Forward</option>
                      <option value="forward">Return</option>
                    </select>
                  </div>
                </div>

                <div style="width: 100%; float: left;">
                  <p> </p>
                </div>

                <div id="2" style="width: 45%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>Pick-up Area Pincode</b><span style="color: red;"><b> *</b></span></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <input type="number" name="fromPinCode" id="from_pin" placeholder="6 Digits Pick-up Area Pincode" maxlength="6" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 88%; border: 1px solid #CFD4D6;" required><span style=" width: 10%; font-size: 30px;"><i class="fa fa-arrow-circle-o-right"></i></span>
                      <p id="invalid_from_code" align="left" style="color: red; display: none;">Invalid Pincode</p>
                    </div>
                  </div>
                </div>

                <div id="3" style="width: 50%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>Delivery Area Pincode</b><span style="color: red;"><b> *</b></span></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <input type="number" name="toPinCode" id="to_pin" placeholder="6 Digits Delivery Area Pincode" maxlength="6" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 98%; border: 1px solid #CFD4D6;" required>
                      <p id="invalid_to_code" align="left" style="color: red; margin-top: 42px; display: none;">Invalid Pincode</p>
                    </div>
                  </div>
                </div>
                <div style="width: 100%; float: left;"><p> </p></div>
                <div id="4" style="width: 45%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>Approximate Weight (kg)</b><span style="color: red;"><b> *</b><button style="border: none; color: #656565; background-color: #fafafa; outline: none;" title="Note: The minimum chargeable weight is 0.5kg"><i class="fa fa-info-circle"></i></button></span></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <input type="number" name="weight" placeholder="E.g. 0.5" min="0.1" step="0.1" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 88%; border: 1px solid #CFD4D6;" required>
                    </div>
                  </div>
                </div>

                <div id="5" style="width: 50%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>Dimensions (cm)</b><span style="color: red;"><b> *</b></span></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <input type="number" name="length" placeholder="L" min="1" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 14%; border: 1px solid #CFD4D6;" required>
                      <p style="float: left; margin-left: 15px; margin-right: 15px; margin-top: 8px;"><b> X </b></p>
                      <input type="number" name="breadth" placeholder="B" min="1" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 14%; border: 1px solid #CFD4D6;" required>
                      <p style="float: left; margin-left: 15px; margin-right: 15px; margin-top: 8px;"><b> X </b></p>
                      <input type="number" name="height" placeholder="H" min="1" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 14%; border: 1px solid #CFD4D6;" required>
                    </div>
                  </div>
                </div>

                <div id="6" style="width: 45%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>COD</b></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <select name="cod_type" style="float: left; width: 88%; height: 35px; padding: 6px 16px; font-size: 14px; color: #3a3f51; border-radius: 4px; border: 1px solid #CFD4D6;">
                      <option value="No">No</option>
                    </select>
                    </div>
                  </div>
                </div>

                <div id="7" style="width: 50%; float: left;">
                  <div style="width: 100%; float: left;">
                    <div style="width: 100%; float: left; margin-top: 30px;">
                      <p align="left" style="font-size: 14px; color: #656565;"><b>Declared Value in INR</b><span style="color: red;"><b> *</b></span></p>
                    </div>

                    <div style="width: 100%; float: left;">
                      <button style="background-color: #fafafa; border: 1px solid #CFD4D6; color: #656565; border-radius: 4px; float: left; height: 35px; padding: 5px 15px; outline: none;"><i class="fa fa-inr"></i></button>
                      <input type="number" name="item_value" placeholder="Declared Value" min="1" style="border-radius: 4px; height: 35px; padding: 6px 16px; float: left; width: 89%; border: 1px solid #CFD4D6;" required>
                    </div>
                  </div>
                </div>
                
                <div id="7" style="width: 100%; float: left;">
                  <div style="width: 100%; float: left; margin-top: 30px;">
                    <button id="calculate_price" name="calculate_price" style="float: left; color: white; background-color: #285fdb; border: none; border-radius: 3px; font-size: 18px; padding: 6px 16px;"><i class="fa fa-calculator"></i>&nbsp Calculate</button>
                  </div>
                </div>
             </div>
             <div id="google_maps" style="width: 44%; float: left;">
              <div id="map" style="width: 100%; height: 550px;"></div>
             </div>
             </form>

               <?php
               if(isset($_POST['calculate_price'])) {

                      $order_type = $_POST['order_type'];
                      $fromCode = $_POST['fromPinCode'];
                      $toCode = $_POST['toPinCode'];
                      $weight = $_POST['weight'];
                      $length = $_POST['length'];
                      $breadth = $_POST['breadth'];
                      $height = $_POST['height'];
                      $cod = $_POST['cod_type'];
                      $price = $_POST['item_value'];

                      if(strcmp($cod, 'No')==0) {
                        $cod = 'prepaid';
                      }
                      else {
                        $cod = 'postpaid';
                      }

                      if(strcmp($member_plan, 'LITE')==0) {
                        $operator_plan = 'operator_details';
                      }
                      if(strcmp($member_plan, 'BASIC')==0) {
                        $operator_plan = 'operator_details_basic';
                      }
                      if(strcmp($member_plan, 'ADVANCED')==0) {
                        $operator_plan = 'operator_details_advanced';
                      }
                      if(strcmp($member_plan, 'PRO')==0) {
                        $operator_plan = 'operator_details_pro';
                      }

                      $all_operators = Opencon()->query("SELECT * FROM ".$operator_plan." ");
                      if($all_operators->num_rows > 0) {
                        while ($row = $all_operators->fetch_assoc()) {
                          $operators[] = $row['operator_name'];
                        }
                      }
                      $operators = array_unique($operators);

                      echo '<div style="width: 55%; float: left; margin-top: -70px; margin-bottom: 40px; background-color: #f3f3f3; padding: 15px;">
                      <div style="width: 100%; float: left;">';
                      echo '<p align="left" style="color: #515253; font-size: 19px;"><b>Surface Mode</b></p>';
                      echo '<p style="float: left; font-size: 14px;">Rates for shipping a '.$weight.'kg '.$cod.' packet from '.$fromCode.' to '.$toCode.'</p>';
                      echo '</div>
                     <div style="float: left; width: 100%;">
                      <div style="float: left; width: 14.5%; border-right: 1px solid white; font-size: 15px;">
                         <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>S.NO.</b></p>
                       </div>
                       <div style="float: left; width: 33.5%; border-right: 1px solid white; font-size: 15px;">
                         <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COURIER PROVIDER</b></p>
                       </div>
                       <div style="float: left; width: 21.5%; border-right: 1px solid white; font-size: 15px;">
                         <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>RATE (INR)</b></p>
                       </div>
                       <div style="float: left; width: 30.5%; border-right: 1px solid white; font-size: 15px;">
                         <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>LOGISTICS RATING</b></p>
                       </div>';

                       $i = 0;

                      foreach ($operators as $current_operator) {
                        $fromCode_query = Opencon()->query("SELECT * FROM pincode_database WHERE pin_code = '".$fromCode."' ");
                        if($fromCode_query->num_rows > 0) {
                          while($row = $fromCode_query->fetch_assoc()) {
                            $fromCode_city = $row['City'];
                            $fromCode_state = $row['State'];
                            $fromCode_ncr = $row['NCR'];
                            $fromCode_metro = $row['METRO'];
                            $fromCode_NE = $row['NorthEast'];
                            $fromCode_fedex = $row['FedEx'];
                            $fromCode_dhl = $row['DHL'];
                            $fromCode_dtdc = $row['DTDC'];
                            $fromCode_speedPost = $row['Speed_Post'];
                          }
                        }
                        $toCode_query = "SELECT * FROM pincode_database WHERE pin_code = '".$toCode."' ";
                        $toCode_result = Opencon()->query($toCode_query);
                        if($toCode_result->num_rows > 0) {
                          while ($row = $toCode_result->fetch_assoc()) {
                            $toCode_city = $row['City'];
                            $toCode_state = $row['State'];
                            $toCode_ncr = $row['NCR'];
                            $toCode_metro = $row['METRO'];
                            $toCode_NE = $row['NorthEast'];
                            $toCode_fedex = $row['FedEx'];
                            $toCode_dhl = $row['DHL'];
                            $toCode_dtdc = $row['DTDC'];
                            $toCode_speedPost = $row['Speed_Post'];
                            $toCode_cod = $row['COD_Available'];
                            $toCode_prepaid = $row['Prepaid_Available'];
                            $toCode_reverse = $row['Reverse_Available'];
                            $toCode_routing = $row['Routing_Code'];
                          }
                        }

                          if(strcmp($fromCode_city, $toCode_city)==0) {
                            $zone = "z_a";
                          }
                          else if(strcmp($fromCode_state, $toCode_state)==0) {
                            $zone = "z_b";
                          }
                          else if(strcmp($fromCode_ncr, 'TRUE')==0 && strcmp($toCode_ncr, 'TRUE')==0) {
                            $zone = "z_b";
                          }
                          else if(strcmp($fromCode_metro, 'TRUE')==0 && strcmp($toCode_metro, 'TRUE')==0) {
                            $zone = "z_c";
                          }
                          else if(strcmp($toCode_NE, 'TRUE')==0) {
                            $zone = "z_e";
                          }
                          else {
                            $zone = "z_d";
                          }

                          if(strcmp($current_operator, 'FedEx')==0) {
                            if(strcmp($fromCode_fedex, 'TRUE')==0 && strcmp($toCode_fedex, 'TRUE')==0) {
                              $i++;

                              $fedex_price = "SELECT price FROM ".$operator_plan." WHERE operator_name = 'FedEx' AND drop_zone = '".$zone."'";
                              $fedex_price_result = Opencon()->query($fedex_price);
                              if($fedex_price_result->num_rows > 0) {
                                while ($row = $fedex_price_result->fetch_assoc()) {
                                    $fedex_price_float = $row['price'];
                                }
                                $fedex_price_final = $fedex_price_float * ceil($weight/0.5);
                              }

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); border-left: 1px solid rgba(0,0,0,0.12);; width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                             <p align="left" style="font-size: 14px;">'.$i.'</p>
                             </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 33.5%; padding-left: 5px; padding-top: 20px; margin-top: -2px;">
                                    <a href="#" style="font-size: 16px; float: left; color: #285fdb;">'.$current_operator.'</a>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 21.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                                    <p align="left" style="font-size: 14px;"><i class="fa fa-inr"></i> '.$fedex_price_final.'</p>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 30.5%; padding-left: 5px; padding-top: 20px; margin-top: -11px;">
                                    <p align="left" style="font-size: 14px;"><img src="images/star_all.png">(4.9)</p>
                                    </div>';
                            }
                          }

                          if(strcmp($current_operator, 'DHL')==0) {
                            if(strcmp($fromCode_dhl, 'TRUE')==0 && strcmp($toCode_dhl, 'TRUE')==0) {
                              $i++;

                              $dhl_price = "SELECT price FROM ".$operator_plan." WHERE operator_name = 'DHL' AND drop_zone = '".$zone."'";
                              $dhl_price_result = Opencon()->query($dhl_price);
                              if($dhl_price_result->num_rows > 0) {
                                while ($row = $dhl_price_result->fetch_assoc()) {
                                    $dhl_price_float = $row['price'];
                                }
                                $dhl_price_final = $dhl_price_float * ceil($weight/0.5);
                              }

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); border-left: 1px solid rgba(0,0,0,0.12);; width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                             <p align="left" style="font-size: 14px;">'.$i.'</p>
                             </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 33.5%; padding-left: 5px; padding-top: 20px; margin-top: -2px;">
                                    <a href="#" style="font-size: 16px; float: left; color: #285fdb;">'.$current_operator.'</a>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 21.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                                    <p align="left" style="font-size: 14px;"><i class="fa fa-inr"></i> '.$dhl_price_final.'</p>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 30.5%; padding-left: 5px; padding-top: 20px; margin-top: -11px;">
                                    <p align="left" style="font-size: 14px;"><img src="images/star_all.png">(4.9)</p>
                                    </div>';
                            }
                          }

                          if(strcmp($current_operator, 'DTDC')==0) {
                            if(strcmp($fromCode_dtdc, 'TRUE')==0 && strcmp($toCode_dtdc, 'TRUE')==0) {
                              $i++;

                              $dtdc_price = "SELECT price FROM ".$operator_plan." WHERE operator_name = 'DTDC' AND drop_zone = '".$zone."'";
                              $dtdc_price_result = Opencon()->query($dtdc_price);
                              if($dtdc_price_result->num_rows > 0) {
                                while ($row = $dtdc_price_result->fetch_assoc()) {
                                    $dtdc_price_float = $row['price'];
                                }
                                $dtdc_price_final = $dtdc_price_float * ceil($weight/0.5);
                              }

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); border-left: 1px solid rgba(0,0,0,0.12);; width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                             <p align="left" style="font-size: 14px;">'.$i.'</p>
                             </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 33.5%; padding-left: 5px; padding-top: 20px; margin-top: -2px;">
                                    <a href="#" style="font-size: 16px; float: left; color: #285fdb;">'.$current_operator.'</a>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 21.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                                    <p align="left" style="font-size: 14px;"><i class="fa fa-inr"></i> '.$dtdc_price_final.'</p>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 30.5%; padding-left: 5px; padding-top: 20px; margin-top: -11px;">
                                    <p align="left" style="font-size: 14px;"><img src="images/star_all.png">(4.9)</p>
                                    </div>';
                            }
                          }

                          if(strcmp($current_operator, 'Speed_Post')==0) {
                            if(strcmp($fromCode_speedPost, 'TRUE')==0 && strcmp($toCode_speedPost, 'TRUE')==0) {
                              $i++;

                              $speedPost_price = "SELECT price FROM ".$operator_plan." WHERE operator_name = 'Speed_Post' AND drop_zone = '".$zone."'";
                              $speedPost_price_result = Opencon()->query($speedPost_price);
                              if($speedPost_price_result->num_rows > 0) {
                                while ($row = $speedPost_price_result->fetch_assoc()) {
                                    $speedPost_price_float = $row['price'];
                                }
                                $speedPost_price_final = $speedPost_price_float * ceil($weight/0.5);
                              }

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); border-left: 1px solid rgba(0,0,0,0.12);; width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                             <p align="left" style="font-size: 14px;">'.$i.'</p>
                             </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 33.5%; padding-left: 5px; padding-top: 20px; margin-top: -2px;">
                                    <a href="#" style="font-size: 16px; float: left; color: #285fdb;">'.$current_operator.'</a>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 21.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                                    <p align="left" style="font-size: 14px;"><i class="fa fa-inr"></i> '.$speedPost_price_final.'</p>
                                    </div>';

                              echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 30.5%; padding-left: 5px; padding-top: 20px; margin-top: -11px;">
                                    <p align="left" style="font-size: 14px;"><img src="images/star_all.png">(4.9)</p>
                                    </div>';
                            }
                          }

                      }

                    }
               ?>
             </div>
           </div>
           
           <div style="width: 100%; float: left;">
               <p align="left" style="font-size: 24px; color: #285fdb; width: 100%;">Plan-Wise Shipping Rates</p>
               <hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
           </div>

           <div style="width: 65%; float: left; border: 1.5px solid rgba(0, 0, 0, 0.12); border-top: none; margin-top: 10px;">
              <div style="width: 100%; float: left; background-color: #ededed;">
                   <button style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" name="lite" id="lite" onclick="displayLitePlans();"><b>Lite Plan</b></button>
                  <button name="basic" id="basic" style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" onclick="displayBasicPlans();"><b>Basic Plan</b></button>
                  <button style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" name="advanced" id="advanced" onclick="displayAdvancedPlans();"><b>Advanced Plan</b></button>
                  <button name="pro" id="pro" style="color: #515253; padding: 8px 20px; background-color: #d4d4d4; display: inline-block; float: left; border: none; outline: none;" onclick="displayProPlans();"><b>Pro Plan</b></button>
               </div>
               <div id="lite_plans" style="float: left; width: 100%; padding: 20px; display: none;">
                <div style="width: 100%; float: left;">
                  <p align="left" style="color: #515253; font-size: 17px;"><b>Surface Mode</b></p>
                </div>
                <div style="float: left; width: 100%; border: 1px solid rgba(0, 0, 0, 0.12);">
                   <div style="float: left; width: 14.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COURIER PARTNER</b></p>
                   </div>
                   <div style="float: left; width: 10.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN CITY</b></p>
                   </div>
                   <div style="float: left; width: 11.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN STATE</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>METRO TO METRO</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>REST OF INDIA</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>NORTH EAST, J&K</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD CHARGES</b></p>
                   </div>
                   <div style="float: left; width: 7.5%;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD %</b></p>
                   </div>
                <?php
                    $lite_plans = Opencon()->query("SELECT * FROM operator_details");
                    if($lite_plans->num_rows > 0) {
                      while ($row = $lite_plans->fetch_assoc()) {
                        $operators[] = $row['operator_name'];
                      }
                    }
                    $operators = array_unique($operators);

                    foreach ($operators as $current_operator) {
                       $zone_a_query = Opencon()->query("SELECT price FROM operator_details WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_a' ");
                       if($zone_a_query->num_rows > 0) {
                        while($row = $zone_a_query->fetch_assoc())
                          $zone_a_price = $row['price'];
                       }

                       $zone_b_query = Opencon()->query("SELECT price FROM operator_details WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_b' ");
                       if($zone_b_query->num_rows > 0) {
                        while($row = $zone_b_query->fetch_assoc())
                          $zone_b_price = $row['price'];
                       }

                       $zone_c_query = Opencon()->query("SELECT price FROM operator_details WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_c' ");
                       if($zone_c_query->num_rows > 0) {
                        while($row = $zone_c_query->fetch_assoc())
                          $zone_c_price = $row['price'];
                       }

                       $zone_d_query = Opencon()->query("SELECT price FROM operator_details WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_d' ");
                       if($zone_d_query->num_rows > 0) {
                        while($row = $zone_d_query->fetch_assoc())
                          $zone_d_price = $row['price'];
                       }

                       $zone_e_query = Opencon()->query("SELECT price FROM operator_details WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_e' ");
                       if($zone_e_query->num_rows > 0) {
                        while($row = $zone_e_query->fetch_assoc())
                          $zone_e_price = $row['price'];
                       }

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$current_operator.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 10.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_a_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 11.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_b_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_c_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_d_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 7.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">2.50%</p>
                            </div>';
                    }

                ?>
              </div>
            </div>
            <p align="left" style="color: #656565; margin-left: 20px;">*Rates shown are for 1/2 kg shipment and are inclusive of GST</p>

              <div id="basic_plans" style="float: left; width: 100%; padding: 20px; display: none;">
                 <div style="width: 100%; float: left;">
                  <p align="left" style="color: #515253; font-size: 17px;"><b>Air Mode</b></p>
                 </div>
                 <div style="float: left; width: 100%; border: 1px solid rgba(0, 0, 0, 0.12);">
                   <div style="float: left; width: 14.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COURIER PARTNER</b></p>
                   </div>
                   <div style="float: left; width: 10.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN CITY</b></p>
                   </div>
                   <div style="float: left; width: 11.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN STATE</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>METRO TO METRO</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>REST OF INDIA</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>NORTH EAST, J&K</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD CHARGES</b></p>
                   </div>
                   <div style="float: left; width: 7.5%;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD %</b></p>
                   </div>

                   <?php
                    $lite_plans = Opencon()->query("SELECT * FROM operator_details_basic");
                    if($lite_plans->num_rows > 0) {
                      while ($row = $lite_plans->fetch_assoc()) {
                        $operators[] = $row['operator_name'];
                      }
                    }
                    $operators = array_unique($operators);

                    foreach ($operators as $current_operator) {
                       $zone_a_query = Opencon()->query("SELECT price FROM operator_details_basic WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_a' ");
                       if($zone_a_query->num_rows > 0) {
                        while($row = $zone_a_query->fetch_assoc())
                          $zone_a_price = $row['price'];
                       }

                       $zone_b_query = Opencon()->query("SELECT price FROM operator_details_basic WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_b' ");
                       if($zone_b_query->num_rows > 0) {
                        while($row = $zone_b_query->fetch_assoc())
                          $zone_b_price = $row['price'];
                       }

                       $zone_c_query = Opencon()->query("SELECT price FROM operator_details_basic WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_c' ");
                       if($zone_c_query->num_rows > 0) {
                        while($row = $zone_c_query->fetch_assoc())
                          $zone_c_price = $row['price'];
                       }

                       $zone_d_query = Opencon()->query("SELECT price FROM operator_details_basic WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_d' ");
                       if($zone_d_query->num_rows > 0) {
                        while($row = $zone_d_query->fetch_assoc())
                          $zone_d_price = $row['price'];
                       }

                       $zone_e_query = Opencon()->query("SELECT price FROM operator_details_basic WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_e' ");
                       if($zone_e_query->num_rows > 0) {
                        while($row = $zone_e_query->fetch_assoc())
                          $zone_e_price = $row['price'];
                       }

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$current_operator.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 10.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_a_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 11.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_b_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_c_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_d_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 7.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">2.50%</p>
                            </div>';
                    }
                   ?>

                 </div>
               </div>

               <div id="advanced_plans" style="float: left; width: 100%; padding: 20px; display: none;">
                 <div style="width: 100%; float: left;">
                  <p align="left" style="color: #515253; font-size: 17px;"><b>Air Mode</b></p>
                 </div>
                 <div style="float: left; width: 100%; border: 1px solid rgba(0, 0, 0, 0.12);">
                   <div style="float: left; width: 14.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COURIER PARTNER</b></p>
                   </div>
                   <div style="float: left; width: 10.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN CITY</b></p>
                   </div>
                   <div style="float: left; width: 11.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN STATE</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>METRO TO METRO</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>REST OF INDIA</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>NORTH EAST, J&K</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD CHARGES</b></p>
                   </div>
                   <div style="float: left; width: 7.5%;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD %</b></p>
                   </div>

                   <?php
                    $lite_plans = Opencon()->query("SELECT * FROM operator_details_advanced");
                    if($lite_plans->num_rows > 0) {
                      while ($row = $lite_plans->fetch_assoc()) {
                        $operators[] = $row['operator_name'];
                      }
                    }
                    $operators = array_unique($operators);

                    foreach ($operators as $current_operator) {
                       $zone_a_query = Opencon()->query("SELECT price FROM operator_details_advanced WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_a' ");
                       if($zone_a_query->num_rows > 0) {
                        while($row = $zone_a_query->fetch_assoc())
                          $zone_a_price = $row['price'];
                       }

                       $zone_b_query = Opencon()->query("SELECT price FROM operator_details_advanced WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_b' ");
                       if($zone_b_query->num_rows > 0) {
                        while($row = $zone_b_query->fetch_assoc())
                          $zone_b_price = $row['price'];
                       }

                       $zone_c_query = Opencon()->query("SELECT price FROM operator_details_advanced WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_c' ");
                       if($zone_c_query->num_rows > 0) {
                        while($row = $zone_c_query->fetch_assoc())
                          $zone_c_price = $row['price'];
                       }

                       $zone_d_query = Opencon()->query("SELECT price FROM operator_details_advanced WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_d' ");
                       if($zone_d_query->num_rows > 0) {
                        while($row = $zone_d_query->fetch_assoc())
                          $zone_d_price = $row['price'];
                       }

                       $zone_e_query = Opencon()->query("SELECT price FROM operator_details_advanced WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_e' ");
                       if($zone_e_query->num_rows > 0) {
                        while($row = $zone_e_query->fetch_assoc())
                          $zone_e_price = $row['price'];
                       }

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$current_operator.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 10.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_a_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 11.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_b_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_c_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_d_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 7.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">2.50%</p>
                            </div>';
                    }
                   ?>

                 </div>
               </div>



               <div id="pro_plans" style="float: left; width: 100%; padding: 20px; display: none;">
                 <div style="width: 100%; float: left;">
                  <p align="left" style="color: #515253; font-size: 17px;"><b>Air Mode</b></p>
                 </div>
                 <div style="float: left; width: 100%; border: 1px solid rgba(0, 0, 0, 0.12);">
                   <div style="float: left; width: 14.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COURIER PARTNER</b></p>
                   </div>
                   <div style="float: left; width: 10.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN CITY</b></p>
                   </div>
                   <div style="float: left; width: 11.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>WITHIN STATE</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>METRO TO METRO</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>REST OF INDIA</b></p>
                   </div>
                   <div style="float: left; width: 15.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>NORTH EAST, J&K</b></p>
                   </div>
                   <div style="float: left; width: 12.5%; border-right: 1px solid white;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD CHARGES</b></p>
                   </div>
                   <div style="float: left; width: 7.5%;">
                     <p align="left" style="background-color: #d4d4d4; padding: 10px;"><b>COD %</b></p>
                   </div>

                   <?php
                    $lite_plans = Opencon()->query("SELECT * FROM operator_details_pro");
                    if($lite_plans->num_rows > 0) {
                      while ($row = $lite_plans->fetch_assoc()) {
                        $operators[] = $row['operator_name'];
                      }
                    }
                    $operators = array_unique($operators);

                    foreach ($operators as $current_operator) {
                       $zone_a_query = Opencon()->query("SELECT price FROM operator_details_pro WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_a' ");
                       if($zone_a_query->num_rows > 0) {
                        while($row = $zone_a_query->fetch_assoc())
                          $zone_a_price = $row['price'];
                       }

                       $zone_b_query = Opencon()->query("SELECT price FROM operator_details_pro WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_b' ");
                       if($zone_b_query->num_rows > 0) {
                        while($row = $zone_b_query->fetch_assoc())
                          $zone_b_price = $row['price'];
                       }

                       $zone_c_query = Opencon()->query("SELECT price FROM operator_details_pro WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_c' ");
                       if($zone_c_query->num_rows > 0) {
                        while($row = $zone_c_query->fetch_assoc())
                          $zone_c_price = $row['price'];
                       }

                       $zone_d_query = Opencon()->query("SELECT price FROM operator_details_pro WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_d' ");
                       if($zone_d_query->num_rows > 0) {
                        while($row = $zone_d_query->fetch_assoc())
                          $zone_d_price = $row['price'];
                       }

                       $zone_e_query = Opencon()->query("SELECT price FROM operator_details_pro WHERE operator_name = '".$current_operator."' AND drop_zone = 'z_e' ");
                       if($zone_e_query->num_rows > 0) {
                        while($row = $zone_e_query->fetch_assoc())
                          $zone_e_price = $row['price'];
                       }

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 14.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$current_operator.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 10.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_a_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 11.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_b_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_c_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_d_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 15.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 12.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">'.$zone_e_price.'</p>
                            </div>';

                       echo '<div style="float: left; border-bottom: 1px solid rgba(0,0,0,0.12); border-right: 1px solid rgba(0,0,0,0.12); width: 7.5%; padding-left: 5px; padding-top: 20px; margin-top: -10px;">
                            <p align="left">2.50%</p>
                            </div>';
                    }
                   ?>

                 </div>
               </div>

           </div>
           <div style="width: 100%; float: left; margin-top: 20px;">
               <p align="left" style="color: #656565; font-size: 20px;">Important Terms</p>
               <p align="left" style="color: #656565;">1. Dead/Dry weight or volumetric weight whichever is higher will be taken while calculating the freight rates.</p>
               <p align="left" style="color: #656565;">2. Fixed COD charge or COD % of the order value whichever is higher will be taken while calculating the COD fee.</p>
               <p align="left" style="color: #656565;">3. Above prices are inclusive of GST.</p>
               <p align="left" style="color: #656565;">4. The above pricing is subject to change based on fuel surcharges and courier company base rates.</p>
               <p align="left" style="color: #656565;">5. Return charges may apply over and above the freight fee incase of E-com Express.</p>
               <p align="left" style="color: #656565;">6. Volumetric weight is calculated LxBxH/5000 for all courier companies except for Fedex Surface and Aramex. In case of Fedex surface, volumetric weight is calculated as LxBxH/4500 and for Aramex, it is LxBxH/6000 (length, breadth, height has to be taken in Centimeters and divided by denominator, this will give the value in Kilograms).</p>
               <p align="left" style="color: #656565;">7. Other Charges like Octroi charges, state entry tax and fees, address correction charges if applicable shall be charged extra.</p>
               <p align="left" style="color: #656565;">8. RTO (return to origin) shipment will be charged differently from the forward delivery rate.</p>
               <p align="left" style="color: #656565;">9. For any queries a ticket has to be raised on <span><a href="#" style="color: #285fdb;"><b>support@logistics.in</b></a></span></p>
               <p align="left" style="color: #656565;">10. The maximum liability if any is limited to whatever compensation the logistics partner offers to Company in event of a claim by the Merchant, provided such claim is raised by the Merchant within one (1) month from the date of such damage or loss or theft..</p>
               <p align="left" style="color: #656565;">11. Shiprocket shall not assist in shipping goods that come under the category of prohibited, dangerous goods or restricted good.</p>
               <p align="left" style="color: #656565;">12. Detailed terms and conditions can be review on <span><a href="#">www.logistics.in/merchant-agreement</a></span></p>
               <button id="submit">Get Lat</button>
          </div>

      </div>
      <div id="international_plans" style="width: 100%; float: left; background-color: #fafafa; display: none; margin-top: 25px;">
          <p style="font-size: 32px; color: #656565;">COMING SOON!</p>
      </div>

    </div>
  </section>
</div>
<?php include '../footer.php';?>
</div>
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>