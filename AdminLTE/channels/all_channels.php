<!DOCTYPE html>
<html>
<head>
	<title>Logistic Company | General Details</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">

  	<link rel="stylesheet" type="text/css" href="css/toggle_button.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php
include '../conn.php';
	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="index.php";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

    /*$dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
*/
	?>

	<div class="wrapper">
    <?php include '../aside.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="text-align: center; overflow-x: scroll; width: 100%; padding: 0px;">
    	<div class="create-order-heading" style="width: 100%; background-color: #ededed; float: left; display: inline-block; min-width: 1500px; overflow-x: hidden; padding: 0px;">
        
        	<div style="background-color: #fafbfc; border-bottom: 1px solid #b3b3b3; color: #212121; width: 100%; float: left; padding: 15px; padding-bottom: 0px;">
          		<p align="left" style="font-size: 23px;">Channels<span><button style="float: right; background-color: #e6e6e6; padding: 4px 13px; border-color: transparent; border-radius: 3px; font-size: 14px;"><i class="fa fa-plus"></i>&nbsp&nbsp&nbspAdd New Channel</button></span></p>
          		
        	</div>

        	<div style="width: 100%; float: left; padding: 10px; background-color: #ffffff;">
        		<input type="text" style="float: left; width: 24.5%; border: 1px solid #dde6e9; border-radius: 3px; height: 26px; margin-left: 10px; padding-left: 10px;" placeholder="Search by Name">
        		<button style="float: left; box-shadow: 0 0px 1px rgba(0, 0, 0, 0.07); border: solid 1px #ccc; margin-left: -3px; padding-top: 2px; padding-bottom: 2px;"><i class="fa fa-search"></i></button>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa;">
        		<div style="width: 12%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>NAME</b><span><button style="float: right; color: #285fdb; border: none; background-color: transparent; outline: none;"><i class="fa fa-caret-down"></i></button></span></p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>LAST ORDER SYNC</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>LAST INVENTORY SYNC</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 12%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>CONNECTION STATUS</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>LAST CONNECTION SYNC</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 14%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>CONNECTION RESPONSE</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 9%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 0px;"><b>STATUS</b><span><button style="float: right; color: #656565; border: none; background-color: transparent; outline: none;"><i class="fa fa-sort"></i></button></span></p>
        		</div>

        		<div style="width: 8%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #333; font-size: 15px; margin-bottom: 2px;"><b>ACTION</b></p>
        		</div>
        	</div>

        	<div style="width: 100%; float: left; border: 1px solid #eeeeee; background-color: #fafafa; border-top: none;">
        		<div style="width: 12%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040; margin-bottom: 20px;">CUSTOM</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 20px;">Not Available</p>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 20px;">Not Available</p>
        		</div>

        		<div style="width: 12%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<i style="background-color: white; color: #27c24c; padding-bottom: 12px;" class="fa fa-check-circle fa-2x"></i>
        		</div>

        		<div style="width: 15%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 20px;">2019-05-26 17:02:43</p>
        		</div>

        		<div style="width: 14%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p style="margin-bottom: 20px;"><br></p>
        		</div>

        		<div style="width: 9%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<p align="left" style="color: #404040;margin-bottom: 20px;">Active</p>
        		</div>

        		<div style="width: 8%; float: left; border: 1px solid #eeeeee; padding: 10px;">
        			<button class="btn btn-info" style="float: left; border: none; border-radius: 3px; background-color: #285fdb; outline: none; margin-bottom: 8px; padding: 6px 16px;"><i class="fa fa-edit"></i>&nbsp&nbsp&nbspEdit</button>
        		</div>
        	</div>

    	</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../footer.php';?>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>
</html>