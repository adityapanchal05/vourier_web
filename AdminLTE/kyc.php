<!DOCTYPE html>
<html>
<head>
	<title>Logistics Company | KYC</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="bower_components/morris.js/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  	<link rel="stylesheet" type="text/css" href="css/create_order.css">


  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font -->
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<?php

  include 'conn.php';

	 function session_error_function() {
      echo '<script language="javascript">';
      echo 'alert("Session Over. Please login again.");';
      echo 'location.href="Home";';
      echo '</script>';
    }

    set_error_handler('session_error_function');
    session_start();
    
    $Email = $_SESSION['Email'];
    $first_name = $_SESSION['FirstName'];
    $last_name = $_SESSION['LastName'];
    $image_link = $_SESSION['ImageLink'];
    $gstin = $_SESSION['GSTIN'];
    $address = $_SESSION['ADDRESS'];
    $wallet = $_SESSION['Wallet'];
    $member_plan = $_SESSION['Member_Plan'];
    restore_error_handler();

   /* $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "logistics_v2";

    Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);*/

	?>

	<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php include 'aside.php';?>
    <!-- Main content -->
    <section class="content" style="text-align: center;" id="id_dashboard">
    	<div class="create-order-heading">
    		<p align="left" style="color: #656565; font-size: 24px;">Complete Your KYC</p>
    		<p align="left" style="color: #656565; font-size: 14px; margin-top: -10px;">Upload your personal or company documents for Verification</p>
    		<hr style="border-top: 1px solid rgba(0,0,0,0.15); width: 100%; margin-top: 10px; margin-bottom: 3px;">
    			<div style="padding: 20px;">
	    			<button style="color: #285fdb; padding: 10px; background-color: white; border: 2px solid #285fdb; display: inline-block; float: left;" name="individual" id="individual" style="" onclick="displayIndividualForm();"><b>Individual</b></button>
	    			<button class="order-type" name="business" id="business" style="color: #285fdb; padding: 10px; background-color: white; border: 2px solid #285fdb; display: inline-block; float: left; margin-left: 20px;" onclick="displayBusinessForm();"><b>Company</b></button><br><br>
	    			<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
	    			<div id="individual_form" style="float: left; width: 100%; display: none; display: inline-block;">
	    				
	    				<div id="verify_with_otp" style="width: 100%; float: left;">
	    					<form method="post" enctype="multipart/form-data">
	    						<p align="left" style="color: #656565; font-size: 18px;"><b>Document 1 - AADHAR Card</b><span style="color: red;"><b>*</b></span></p>
	    						<p align="left" style="color: #656565; font-size: 14px;"><b>Aadhar Number</b></p>
	    						<input type="number" name="aadhar-number" id="aadhar-number" placeholder="12 Digit Aadhar Number" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;">
	    						<div style="width: 100%; float: left; margin-top: 10px;">
	    							<p align="left" id="aadhar-counter" style="font-size: 12px; color: red;">Invalid Aadhar Number</p>
	    						</div>
	    						<div style="width: 100%; float: left; margin-top: 10px;">
	    							<button id="aadhar-otp-button" style="float: left; background-color: #285fdb; border: none;" class="btn btn-info">Generate OTP</button>
	    						</div>
    						</form>
    						<div style="width: 100%; float: left; padding: 20px; margin-left: 15px;">
    							<p align="left" style="font-size: 18px; color: #656565;"><b>OR</b></p>
    						</div>
    						<div style="width: 100%; float: left;">
    							<button class="btn btn-info" name="verify-without-otp-button" onclick="verifyWithoutOtp();" style="background-color: #285fdb; float: left; margin-top: -12px;">Verify Without OTP</button>
    						</div>
	    				</div>

	    				<div id="verify_without_otp" style="width: 100%; float: left; display: none;">
	    					<form method="post" enctype="multipart/form-data">
	    						<p align="left" style="color: #656565; font-size: 18px;"><b>Document 1 - AADHAR Card</b><span style="color: red;"><b>*</b></span></p>
	    						<p align="left" style="color: #656565; font-size: 14px;"><b>Aadhar Number</b></p>
	    						<input type="number" name="aadhar-number" id="aadhar-number" placeholder="Enter Aadhar Number" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;"><br><br><br>
	    						<input type="text" name="aadhar-name" placeholder="Enter Name on Aadhar" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;">

	    						<div style="width: 100%; float: left; display: inline-block; margin-top: 20px;">
	    							<div style="width: 20%; float: left;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
	    								<input type="file" name="aadhar-front-image">
	    							</div>
	    							<div style="width: 20%; float: left;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
	    								<input type="file" name="aadhar-front-image">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<p align="left" style="color: #656565; font-size: 18px;"><b>Document 2</b><span style="color: red;"><b>*</b></span></p>
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<select style="float: left; height: 40px; border-radius: 3px; color: #656565; width: 17%;">
	    									<option>Select Document Type 2</option>
	    									<option>Bank Account Statement</option>
	    									<option>Driving License</option>
	    									<option>Valid Passport</option>
	    									<option>Voter ID Card</option>
	    									<option>Pan Card</option>
	    								</select>
	    								<br><br><br>
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<input type="text" name="document-id" id="document-id" placeholder="Enter Document ID" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
	    							</div>
	    							<div style="width: 100%; float: left; margin-top: 10px;">
	    								<input type="text" name="document-name" placeholder="Enter Name on Document" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
	    								<input type="file" name="document-front-image">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
	    								<input type="file" name="document-front-image">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Selfie Image</b><span style="color: red;"><b>*</b></span></p>
	    								<input type="file" name="selfie-image">
	    							</div>
	    							<div style="width: 100%; float: left; margin-top: 20px;">
	    								<button class="btn btn-info" name="submit-documents" style="background-color: #285fdb; float: left;">Submit Documents</button>
	    							</div>
	    							<div style="width: 100%; float: left; padding: 20px; margin-left: 15px;">
		    							<p align="left" style="font-size: 18px; color: #656565;"><b>OR</b></p>
		    						</div>
	    						</div>
	    					</form>
	    					<div style="width: 100%; float: left;">
	    						<button class="btn btn-info" name="verify-with-otp-button" onclick="verifyWithOtp();" style="background-color: #285fdb; float: left; margin-top: -12px;">Verify Using Aadhar OTP</button>
	    					</div>
	    				</div>
    				</div>
    				<div id="business-form" style="display: inline-block; float: left; width: 100%; display: none;">
    					<form method="post" enctype="multipart/form-data">
    						<div style="width: 100%; float: left;">
	    						<select style="float: left; height: 40px; border-radius: 3px; color: #656565; width: 17%;" onchange="showProperDiv(event);">
	    							<option value="None">Select Company Type</option>
	    							<option value="sole">Sole Proprietor</option>
	    							<option value="other">Partnership</option>
	    							<option value="other">Limited Liability Partnership</option>
	    							<option value="other">Public Company Limited</option>
	    							<option value="other">Private Limited Company</option>
	    						</select><br><br><br>
	    					</div>

	    					<div id="sole_proprietor" style="float: left; width: 100%; display: none; display: inline-block;">
			    				<div id="sole_proprietor_verify_with_otp" style="width: 100%; float: left;">
			    					<form method="post" enctype="multipart/form-data">
			    						<p align="left" style="color: #656565; font-size: 18px;"><b>Document 1 - AADHAR Card</b><span style="color: red;"><b>*</b></span></p>
			    						<p align="left" style="color: #656565; font-size: 14px;"><b>Aadhar Number</b></p>
			    						<input type="number" name="aadhar-number" id="aadhar-number" placeholder="12 Digit Aadhar Number" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;">
			    						<div style="width: 100%; float: left; margin-top: 10px;">
			    							<button id="aadhar-otp-button" style="float: left; background-color: #285fdb; border: none;" class="btn btn-info">Generate OTP</button>
			    						</div>
		    						</form>
		    						<div style="width: 100%; float: left; padding: 20px; margin-left: 15px;">
		    							<p align="left" style="font-size: 18px; color: #656565;"><b>OR</b></p>
		    						</div>
		    						<div style="width: 100%; float: left;">
		    							<button class="btn btn-info" name="sole_proprietor_verify-without-otp-button" onclick="return sole_proprietor_verifyWithoutOtp();" style="background-color: #285fdb; float: left; margin-top: -12px;">Verify Without OTP</button>
		    						</div>
			    				</div>

			    				<div id="sole_proprietor_verify_without_otp" style="width: 100%; float: left; display: none;">
			    					<form method="post" enctype="multipart/form-data">
			    						<p align="left" style="color: #656565; font-size: 18px;"><b>Document 1 - AADHAR Card</b><span style="color: red;"><b>*</b></span></p>
			    						<p align="left" style="color: #656565; font-size: 14px;"><b>Aadhar Number</b></p>
			    						<input type="number" name="aadhar-number" id="aadhar-number" placeholder="Enter Aadhar Number" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;"><br><br><br>
			    						<input type="text" name="aadhar-name" placeholder="Enter Name on Aadhar" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;">

			    						<div style="width: 100%; float: left; display: inline-block; margin-top: 20px;">
			    							<div style="width: 20%; float: left;">
			    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
			    								<input type="file" name="aadhar-front-image">
			    							</div>
			    							<div style="width: 20%; float: left;">
			    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
			    								<input type="file" name="aadhar-front-image">
			    							</div>
			    							<div style="width: 100%; float: left;">
			    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
			    							</div>
			    							<div style="width: 100%; float: left;">
			    								<p align="left" style="color: #656565; font-size: 18px;"><b>Document 2</b><span style="color: red;"><b>*</b></span></p>
			    							</div>
			    							<div style="width: 100%; float: left;">
			    								<select style="float: left; height: 40px; border-radius: 3px; color: #656565; width: 17%;">
			    									<option>Select Document Type 2</option>
			    									<option>Bank Account Statement</option>
			    									<option>Driving License</option>
			    									<option>Valid Passport</option>
			    									<option>Voter ID Card</option>
			    									<option>Pan Card</option>
			    								</select>
			    								<br><br><br>
			    							</div>
			    							<div style="width: 100%; float: left;">
			    								<input type="text" name="document-id" id="document-id" placeholder="Enter Document ID" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
			    							</div>
			    							<div style="width: 100%; float: left; margin-top: 10px;">
			    								<input type="text" name="document-name" placeholder="Enter Name on Document" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
			    							</div>
			    							<div style="width: 20%; float: left; margin-top: 15px;">
			    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
			    								<input type="file" name="document-front-image">
			    							</div>
			    							<div style="width: 20%; float: left; margin-top: 15px;">
			    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
			    								<input type="file" name="document-front-image">
			    							</div>
			    							<div style="width: 100%; float: left;">
			    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
			    							</div>
			    							<div style="width: 20%; float: left; margin-top: 15px;">
			    								<p align="left" style="width: 100%; color: #656565;"><b>Selfie Image</b><span style="color: red;"><b>*</b></span></p>
			    								<input type="file" name="selfie-image">
			    							</div>
			    							<div style="width: 100%; float: left; margin-top: 20px;">
			    								<button class="btn btn-info" name="submit-documents" style="background-color: #285fdb; float: left;">Submit Documents</button>
			    							</div>
			    							<div style="width: 100%; float: left; padding: 20px; margin-left: 15px;">
				    							<p align="left" style="font-size: 18px; color: #656565;"><b>OR</b></p>
				    						</div>
			    						</div>
			    					</form>
			    					<div style="width: 100%; float: left;">
			    						<button class="btn btn-info" name="sole_proprietor_verify-with-otp-button" onclick="return sole_proprietor_verifyWithOtp();" style="background-color: #285fdb; float: left; margin-top: -12px;">Verify Using Aadhar OTP</button>
			    					</div>
			    				</div>
    						</div>

	    					<div id="other_company" style="width: 100%; float: left; display: none;">
	    					<form method="post" enctype="multipart/form-data">
	    						<p align="left" style="color: #656565; font-size: 18px;"><b>Document 1 - AADHAR Card</b><span style="color: red;"><b>*</b></span></p>
	    						<p align="left" style="color: #656565; font-size: 14px;"><b>Aadhar Number</b></p>
	    						<input type="number" name="aadhar-number" id="aadhar-number" placeholder="Enter Aadhar Number" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;"><br><br><br>
	    						<input type="text" name="aadhar-name" placeholder="Enter Name on Aadhar" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 45px; width: 25%; font-size: 16px; padding-left: 10px;">

	    						<div style="width: 100%; float: left; display: inline-block; margin-top: 20px;">
	    							<div style="width: 20%; float: left;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
	    								<input type="file" name="aadhar-front-image">
	    							</div>
	    							<div style="width: 20%; float: left;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
	    								<input type="file" name="aadhar-front-image">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<p align="left" style="color: #656565; font-size: 18px;"><b>Document 2</b><span style="color: red;"><b>*</b></span></p>
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<select style="float: left; height: 40px; border-radius: 3px; color: #656565; width: 17%;">
	    									<option>Select Document Type 2</option>
	    									<option>Bank Account Statement</option>
	    									<option>Driving License</option>
	    									<option>Valid Passport</option>
	    									<option>Voter ID Card</option>
	    									<option>Pan Card</option>
	    								</select>
	    								<br><br><br>
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<input type="text" name="document-id" id="document-id" placeholder="Enter Document ID" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
	    							</div>
	    							<div style="width: 100%; float: left; margin-top: 10px;">
	    								<input type="text" name="document-name" placeholder="Enter Name on Document" style="border-radius: 3px; border: 1px solid rgba(0,0,0,0.15); float: left; height: 35px; width: 25%; font-size: 16px; padding-left: 10px;">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Front Image</b></p>
	    								<input type="file" name="document-front-image">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Back Image</b></p>
	    								<input type="file" name="document-front-image">
	    							</div>
	    							<div style="width: 100%; float: left;">
	    								<hr style="border-top: 1px solid rgba(0,0,0,0.15);">
	    							</div>
	    							<div style="width: 20%; float: left; margin-top: 15px;">
	    								<p align="left" style="width: 100%; color: #656565;"><b>Selfie Image</b><span style="color: red;"><b>*</b></span></p>
	    								<input type="file" name="selfie-image">
	    							</div>
	    							<div style="width: 100%; float: left; margin-top: 20px;">
	    								<button class="btn btn-info" name="submit-documents" style="background-color: #285fdb; float: left;">Submit Documents</button>
	    							</div>
	    						</div>
	    					</form>
	    				</div>
    					</form>
    				</div>
    			</div>

    		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include 'footer.php';?>
</div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script src="js/displayDivs.js"></script>

<script>
$("button[name='individual']").click(function () {

      document.getElementById("individual").style.color = "#285fdb";
      document.getElementById("individual").style.border = "2px solid #285fdb";

      document.getElementById("business").style.color = "#656565";
      document.getElementById("business").style.border = "2px solid rgba(0,0,0,0.15)";

		if($("#individual_form").is(":visible")){
    	$("#fromName").prop("required", true);
    	$("#fromAddressLine1").prop("required", true);
    	$("#fromMobile").prop("required", true);
    	$("#fromCode").prop("required", true);

    	$("#toName").prop("required", true);
    	$("#toAddressLine1").prop("required", true);
    	$("#toMobile").prop("required", true);
    	$("#toCode").prop("required", true);

    	$("#individual-document-quantity").prop("required", true);

    	$("input[name='package-type']").click(function () {
    		$('#show-document').css('display', ($(this).val() === 'document') ? 'block':'none');
    		if($("#individual-document-quantity").is(":visible")){
    			$("#individual-document-quantity").prop("required", true);
    			$("#product_name").prop("required", false);
    			$("#quantity").prop("required", false);
    			$("#price").prop("required", false);
    			$("#weight").prop("required", false);
    			$("#length").prop("required", false);
    			$("#breadth").prop("required", false);
    			$("#height").prop("required", false);
    		} else{
    			$("#individual-document-quantity").prop("required", false);
    			$("#product_name").prop("required", true);
    			$("#quantity").prop("required", true);
    			$("#price").prop("required", true);
    			$("#weight").prop("required", true);
    			$("#length").prop("required", true);
    			$("#breadth").prop("required", true);
    			$("#height").prop("required", true);
    		}
});
  	}
});

$("button[name='business']").click(function () {

    document.getElementById("business").style.color = "#285fdb";
    document.getElementById("business").style.border = "2px solid #285fdb";

    document.getElementById("individual").style.color = "#656565";
    document.getElementById("individual").style.border = "2px solid rgba(0,0,0,0.15)";

    document.getElementById("sole_proprietor").style.display = 'none';
	document.getElementById("other_company").style.display = 'none';

		if($("#business-form").is(":visible")){
    	$("#business-fromName").prop("required", true);
    	$("#business-fromAddressLine1").prop("required", true);
    	$("#business-fromMobile").prop("required", true);
    	$("#business-fromCode").prop("required", true);

    	$("#business-toName").prop("required", true);
    	$("#business-toAddressLine1").prop("required", true);
    	$("#business-toMobile").prop("required", true);
    	$("#business-toCode").prop("required", true);
		
		$("#business-document-quantity").prop("required", true);

		$("input[name='business-package-type']").click(function () {
    		$('#business-show-document').css('display', ($(this).val() === 'document') ? 'block':'none');
    		if($("#business-document-quantity").is(":visible")){
    			$("#business-document-quantity").prop("required", true);
    			$("#business-product_name").prop("required", false);
    			$("#business-quantity").prop("required", false);
    			$("#business-price").prop("required", false);
    			$("#business-weight").prop("required", false);
    			$("#business-length").prop("required", false);
    			$("#business-breadth").prop("required", false);
    			$("#business-height").prop("required", false);
    		} else{
    			$("#business-document-quantity").prop("required", false);
    			$("#business-product_name").prop("required", true);
    			$("#business-quantity").prop("required", true);
    			$("#business-price").prop("required", true);
    			$("#business-weight").prop("required", true);
    			$("#business-length").prop("required", true);
    			$("#business-breadth").prop("required", true);
    			$("#business-height").prop("required", true);
    		}
});
  	}
});
</script>
<script type="text/javascript">
	window.onload = function(){
    document.getElementById('individual').click();
    document.getElementById('individual').focus();
}
</script>

<script type="text/javascript">
	window.setInterval(function () {
	 var digits = document.getElementById("aadhar-number").value;
	 var len = digits.toString().length;
	 if(len == 0) {
	 	document.getElementById("aadhar-counter").style.display = 'none';
	 	document.getElementById("aadhar-otp-button").style.cursor = 'not-allowed';
	 	document.getElementById("aadhar-otp-button").disabled = true;
	 	document.getElementById("aadhar-otp-button").style.backgroundColor = "#eee";
    	document.getElementById("aadhar-otp-button").style.color = "rgba(0,0,0,0.15)";
	 }
	 else if(len == 12) {
	 	document.getElementById("aadhar-counter").style.display = 'none';
	 	document.getElementById("aadhar-otp-button").style.cursor = 'pointer';
	 	document.getElementById("aadhar-otp-button").disabled = false;
	 	document.getElementById("aadhar-otp-button").style.backgroundColor = "#285fdb";
    	document.getElementById("aadhar-otp-button").style.color = "white";
	 }
	 else {
	 	document.getElementById("aadhar-counter").style.display = 'block';
	 	document.getElementById("aadhar-otp-button").style.cursor = 'not-allowed';
	 	document.getElementById("aadhar-otp-button").disabled = true;
	 	document.getElementById("aadhar-otp-button").style.backgroundColor = "#eee";
    	document.getElementById("aadhar-otp-button").style.color = "rgba(0,0,0,0.15)";
	 }
	},1);
</script>

<script type="text/javascript">
	function showProperDiv(e) {
		var companyType = e.target.value;
		if((companyType.localeCompare("sole"))==0) {
			document.getElementById("sole_proprietor").style.display = 'block';
			document.getElementById("other_company").style.display = 'none';
		}
		else if((companyType.localeCompare("None"))==0) {
			document.getElementById("sole_proprietor").style.display = 'none';
			document.getElementById("other_company").style.display = 'none';
		}
		else {
			document.getElementById("sole_proprietor").style.display = 'none';
			document.getElementById("other_company").style.display = 'block';
		}
	}
</script>

</body>
</html>
