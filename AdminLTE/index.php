<!DOCTYPE html>
<html>
<head>
	<title>Logistics</title>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="css/index.css">

</head>
<body>

	<?php

	include 'conn.php';

		require_once "users/config-login.php";
		$loginURL = $gClient->createAuthUrl();

		if($_POST) {
			if(isset($_POST['login'])) {
				startLogin();
			}
		}

		/*$dbhost = "localhost";
		$dbuser = "root";
		$dbpass = "";
		$dbname = "logistics_v2";

		Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

		if(!Opencon()) {
		die("Connection Failed :".mysqli_connect_error());
		}
		else {
			*/
			$sql_query = "SELECT * from customer_details";
			$result = Opencon() -> query($sql_query);

			if($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$db_email = $row["Email"];
				}
			}
		//}

		function startLogin() {

			$email = $_POST['email'];
			$password = $_POST['password'];
			$isAMember = 1;

		/*	$dbhost = "localhost";
			$dbuser = "root";
			$dbpass = "";
			$dbname = "logistics_v2";

			Opencon() = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

			if(!Opencon()) {
				die("Connection Failed :".mysqli_connect_error());
			}
			else {*/
				$sql_query = "SELECT * from customer_details";
				$result = Opencon() -> query($sql_query);

				if($result->num_rows > 0) {
					while ($row = $result->fetch_assoc()) {
						$db_email = $row["Email"];
						$first_name = $row['FirstName'];
						$last_name = $row['LastName'];
						$account_type = $row['AccountType'];
						$image_link = $row['ImageLink'];
						$gstin = $row['GSTIN'];
						$address = $row['ADDRESS'];
						$wallet = $row['Wallet'];
						$user_id = $row['User_ID'];
						$member_plan = $row['Member_Plan'];
						if(strcmp($db_email, $email) == 0) {
							$sql_password_query = "SELECT Password from customer_details WHERE Email = '".$email."'";
							$result = Opencon()-> query($sql_password_query);
							while ($row = $result->fetch_assoc()) {
								$db_password = $row['Password'];
								if(strcmp($db_password, $password) == 0) {
									$isAMember++;
								}
							}
						}
					}
					if($isAMember > 1) {
						$_SESSION['Email'] = $email;
						$_SESSION['FirstName'] = $first_name;
						$_SESSION['LastName'] = $last_name;
						$_SESSION['AccountType'] = $account_type;
						$_SESSION['ImageLink'] = $image_link;
						$_SESSION['GSTIN'] = $gstin;
						$_SESSION['ADDRESS'] = $address;
						$_SESSION['User_ID'] = $user_id;
						$_SESSION['Member_Plan'] = $member_plan;
						echo '<script language="javascript">';
						echo 'location.href="Dashboard";';
						echo '</script>';
					}
					else {
						echo '<script language="javascript">';
						echo 'alert("Invalid Details");';
						echo 'location.href="Home";';
						echo '</script>';
					}
				}
			//}
		}

	?>

	<img src="images/background_1.jpg" style="width: 100%; height: 609px;">
	<div class="container">
		<div style="font-size: 34px; margin-top: -454px; color: white;"><b>ECommerce Shipping Made Easy</b><br><br><span style="font-size: 20px; line-height: 1.6; color: white;">India’s #1 eCommerce shipping solution, trusted by over 25,000 brands and entrepreneurs<br> for lowest shipping rates, widest reach and best customer service.</span><br><br><span><button onclick="window.location.href = 'Register';" type="button" class="btn btn-info" style="border-radius: 48px; padding: 10px; font-size: 16px;">Sign Up For Free</button></span></div>
		<div class="navbar transparent navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="Home">
						<img alt="Logo" src="images/logo.png" style="margin-top: -10px; height: 36px; width: 148px; max-width: 100%">
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#">Home<span class="sr-only">(current)</span></a></li>
						<li><a href="#">About Us</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#">Pricing</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<form style="padding: 20px;" method="post" enctype="multipart/form-data">
									<li><p style="color: white">Email&nbsp&nbsp<span style="color: black;"><input type="text" name="email" required></span></p></li>
									<br>
									<li><p style="color: white;">Password&nbsp&nbsp&nbsp<span style="color: black;"><input type="Password" name="password" id="showPass" style="width: 54%;" required><a href="javascript:showPassword();" style="color: gray; cursor: pointer; background-color: white; border: white; padding: 4px;"><i class="fas fa-eye"></i></a></span></p></li>
									<br>
									<li><span style="display: block; margin-left: 38%;"><input type="submit" name="login" value="Login" class="btn btn-info"></span></li>
								</form>
								<li><p style="text-align: center; font-size: 14px; color: white;">&nbsp&nbsp&nbspOR<p></li><br>
								<li><button class="btn btn-info" style="width: 40%; float: left;" onclick="window.location = '<?php echo $loginURL ?>';"><i class="fab fa-google"></i></button>
								<button class="btn btn-info" style="width: 40%; float: right;"><i class="fab fa-facebook-f"></i></button></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Options<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Feedback</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</div><br><br><br><br><br><br><br><br>
		<h1 style="color: #3C414C;">Why Choose Us?</h1><br>
		<p style="float: right; font-size: 19px; margin-top: 15px;"><span style="color: #ff9900;">Grow your ecommerce business</span> and reduce cost.</p>
		<img src="images/grow_business.png" style="float: right; height: auto; width: auto; max-width: 100%;">
		<img src="images/reduce_shipping_cost.png" style="float: left; height: auto; width: : auto; max-width: 100%">

		<br><br>
	</div>
	<script type="text/javascript">
		function showPassword() {
			var x = document.getElementById("showPass");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}
	</script>
</body>
</html>
